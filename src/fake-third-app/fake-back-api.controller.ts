import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Req,
  Res,
} from '@nestjs/common';
import { lastValueFrom } from 'rxjs';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { FakeBackApiService } from './services/fake-back-api.service';
import { join } from 'path';
import { Response, NextFunction, Request } from 'express';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { UnitEntity } from './entities/unit.entity';
import { PaginationQueryDto } from './dto/pagination-query.dto';

@ApiTags('backend fake third app')
@Controller('units')
export class FakeBackApiController {
  errorContext: string;
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
    private readonly fakeBackApiService: FakeBackApiService,
  ) {
    this.errorContext = 'FakeThirdAppModule.FakeBackApiController';
    // Reset sample data on restart session
    this.fakeBackApiService.resetAllUnits();
  }

  // QUERY
  @Get()
  async getUnits(@Res() res, @Query() paginationQuery: PaginationQueryDto) {
    try {
      this.logger.debug(`${this.errorContext}.getUnits().begun`);
      const units = await lastValueFrom(
        this.fakeBackApiService.getUnits(paginationQuery),
      );
      return res.status(HttpStatus.OK).json(units);
    } catch (err) {
      const errorMessage = `${this.errorContext}.getUnits().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: Units not listed! ${errorMessage}`,
        status: 500,
      });
    }
  }

  // READ
  @Get('/:id')
  async getUnitById(@Res() res, @Param('id') unitId: string) {
    try {
      this.logger.debug(`${this.errorContext}.getUnitById().begun`);
      if (!unitId) {
        throw new NotFoundException('Param ID is required');
      }
      const unit = await lastValueFrom(
        this.fakeBackApiService.getUnitById(unitId),
      );
      if (!unit) {
        throw new NotFoundException('Unit does not exist!');
      }
      return res.status(HttpStatus.OK).json(unit);
    } catch (err) {
      const errorMessage = `${this.errorContext}.getUnitById().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: Unit not read! ${errorMessage}`,
        status: 500,
      });
    }
  }

  // CREATE
  @Post()
  // @HttpCode(201) // already 201 by defaut for POST
  async createUnit(@Res() res, @Body() createUnitEntity: UnitEntity) {
    try {
      this.logger.debug(`${this.errorContext}.createUnit().begun`);
      if (!createUnitEntity) {
        throw new NotFoundException('Param body is required');
      }
      const unit = await lastValueFrom(
        this.fakeBackApiService.createUnit(createUnitEntity),
      );
      return res.status(HttpStatus.CREATED).json(unit);
    } catch (err) {
      const errorMessage = `${this.errorContext}.createUnit().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: Unit not created! ${errorMessage}`,
        status: 500,
      });
    }
  }

  // UPDATE
  // curl -X PATCH -H "Content-type:application/json" --data-binary "{ \"code\": \"test\"}" http://localhost:3000/units/608da27a23ec6f382c27a644
  @Patch('/:id')
  async updateUnit(
    @Res() res,
    @Param('id') unitId: string,
    @Body() updateUnitEntity: UnitEntity,
  ) {
    try {
      this.logger.debug(`${this.errorContext}.updateUnit().begun`);
      if (!unitId) {
        throw new NotFoundException('Param ID is required');
      }
      if (!updateUnitEntity) {
        throw new NotFoundException('Param body is required');
      }
      const unit = await lastValueFrom(
        this.fakeBackApiService.updateUnit(unitId, updateUnitEntity),
      );
      if (!unit) {
        throw new NotFoundException('Unit does not exist!');
      }
      return res.status(HttpStatus.OK).json(unit);
    } catch (err) {
      const errorMessage = `${this.errorContext}.updateUnit().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: Unit not updated! ${errorMessage}`,
        status: 500,
      });
    }
  }

  @Put('/:id')
  async updatePutUnit(
    @Res() res,
    @Param('id') unitId: string,
    @Body() updateUnitEntity: UnitEntity,
  ) {
    try {
      this.logger.debug(`${this.errorContext}.updatePutUnit().begun`);
      if (!unitId) {
        throw new NotFoundException('Param ID is required');
      }
      const unit = await lastValueFrom(
        this.fakeBackApiService.updateUnit(unitId, updateUnitEntity),
      );
      if (!unit) {
        throw new NotFoundException('Unit does not exist!');
      }
      return res.status(HttpStatus.OK).json(unit);
    } catch (err) {
      const errorMessage = `${this.errorContext}.updatePutUnit().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: Unit not updated! ${errorMessage}`,
        status: 500,
      });
    }
  }

  // DELETE
  @Delete('/:id')
  async deleteUnit(@Res() res, @Param('id') unitId: string) {
    try {
      this.logger.debug(`${this.errorContext}.deleteUnit().begun`);
      if (!unitId) {
        throw new NotFoundException('Unit ID does not exist');
      }
      const unit = lastValueFrom(this.fakeBackApiService.deleteUnit(unitId));
      if (!unit) {
        throw new NotFoundException('Unit does not exist!');
      }
      return res.status(HttpStatus.OK).json(unit);
    } catch (err) {
      const errorMessage = `${this.errorContext}.deleteUnit().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: Unit not deleted! ${errorMessage}`,
        status: 500,
      });
    }
  }
}
