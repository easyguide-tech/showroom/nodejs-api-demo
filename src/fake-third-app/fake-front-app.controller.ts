import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Req,
  Res,
} from '@nestjs/common';
import { lastValueFrom } from 'rxjs';
import { ApiTags } from '@nestjs/swagger';
import { FakeBackApiService } from './services/fake-back-api.service';
import { join } from 'path';
import { Response, NextFunction, Request } from 'express';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { UnitEntity } from './entities/unit.entity';
import { PaginationQueryDto } from './dto/pagination-query.dto';

@ApiTags('front fake third app')
@Controller('front')
export class FakeFrontAppController {
  errorContext: string;
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
    private readonly fakeBackApiService: FakeBackApiService,
  ) {
    this.errorContext = 'FakeThirdAppModule.FakeFrontAppController';
    // Reset sample data on restart session
    this.fakeBackApiService.resetAllUnits();
  }

  // Get images
  @Get('assets/:imgId')
  getAssets(@Param('imgId') imgId, @Res() res) {
    try {
      const imgPath = join(process.cwd(), `assets/images/${imgId}`);
      return res.sendFile(imgPath, { root: 'public' });
    } catch (err) {
      const errorMessage = `${this.errorContext}.getAssets().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.render('error', {
        title: 'Error',
        message: `Error: Assets not get ! ${errorMessage}`,
      });
    }
  }

  // CREATE FRONTEND
  @Get('/units/new')
  // @HttpCode(201) // already 201 by defaut for POST
  async newFrontUnit(@Res() res) {
    try {
      this.logger.debug(`${this.errorContext}.newFrontUnit().begun`);
      const unit = {
        code: '',
        label: '',
        photo: '',
        active: '',
      };
      return res.render('crud/form', { title: 'Unit', data: unit });
    } catch (err) {
      const errorMessage = `${this.errorContext}.newFrontUnit().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.render('error', {
        title: 'Error',
        message: `Error: Units not created! ${errorMessage}`,
      });
    }
  }

  // EDIT FRONTEND
  @Get('/units/:id/edit')
  async editFrontUnit(@Res() res, @Param('id') unitId: string) {
    try {
      this.logger.debug(`${this.errorContext}.editFrontUnit().begun`);
      if (!unitId) {
        throw new NotFoundException('Param ID is required');
      }
      const unit = await lastValueFrom(
        this.fakeBackApiService.getUnitById(unitId),
      );
      if (!unit) {
        throw new NotFoundException('Unit does not exist!');
      }
      return res.render('crud/form', {
        title: 'Unit',
        message: 'Success form',
        data: unit,
      });
    } catch (err) {
      const errorMessage = `${this.errorContext}.editFrontUnit().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.render('error', {
        title: 'Error',
        message: `Error: Unit edit form not rendered ! ${errorMessage}`,
      });
    }
  }

  // UPDATE FRONTEND with PATCH
  @Post('/units/:id/update')
  async updateFrontPatchUnit(
    @Res() res,
    @Param('id') unitId: string,
    @Body() updateUnitEntity: UnitEntity,
  ) {
    try {
      this.logger.debug(`${this.errorContext}.updateFrontPatchUnit().begun`);
      if (!unitId) {
        throw new NotFoundException('Param ID is required');
      }
      if (!updateUnitEntity) {
        throw new NotFoundException('Param body is required');
      }
      const unit = await lastValueFrom(
        this.fakeBackApiService.updateUnit(unitId, updateUnitEntity),
      );
      if (!unit) {
        throw new NotFoundException('Unit does not exist!');
      }
      return res.status(200).redirect(`/front/units/${unit.id}`);
    } catch (err) {
      const errorMessage = `${this.errorContext}.updateFrontPatchUnit().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.render('error', {
        title: 'Error',
        message: `Error: Unit not updated with patch! ${errorMessage}`,
      });
    }
  }

  // DELETE
  @Get('/units/:id/delete')
  async deleteFrontUnit(@Res() res, @Param('id') unitId: string) {
    try {
      this.logger.debug(`${this.errorContext}.deleteFrontUnit().begun`);
      if (!unitId) {
        throw new NotFoundException('Unit ID does not exist');
      }
      const unit = lastValueFrom(this.fakeBackApiService.deleteUnit(unitId));
      if (!unit) {
        throw new NotFoundException('Unit does not exist!');
      }
      return res.redirect(`/front/units`);
    } catch (err) {
      const errorMessage = `${this.errorContext}.deleteFrontUnit().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.render('error', {
        title: 'Error',
        message: `Error: Unit not deleted! ${errorMessage}`,
      });
    }
  }

  // READ FRONTEND
  @Get('/units/:id')
  async getFrontUnitById(@Res() res, @Param('id') unitId: string) {
    try {
      this.logger.debug(`${this.errorContext}.getFrontUnitById().begun`);
      if (!unitId) {
        throw new NotFoundException('Param ID is required');
      }
      const unit = await lastValueFrom(
        this.fakeBackApiService.getUnitById(unitId),
      );
      if (!unit) {
        throw new NotFoundException('Unit does not exist!');
      }
      return res.render('crud/detail', {
        title: 'Unit',
        message: 'Success detail',
        data: unit,
      });
    } catch (err) {
      const errorMessage = `${this.errorContext}.getFrontUnitById().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.render('error', {
        title: 'Error',
        message: `Error: Units not read! ${errorMessage}`,
      });
    }
  }

  // QUERY FRONTEND
  @Get('/units')
  async getFrontUnits(
    @Res() res,
    @Query() paginationQuery: PaginationQueryDto,
  ) {
    try {
      this.logger.debug(`${this.errorContext}.getFrontUnits().begun`);
      const units = await lastValueFrom(
        this.fakeBackApiService.getUnits(paginationQuery),
      );
      // return res.status(HttpStatus.OK).json(units);
      return res.render('crud/list', {
        title: 'Units',
        message: 'Success list',
        data: units,
      });
    } catch (err) {
      const errorMessage = `${this.errorContext}.getFrontUnits().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.render('error', {
        title: 'Error',
        message: `Error: Units not listed! ${errorMessage}`,
      });
    }
  }

  // CREATE FRONTEND
  @Post('/units')
  // @HttpCode(201) // already 201 by defaut for POST
  async createFrontUnit(@Res() res, @Body() createUnitEntity: UnitEntity) {
    try {
      this.logger.debug(`${this.errorContext}.createFrontUnit().begun`);
      if (!createUnitEntity) {
        throw new NotFoundException('Param body is required');
      }
      const unit = await lastValueFrom(
        this.fakeBackApiService.createUnit(createUnitEntity),
      );
      return res.redirect(`/front/units/${unit.id}`);
    } catch (err) {
      const errorMessage = `${this.errorContext}.createFrontUnit().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.render('error', {
        title: 'Error',
        message: `Error: Units not created! ${errorMessage}`,
      });
    }
  }

  // Browser
  @Get('/login')
  getLogin(@Res() res: Response) {
    return res.render('auth/login', { title: 'Login', data: null });
  }

  // Browser
  @Get('/register')
  getRegister(@Res() res: Response) {
    return res.render('auth/register', { title: 'Register', data: null });
  }

  // Browser
  @Post('/login')
  postLogin(@Req() req: Request, @Res() res: Response) {
    if (req.body.name !== 'foo' || req.body.password !== 'bar')
      return res.redirect(
        '/front/failed' + "?message='Missing login and password'",
      );
    res.redirect('/front/success' + "?message='Authenticated user'");
  }

  // Browser
  @Post('/register')
  postRegister(@Req() req: Request, @Res() res: Response) {
    if (
      req.body.name !== 'foo' ||
      req.body.password !== 'bar' ||
      req.body.confirm_password !== 'bar'
    )
      return res.redirect(
        '/front/failed' + "?message='Missing login and password'",
      );
    res.redirect('/front/login' + "?message='You can login now'");
  }

  // Browser
  @Get('/success')
  getSuccess(@Req() req: Request, @Res() res: Response) {
    return res.render('success', {
      title: 'Success',
      message: `Success: ${req.query.message}`,
    });
    return res.sendFile(join(process.cwd(), 'src/views/success.hbs'));
  }

  // Browser
  @Get('/failed')
  getFailed(@Req() req: Request, @Res() res: Response) {
    return res.render('failed', {
      title: 'Failed',
      message: `Failed: ${req.query.message}`,
    });
  }

  // Browser
  @Get('/')
  getHome(@Req() req: Request, @Res() res: Response) {
    return res.render('home', {
      title: 'Home',
      message: `Welcome: ${req.query.message}`,
    });
    return res.sendFile(join(process.cwd(), 'src/views/home.hbs'));
  }
}
