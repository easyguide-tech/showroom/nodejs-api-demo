import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import {
  delay,
  dematerialize,
  materialize,
  Observable,
  of,
  throwError,
} from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { Buffer } from 'buffer';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';

/*
Make sure to add these settings in the compilerOptions section of your file tsconfig.json:
"resolveJsonModule": true,
"esModuleInterop": true,
*/
import unitsJson from '../data/units.json';
import { UnitsDto } from '../dto/units.dto';
import { UnitEntity } from '../entities/unit.entity';
import store from 'store';
import { PaginationQueryDto } from '../dto/pagination-query.dto';

// array in local storage for units
const unitsKey = 'units-mock';
const unitsJsonLocal = unitsJson;

@Injectable()
export class FakeBackApiService {
  errorContext: string;
  units: UnitsDto;

  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
  ) {
    this.errorContext = 'FakeThirdAppModule.FakeBackApiService';
  }

  getUnits(paginationQuery?: PaginationQueryDto): Observable<any> {
    try {
      this.logger.debug(`${this.errorContext}.getUnits().begun`);
      const { limit, offset, keyword } = paginationQuery;
      this.units = store.get(unitsKey)
        ? JSON.parse(store.get(unitsKey))
        : unitsJsonLocal;
      let filteredObj = Object.assign({}, this.units);
      // return ok(units.map(x => basicDetails(x)));
      if (keyword && keyword != '')
        filteredObj.results = filteredObj.results.filter((item) => {
          return item.label.includes(keyword);
        });
      if (offset && offset != '')
        filteredObj.results = filteredObj.results.slice(parseInt(offset, 10));
      if (limit && limit != '')
        filteredObj.results = filteredObj.results.slice(0, parseInt(limit, 10));
      filteredObj.results = filteredObj.results.map((x) =>
        this.basicDetails(x),
      );
      return this.ok(filteredObj);
    } catch (err) {
      const errorMessage: string = `${this.errorContext}.getUnits().failed: ${err}`;
      this.logger.error(errorMessage);
      throw errorMessage;
    }
  }

  getUnitById(id): Observable<any> {
    try {
      this.logger.debug(`${this.errorContext}.getUnitById().begun`);
      this.units = store.get(unitsKey)
        ? JSON.parse(store.get(unitsKey))
        : unitsJsonLocal;
      const unit = this.units.results.find((x) => x.id === id);
      if (!unit) {
        throw new NotFoundException(`Unit #${id} not found`);
      }
      return this.ok(this.basicDetails(unit));
    } catch (err) {
      const errorMessage: string = `${this.errorContext}.getUnitById().failed: ${err}`;
      this.logger.error(errorMessage);
      throw errorMessage;
    }
  }

  createUnit(body): Observable<any> {
    try {
      this.logger.debug(`${this.errorContext}.createUnit().begun`);
      this.units = store.get(unitsKey)
        ? JSON.parse(store.get(unitsKey))
        : unitsJsonLocal;
      const unit = body;

      if (this.units.results.find((x) => x.code === unit.code)) {
        throw Error(`Unit with the code ${unit.code} already exists`);
      }

      // assign unit id and a few other properties then save
      unit.id = this.newUnitId();
      this.units.results.push(unit);
      store.set(unitsKey, JSON.stringify(this.units));

      return this.ok(this.basicDetails(unit));
    } catch (err) {
      const errorMessage: string = `${this.errorContext}.createUnit().failed: ${err}`;
      this.logger.error(errorMessage);
      throw errorMessage;
    }
  }

  updateUnit(id, body): Observable<any> {
    try {
      this.logger.debug(`${this.errorContext}.updateUnit().begun`);
      this.units = store.get(unitsKey)
        ? JSON.parse(store.get(unitsKey))
        : unitsJsonLocal;
      const params = body;
      let unitIndex: number;
      const unit = this.units.results.find((x, index) => {
        if (x.id === id) {
          unitIndex = index;
          return true;
        }
        return false;
      });

      if (!unit) {
        throw new NotFoundException(`Unit #${id} not found`);
      }

      /*
      if (
        params.code !== unit.code &&
        this.units.results.find((x) => x.code === params.code)
      ) {
        throw Error(`Unit with the code ${params.code} already exists`);
      }
      */

      // only update password if entered
      if (!params.password) {
        delete params.password;
      }

      // update and save unit
      const updatedUnit = Object.assign(unit, params);
      this.units.results[unitIndex] = updatedUnit;
      store.set(unitsKey, JSON.stringify(this.units));

      return this.ok(this.basicDetails(unit));
    } catch (err) {
      const errorMessage: string = `${this.errorContext}.updateUnit().failed: ${err}`;
      this.logger.error(errorMessage);
      throw errorMessage;
    }
  }

  deleteUnit(id): Observable<any> {
    try {
      this.logger.debug(`${this.errorContext}.deleteUnit().begun`);
      const unit = this.units.results.find((x) => x.id === id);
      if (!unit) {
        throw new NotFoundException(`Unit #${id} not found`);
      }
      this.units = store.get(unitsKey)
        ? JSON.parse(store.get(unitsKey))
        : unitsJsonLocal;
      this.units.results = this.units.results.filter((x) => x.id !== id);
      store.set(unitsKey, JSON.stringify(this.units));
      return this.ok(this.basicDetails(unit));
    } catch (err) {
      const errorMessage: string = `${this.errorContext}.deleteUnit().failed: ${err}`;
      this.logger.error(errorMessage);
      throw errorMessage;
    }
  }

  resetAllUnits() {
    // Remove current selection
    store.remove(unitsKey);

    // Clear all keys
    // store.clearAll();
  }

  // helper functions

  ok(body?: any) {
    return of(body).pipe(delay(500)); // delay observable to simulate server api call
  }

  error(message: any) {
    this.logger.error(message);
    return of(message).pipe(materialize(), delay(500), dematerialize()); // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648);
  }

  basicDetails(unit: any) {
    const { id, code, label, photo, active } = unit;
    return { id, code, label, photo, active };
  }

  newUnitId() {
    let myuuid = uuidv4();
    // base64 encoding
    myuuid = Buffer.from(myuuid).toString('base64').substring(0, 20);
    // return units.results.length ? Math.max(...units.results.map(x => x.id)) + 1 : 1;
    return myuuid;
  }
}
