import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString, IsBoolean } from 'class-validator';

export class UnitEntity {
  @IsBoolean()
  @ApiProperty({ example: false, description: 'The active' })
  active: boolean;

  @IsString()
  @ApiProperty({ example: 'fake code 5', description: 'The code' })
  code: string;

  @IsString()
  @ApiProperty({ example: 'fake label 5', description: 'The label' })
  label: string;

  @IsString()
  @ApiProperty({
    example: 'assets/images/icon-email.png',
    description: 'The photo url',
  })
  photo: string;

  @IsString()
  @ApiProperty({ example: '608ab7e27a84420dfc16b335', description: 'The ID' })
  id: string;
}
