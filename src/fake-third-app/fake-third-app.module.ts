import { Module } from '@nestjs/common';
import { FakeBackApiController } from './fake-back-api.controller';
import { FakeFrontAppController } from './fake-front-app.controller';
import { FakeBackApiService } from './services/fake-back-api.service';

@Module({
  imports: [],
  controllers: [FakeBackApiController, FakeFrontAppController],
  providers: [FakeBackApiService],
})
export class FakeThirdAppModule {}
