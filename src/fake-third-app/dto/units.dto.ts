import { IsInt, IsString, IsBoolean } from 'class-validator';
import { UnitEntity } from '../entities/unit.entity';

export class UnitsDto {
  results: UnitEntity[];

  @IsInt()
  page: number;

  @IsInt()
  limit: number;

  @IsInt()
  totalPages: number;

  @IsInt()
  totalResults: number;
}
