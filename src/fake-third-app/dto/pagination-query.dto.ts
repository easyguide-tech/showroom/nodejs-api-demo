import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class PaginationQueryDto {
  @ApiProperty()
  @IsOptional()
  limit: string;

  @ApiProperty()
  @IsOptional()
  offset: string;

  @ApiProperty()
  @IsOptional()
  keyword: string;
}
