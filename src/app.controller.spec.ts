import { Test, TestingModule } from '@nestjs/testing';
import { WinstonModule } from 'nest-winston';

import { LoggerConfig } from './logger.config';
import { AppController } from './app.controller';
import { AppService } from './app.service';

describe('AppController', () => {
  let appModule: TestingModule;
  let appController;

  beforeAll(async () => {
    const logger: LoggerConfig = new LoggerConfig();
    appModule = await Test.createTestingModule({
      imports: [WinstonModule.forRoot(logger.console())],
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = appModule.get<AppController>(AppController);
  });

  describe('getHello', () => {
    it('should return "Hello World!"', () => {
      const loggerSpy = jest.spyOn((appController as any).logger, 'debug'); // you can also add on mockResponse type functions here like mockReturnValue and mockResolvedValue
      expect(loggerSpy).toHaveBeenCalled();
      expect(appController.getHello()).toBe('Hello World!');
    });
  });
});
