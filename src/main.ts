import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import path, { join } from 'path';
import { readFileSync, writeFileSync } from 'fs';
import * as yaml from 'js-yaml';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { WinstonModule } from 'nest-winston';
import { format, transports } from 'winston';

async function bootstrap() {
  // const app = await NestFactory.create(AppModule);
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    logger: WinstonModule.createLogger({
      exitOnError: false,
      format: format.combine(
        format.colorize(),
        format.timestamp(),
        format.printf((msg) => {
          return `Nestjs - ${msg.timestamp} [${msg.level}] - ${msg.message}`;
        }),
      ),
      transports: [new transports.Console({ level: 'debug' })], // alert > error > warning > notice > info > debug
    }),
  });

  // Swagger documentation
  const SWAGGER_ENVS = ['local', 'dev', 'staging'];
  console.debug(process.env.NODE_ENV);
  if (SWAGGER_ENVS.includes(process.env.NODE_ENV)) {
    // Hide Swagger UI on production
    const config = new DocumentBuilder()
      .setTitle('API Docs')
      .setDescription('The API documentation')
      .setVersion('1.0')
      //.addTag('Nestjs starter')
      .build();
    const document = SwaggerModule.createDocument(app, config);

    // To create the json swagger file specification, or throw exception on error
    try {
      await writeFileSync(
        path.resolve(__dirname, '../docs/openapi-swagger/openapi-spec.yml'),
        yaml.dump(document),
      );
      // const doc = yaml.load(fs.readFileSync(path.resolve(__dirname, '../docs/openapi-swagger/openapi-spec.yml'), 'utf8'));
      // console.debug(doc);
    } catch (err) {
      console.debug(`Something went wrong: ${JSON.stringify(err, null, 4)}`);
    }

    // To let nestjs serve the swagger doc
    SwaggerModule.setup('api', app, document);
  }

  // For adding server-side render views with handlebar files
  app.setBaseViewsDir(join(__dirname, 'fake-third-app', 'views'));
  app.setViewEngine('hbs');
  // For adding static files to serve
  app.useStaticAssets(join(__dirname, '..', 'docs'), {
    prefix: '/docs',
  });
  app.useStaticAssets(join(__dirname, '..', 'coverage'), {
    prefix: '/coverage',
  });
  await app.listen(3000);
}
bootstrap();
