import {
  Controller,
  Get,
  Res,
  HttpStatus,
  Post,
  Body,
  Put,
  NotFoundException,
  Delete,
  Param,
  Query,
  Inject,
} from '@nestjs/common';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { UsersService } from './users.service';
import { CreateUserDto, UpdateUserDto } from './dto';
import { PaginationQueryDto } from '../common/dto/pagination-query.dto';

@ApiTags('Users resource')
@Controller('users')
export class UsersController {
  errorContext: string;
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
    private usersService: UsersService,
  ) {
    this.errorContext = 'UsersModule.UsersController';
  }

  // QUERY
  @Get()
  public async getAllUser(
    @Res() res,
    @Query() paginationQuery: PaginationQueryDto,
  ) {
    try {
      this.logger.debug(`${this.errorContext}.getAllUser().begun`);
      const users = await this.usersService.findAll(paginationQuery);
      return res.status(HttpStatus.OK).json(users);
    } catch (err) {
      const errorMessage = `${this.errorContext}.getAllUser().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: Users not listed! ${errorMessage}`,
        status: 500,
      });
    }
  }

  // READ
  @Get('/:id')
  public async getUser(@Res() res, @Param('id') userId: string) {
    try {
      this.logger.debug(`${this.errorContext}.getUser().begun`);
      if (!userId) {
        throw new NotFoundException('Param ID is required');
      }
      const user = await this.usersService.findOne(userId);
      if (!user) {
        throw new NotFoundException('User does not exist!');
      }
      return res.status(HttpStatus.OK).json(user);
    } catch (err) {
      const errorMessage = `${this.errorContext}.getUser().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: User not read! ${errorMessage}`,
        status: 500,
      });
    }
  }

  // CREATE
  @Post()
  // @HttpCode(201) // already 201 by defaut for POST
  public async createUser(@Res() res, @Body() createUserDto: CreateUserDto) {
    try {
      this.logger.debug(`${this.errorContext}.createUser().begun`);
      if (!createUserDto) {
        throw new NotFoundException('Param body is required');
      }
      const user = await this.usersService.create(createUserDto);
      return res.status(HttpStatus.CREATED).json({
        message: 'User has been created successfully',
        user,
      });
    } catch (err) {
      const errorMessage = `${this.errorContext}.createUser().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: User not created! ${errorMessage}`,
        status: 500,
      });
    }
  }

  // UPDATE
  @Put('/:id')
  public async updateUser(
    @Res() res,
    @Param('id') userId: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    try {
      this.logger.debug(`${this.errorContext}.updateUser().begun`);
      if (!userId) {
        throw new NotFoundException('Param ID is required');
      }
      const user = await this.usersService.update(userId, updateUserDto);
      if (!user) {
        throw new NotFoundException('User does not exist!');
      }
      return res.status(HttpStatus.OK).json({
        message: 'User has been successfully updated',
        user,
      });
    } catch (err) {
      const errorMessage = `${this.errorContext}.updateUser().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: User not updated! ${errorMessage}`,
        status: 500,
      });
    }
  }

  // DELETE
  @Delete('/:id')
  public async deleteUser(@Res() res, @Param('id') userId: string) {
    try {
      this.logger.debug(`${this.errorContext}.deleteUser().begun`);
      if (!userId) {
        throw new NotFoundException('User ID does not exist');
      }

      const user = await this.usersService.remove(userId);

      if (!user) {
        throw new NotFoundException('User does not exist');
      }

      return res.status(HttpStatus.OK).json({
        message: 'User has been deleted',
        user,
      });
    } catch (err) {
      const errorMessage = `${this.errorContext}.deleteUser().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: User not deleted! ${errorMessage}`,
        status: 500,
      });
    }
  }
}
