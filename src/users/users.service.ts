import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { UserInterface } from './models/interfaces/user.interface';
import { CreateUserDto, UpdateUserDto } from './dto';
import { UserModel } from './models/schemas/user.schema';
import { PaginationQueryDto } from '../common/dto/pagination-query.dto';

@Injectable()
export class UsersService {
  errorContext: string;
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
    @InjectModel(UserModel.name) private readonly userModel: Model<UserModel>,
  ) {
    this.errorContext = 'UsersModule.UsersService';
  }

  public async findAll(
    paginationQuery: PaginationQueryDto,
  ): Promise<UserModel[]> {
    try {
      this.logger.debug(`${this.errorContext}.findAll().begun`);
      const { limit, offset, keyword } = paginationQuery;

      return await this.userModel
        .find()
        .skip(offset)
        .limit(limit)
        //.populate('organization')
        .exec();
    } catch (err) {
      const errorMessage = `${this.errorContext}.findAll().failed: ${err}`;
      this.logger.error(errorMessage);
      throw errorMessage;
    }
  }

  public async findOne(userId: string): Promise<UserModel> {
    try {
      this.logger.debug(`${this.errorContext}.findOne().begun`);
      const user = await this.userModel
        .findById({ _id: userId })
        //.populate('organization')
        .exec();

      if (!user) {
        throw new NotFoundException(`User #${userId} not found`);
      }
      return user;
    } catch (err) {
      const errorMessage = `${this.errorContext}.findOne().failed: ${err}`;
      this.logger.error(errorMessage);
      throw errorMessage;
    }
  }

  public async getByEmail(email: string): Promise<UserModel> {
    try {
      this.logger.debug(`${this.errorContext}.getByEmail().begun`);
      const user = await this.userModel
        .findOne({ email: email })
        //.populate('organization')
        .exec();

      if (!user) {
        throw new NotFoundException(`User #${email} not found`);
      }
      return user;
    } catch (err) {
      const errorMessage = `${this.errorContext}.getByEmail().failed: ${err}`;
      this.logger.error(errorMessage);
      throw errorMessage;
    }
  }

  public async create(createUserDto: CreateUserDto): Promise<UserInterface> {
    try {
      this.logger.debug(`${this.errorContext}.create().begun`);
      const newUser = await new this.userModel(createUserDto);
      return newUser.save();
    } catch (err) {
      const errorMessage = `${this.errorContext}.create().failed: ${err}`;
      this.logger.error(errorMessage);
      throw errorMessage;
    }
  }

  public async update(
    userId: string,
    updateUserDto: UpdateUserDto,
  ): Promise<UserInterface> {
    try {
      this.logger.debug(`${this.errorContext}.update().begun`);
      const existingUser = await this.userModel.findByIdAndUpdate(
        { _id: userId },
        updateUserDto,
      );

      if (!existingUser) {
        throw new NotFoundException(`User #${userId} not found`);
      }

      return existingUser;
    } catch (err) {
      const errorMessage = `${this.errorContext}.update().failed: ${err}`;
      this.logger.error(errorMessage);
      throw errorMessage;
    }
  }

  public async remove(userId: string): Promise<any> {
    try {
      this.logger.debug(`${this.errorContext}.remove().begun`);
      const deletedUser = await this.userModel.findByIdAndDelete(userId);
      return deletedUser;
    } catch (err) {
      const errorMessage = `${this.errorContext}.remove().failed: ${err}`;
      this.logger.error(errorMessage);
      throw errorMessage;
    }
  }
}
