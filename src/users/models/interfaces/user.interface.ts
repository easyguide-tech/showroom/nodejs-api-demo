import { Document } from 'mongoose';

export interface UserInterface extends Document {
  readonly firstName: string;
  readonly lastName: string;
  readonly email: string;
  readonly phone: string;
  readonly description: string;
}
