import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '../../users.service';
import request from 'supertest';
// import mockedUser from './mocks/user.mock';
import mockedUsers from './mocks/users.mock.json';
import { UserInterface } from 'src/users/models/interfaces/user.interface';
import { CreateUserDto } from 'src/users/dto';
import { UpdateUserDto } from 'src/users/dto';
import { UserModel } from 'src/users/models/schemas/user.schema';

describe('Users controller', () => {
  let app: INestApplication;
  let usersService: UsersService;
  // let expectedData: Partial<UserInterface>;
  // let mockedGetThirdData: jest.Mock;

  beforeAll(async () => {
    /*
    const mockedThirdService = {
      getThirdData: jest.fn().mockResolvedValue(mockedUsers),
      // getThirdData: jest.fn().mockReturnValue(Promise.resolve(mockedUsers)),
      // getThirdData: jest.fn().mockReturnValue(Promise.reject(mockedUsers)),
      // getThirdData: mockedGetThirdData
    }
    */

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        // CoreModule,
        // ThirdModule,
      ],
      providers: [
        UsersService,
        /*
        {
            provide: ThirdService,
            useValue: mockedThirdService
        },
        */
      ],
    })
      //.overrideProvider(ThirdService)
      //.useValue(mockedThirdService)
      .compile();

    app = await moduleFixture.createNestApplication();
    // AAdd the ValidationPipe if you want to verify your validation.
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    usersService = await moduleFixture.get<UsersService>(UsersService);
  });

  afterEach(() => {
    // To mockRestore the spy
    // You can use mockImplementationOnce() instead of mockImplementation to not having to mockRestore the spy.
    jest.restoreAllMocks();
  });

  // QUERY
  describe('when running a query request', () => {
    describe('and using valid data', () => {
      const expectedData: any = mockedUsers;
      const mockUsersService = jest
        .spyOn(usersService, 'findAll')
        .mockImplementationOnce(() => Promise.resolve(expectedData));
      // mockedGetThirdData = jest.fn().mockResolvedValue(expectedData);
      it('should respond with a list of data', (done) => {
        // Jest will wait until the done callback is called before finishing the test
        return request(app.getHttpServer())
          .get('/users')
          .end((err, res) => {
            if (err) done(err);
            expect(mockUsersService).toBeDefined();
            expect(mockUsersService).toHaveBeenCalled();
            expect(res.status).toBe(200);
            expect(res.type).toEqual('application/json');
            // Check body data
            expect(res.body).not.toBeNull();
            //expect(res.body).toEqual(expect.any(Object));
            expect(res.body).toEqual(expect.any(Array));
            expect(res.body).toHaveLength(3);
            expect(res.body).toBe(expectedData);
            // Check also first data
            const firstUser = res.body.find(
              (user: any) => user.email === 'user@email.com',
            );
            expect(firstUser).toBeTruthy();
            expect(firstUser).toHaveProperty('firstName');
            expect(firstUser.firstName).toBe('John');
            expect(firstUser).toHaveProperty('lastName');
            expect(firstUser.lastName).toBe('Wayne');
            done();
          });
      });
      it('should respond with a list of filtered data', (done) => {
        // Jest will wait until the done callback is called before finishing the test

        return request(app.getHttpServer())
          .get('/users?limit=2')
          .end((err, res) => {
            if (err) done(err);
            expect(mockUsersService).toBeDefined();
            expect(mockUsersService).toHaveBeenCalled();
            expect(res.status).toBe(200);
            // expect(res.type).toEqual('application/json');
            expect(res.body).not.toBeNull();
            // expect(res.body).toEqual(expect.any(Object));
            expect(res.body).toEqual(expect.any(Array));
            expect(res.body).toHaveLength(2);
            expect(res.body).toBe(expectedData);
            done();
          });
      });
    });
    describe('and using invalid data', () => {
      it('should throw an error', (done) => {
        // Jest will wait until the done callback is called before finishing the test
        const expectedError = { message: 'Error' };
        const mockUsersService = jest
          .spyOn(usersService, 'findAll')
          .mockImplementationOnce(() => Promise.reject(expectedError));
        // mockedGetThirdData = jest.fn().mockResolvedValue(expectedError);

        return request(app.getHttpServer())
          .get('/users')
          .end((err, res) => {
            if (err) done(err);
            expect(mockUsersService).toBeDefined();
            expect(mockUsersService).toHaveBeenCalled();
            expect(res.status).toBe(500);
            // expect(res.type).toEqual('application/json');
            done();
          });
      });
    });
  });

  // CREATE
  describe('when running a create request', () => {
    describe('and using valid data', () => {
      const inputData: CreateUserDto = {
        email: 'user4@email.com',
        firstName: 'Jule',
        lastName: 'Wayne',
        phone: '+48123123124',
        description: 'Lorem ipsum 4',
      };

      const expectedData: Partial<UserInterface> = {
        id: '608ab7e27a84420dfc16b334', // Add a mocked ID
        ...inputData,
      };
      const mockUsersService = jest
        .spyOn(usersService, 'create')
        .mockImplementationOnce(() =>
          Promise.resolve(expectedData as UserInterface),
        );
      // mockedGetThirdData = jest.fn().mockResolvedValue(expectedData);

      it('should respond with the added data', (done) => {
        // Jest will wait until the done callback is called before finishing the test
        return request(app.getHttpServer())
          .post('/users') // .set({ Authorization: `Basic ${credentialInput}` })
          .send(inputData)
          .end((err, res) => {
            if (err) done(err);
            expect(mockUsersService).toBeDefined();
            expect(mockUsersService).toHaveBeenCalled();
            expect(res.status).toBe(201);
            expect(res.type).toEqual('application/json');
            // Check body data
            expect(res.body).not.toBeNull();
            expect(res.body).toEqual(expect.any(Object));
            expect(res.body).toBe(expectedData);
            // Check also user data
            // expect(res.body).toBeTruthy();
            expect(res.body).toHaveProperty('firstName');
            expect(res.body.firstName).toBe('Jule');
            expect(res.body).toHaveProperty('lastName');
            expect(res.body.lastName).toBe('Wayne');
            done();
          });
      });
    });
    describe('and using invalid data', () => {
      it('should throw an error', (done) => {
        // Jest will wait until the done callback is called before finishing the test
        const inputInvalidData: Partial<CreateUserDto> = {
          email: '',
        };
        const expectedError = { message: 'Error' };
        const mockUsersService = jest
          .spyOn(usersService, 'create')
          .mockImplementationOnce(() => Promise.reject(expectedError));
        // mockedGetThirdData = jest.fn().mockResolvedValue(expectedError);

        return request(app.getHttpServer())
          .post('/users') // .set({ Authorization: `Basic ${credentialInput}` })
          .send(inputInvalidData)
          .end((err, res) => {
            if (err) done(err);
            expect(mockUsersService).toBeDefined();
            expect(mockUsersService).toHaveBeenCalled();
            expect(res.status).toBe(500);
            // expect(res.type).toEqual('application/json');
            done();
          });
      });
    });
  });

  // READ
  describe('when running a read request', () => {
    describe('and using valid data', () => {
      const expectedData: Partial<UserInterface> = {
        id: '608ab7e27a84420dfc16b334',
        email: 'user4@email.com',
        firstName: 'Jule',
        lastName: 'Wayne',
        phone: '+48123123124',
        description: 'Lorem ipsum 4',
      };
      const mockUsersService = jest
        .spyOn(usersService, 'findOne')
        .mockImplementationOnce(() =>
          Promise.resolve(expectedData as UserInterface),
        );
      // mockedGetThirdData = jest.fn().mockResolvedValue(expectedData);
      it('should respond with the selected data', (done) => {
        // Jest will wait until the done callback is called before finishing the test
        return request(app.getHttpServer())
          .get(`users/${expectedData.id}`)
          .end((err, res) => {
            if (err) done(err);
            expect(mockUsersService).toBeDefined();
            expect(mockUsersService).toHaveBeenCalled();
            expect(res.status).toBe(200);
            expect(res.type).toEqual('application/json');
            // Check body data
            expect(res.body).not.toBeNull();
            expect(res.body).toEqual(expect.any(Object));
            expect(res.body).toBe(expectedData);
            // Check also user data
            // expect(res.body).toBeTruthy();
            expect(res.body).toHaveProperty('firstName');
            expect(res.body.firstName).toBe('Jule');
            expect(res.body).toHaveProperty('lastName');
            expect(res.body.lastName).toBe('Wayne');
            done();
          });
      });
    });
    describe('and using invalid data', () => {
      it('should throw an error', (done) => {
        // Jest will wait until the done callback is called before finishing the test
        const inputInvalidData: Partial<UserInterface> = {
          id: '0000',
        };
        const expectedError = { message: 'Error' };
        const mockUsersService = jest
          .spyOn(usersService, 'findOne')
          .mockImplementationOnce(() => Promise.reject(expectedError));
        // mockedGetThirdData = jest.fn().mockResolvedValue(expectedError);

        return request(app.getHttpServer())
          .get(`users/${inputInvalidData.id}`) // .set({ Authorization: `Basic ${credentialInput}` })
          .send(inputInvalidData)
          .end((err, res) => {
            if (err) done(err);
            expect(mockUsersService).toBeDefined();
            expect(mockUsersService).toHaveBeenCalled();
            expect(res.status).toBe(404);
            // expect(res.type).toEqual('application/json');
            done();
          });
      });
    });
  });

  // UPDATE
  describe('when running an update request', () => {
    describe('and using valid data', () => {
      const inputData: Partial<UpdateUserDto> = {
        firstName: 'new name',
      };

      const expectedData: Partial<UserInterface> = {
        id: '608ab7e27a84420dfc16b334',
        email: 'user4@email.com',
        firstName: inputData.firstName,
        lastName: 'Wayne',
        phone: '+48123123124',
        description: 'Lorem ipsum 4',
      };

      const mockUsersService = jest
        .spyOn(usersService, 'update')
        .mockImplementationOnce(() =>
          Promise.resolve(expectedData as UserInterface),
        );
      // mockedGetThirdData = jest.fn().mockResolvedValue(expectedData);

      it('should respond with the updated data', (done) => {
        // Jest will wait until the done callback is called before finishing the test
        return request(app.getHttpServer())
          .put('/users') // .set({ Authorization: `Basic ${credentialInput}` })
          .send(inputData)
          .end((err, res) => {
            if (err) done(err);
            expect(mockUsersService).toBeDefined();
            expect(mockUsersService).toHaveBeenCalled();
            expect(res.status).toBe(202);
            expect(res.type).toEqual('application/json');
            // Check body data
            expect(res.body).not.toBeNull();
            expect(res.body).toEqual(expect.any(Object));
            expect(res.body).toBe(expectedData);
            // Check also user data
            // expect(res.body).toBeTruthy();
            expect(res.body).toHaveProperty('firstName');
            expect(res.body.firstName).toBe(inputData.firstName);
            expect(res.body).toHaveProperty('lastName');
            expect(res.body.lastName).toBe('Wayne');
            done();
          });
      });
    });
    describe('and using invalid data', () => {
      it('should throw an error', (done) => {
        // Jest will wait until the done callback is called before finishing the test
        const inputInvalidData: Partial<CreateUserDto> = {
          firstName: '',
        };
        const expectedError = { message: 'Error' };
        const mockUsersService = jest
          .spyOn(usersService, 'create')
          .mockImplementationOnce(() => Promise.reject(expectedError));
        // mockedGetThirdData = jest.fn().mockResolvedValue(expectedError);

        return request(app.getHttpServer())
          .put('/users') // .set({ Authorization: `Basic ${credentialInput}` })
          .send(inputInvalidData)
          .end((err, res) => {
            if (err) done(err);
            expect(mockUsersService).toBeDefined();
            expect(mockUsersService).toHaveBeenCalled();
            expect(res.status).toBe(500);
            // expect(res.type).toEqual('application/json');
            done();
          });
      });
    });
  });

  // DELETE
  describe('when running a delete request', () => {
    describe('and using valid data', () => {
      const expectedData: Partial<UserInterface> = {
        id: '608ab7e27a84420dfc16b334',
        email: 'user4@email.com',
        firstName: 'Jule',
        lastName: 'Wayne',
        phone: '+48123123124',
        description: 'Lorem ipsum 4',
      };
      const mockUsersService = jest
        .spyOn(usersService, 'remove')
        .mockImplementationOnce(() =>
          Promise.resolve(expectedData as UserInterface),
        );
      // mockedGetThirdData = jest.fn().mockResolvedValue(expectedData);

      it('should respond with the selected data', (done) => {
        // Jest will wait until the done callback is called before finishing the test
        return request(app.getHttpServer())
          .delete(`users/${expectedData.id}`) // .set({ Authorization: `Basic ${credentialInput}` })
          .end((err, res) => {
            if (err) done(err);
            expect(mockUsersService).toBeDefined();
            expect(mockUsersService).toHaveBeenCalled();
            expect(res.status).toBe(204);
            expect(res.type).toEqual('application/json');
            // Check body data
            expect(res.body).not.toBeNull();
            expect(res.body).toEqual(expect.any(Object));
            expect(res.body).toBe(expectedData);
            // Check also user data
            // expect(res.body).toBeTruthy();
            expect(res.body).toHaveProperty('firstName');
            expect(res.body.firstName).toBe('Jule');
            expect(res.body).toHaveProperty('lastName');
            expect(res.body.lastName).toBe('Wayne');
            done();
          });
      });
    });
    describe('and using invalid data', () => {
      it('should throw an error', (done) => {
        // Jest will wait until the done callback is called before finishing the test
        const inputInvalidData: Partial<UserInterface> = {
          id: '0000',
        };
        const expectedError = { message: 'Error' };
        const mockUsersService = jest
          .spyOn(usersService, 'remove')
          .mockImplementationOnce(() => Promise.reject(expectedError));
        // mockedGetThirdData = jest.fn().mockResolvedValue(expectedError);

        return request(app.getHttpServer())
          .delete(`users/${inputInvalidData.id}`) // .set({ Authorization: `Basic ${credentialInput}` })
          .end((err, res) => {
            if (err) done(err);
            expect(mockUsersService).toBeDefined();
            expect(mockUsersService).toHaveBeenCalled();
            expect(res.status).toBe(404);
            // expect(res.type).toEqual('application/json');
            done();
          });
      });
    });
  });
});
