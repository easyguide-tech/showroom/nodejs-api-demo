// import { UserInterface } from "../../../models/interfaces/user.interface";

const mockedUser = {
  id: 1,
  email: 'user@email.com',
  firstName: 'John',
  lastName: 'Wayne',
  phone: '+48123123123',
  description: 'Lorem ipsum',
};

export default mockedUser;
