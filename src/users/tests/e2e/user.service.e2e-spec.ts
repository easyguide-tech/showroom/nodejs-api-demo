import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '../../users.service';
import request from 'supertest';
// import mockedUser from './mocks/user.mock';
import mockedUsers from './mocks/users.mock.json';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { UserModel } from 'src/users/models/schemas/user.schema';

describe('Users service', () => {
  let usersService: UsersService;
  let expectedData: any;
  // let mockedGetThirdData: jest.Mock;

  beforeEach(async () => {
    expectedData = mockedUsers;

    /*
    const mockedThirdService = {
      getThirdData: jest.fn().mockResolvedValue(mockedUsers),
      // getThirdData: jest.fn().mockReturnValue(Promise.resolve(mockedUsers)),
      // getThirdData: jest.fn().mockReturnValue(Promise.reject(mockedUsers)),
      // getThirdData: mockedGetThirdData
    }
    */

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        // CoreModule,
        // ThirdModule,
      ],
      providers: [
        UsersService,
        /*
        {
            provide: ThirdService,
            useValue: mockedThirdService
        },
        */
      ],
    })
      //.overrideProvider(ThirdService)
      //.useValue(mockedThirdService)
      .compile();
    usersService = await moduleFixture.get<UsersService>(UsersService);
  });

  afterEach(() => {
    // To mockRestore the spy
    // You can use mockImplementationOnce() instead of mockImplementation to not having to mockRestore the spy.
    jest.restoreAllMocks();
  });

  // QUERY
  describe('when running a query service', () => {
    const paginationQuery: PaginationQueryDto = {
      limit: 10,
      offset: 1,
      keyword: '',
    };
    describe('and using valid data', () => {
      it('should return a list of data', async () => {
        const mockUserModel = jest
          .spyOn(UserModel.prototype, 'get')
          .mockImplementationOnce(() => Promise.resolve(expectedData) as any);
        // mockedGetThirdData = jest.fn().mockResolvedValue(expectedData);

        const usersResult = await usersService.findAll(paginationQuery);
        expect(mockUserModel).toBeDefined();
        // expect(mockUserModel).toHaveBeenCalled();
        expect(mockUserModel).toBeCalledTimes(1);

        // Check body data
        expect(usersResult).not.toBeNull();
        // expect(usersResult).toEqual(expect.any(Object));
        expect(usersResult).toEqual(expect.any(Array));
        expect(usersResult).toHaveLength(3);
        expect(usersResult).toBe(expectedData);

        // Check also first data
        const firstUser = usersResult.find(
          (user: any) => user.email === 'user@email.com',
        );
        expect(firstUser).toBeTruthy();
        expect(firstUser).toHaveProperty('firstName');
        expect(firstUser.firstName).toBe('John');
        expect(firstUser).toHaveProperty('lastName');
        expect(firstUser.lastName).toBe('Wayne');
      });
      it('should respond with a list of filtered data', async () => {
        // Jest will wait until the done callback is called before finishing the test

        paginationQuery.limit = 2;
        const mockUserModel = jest
          .spyOn(UserModel.prototype, 'get')
          .mockImplementationOnce(() => Promise.resolve(expectedData) as any);
        const usersResult = await usersService.findAll(paginationQuery);

        expect(mockUserModel).toBeDefined();
        expect(mockUserModel).toHaveBeenCalled();
        // expect(res.type).toEqual('application/json');
        expect(usersResult).not.toBeNull();
        // expect(usersResult).toEqual(expect.any(Object));
        expect(usersResult).toEqual(expect.any(Array));
        expect(usersResult).toHaveLength(2);
        expect(usersResult).toBe(expectedData);
      });
    });
    describe('and using invalid data', () => {
      it('should throw an error', async () => {
        // Jest will wait until the done callback is called before finishing the test
        const expectedError = { message: 'Error' };
        const mockUserModel = jest
          .spyOn(UserModel.prototype, 'get')
          .mockImplementationOnce(() => Promise.reject(expectedError) as any);
        // mockedGetThirdData = jest.fn().mockResolvedValue(expectedError);

        expect(mockUserModel).toBeDefined();
        expect(mockUserModel).toHaveBeenCalled();
        await expect(usersService.findAll(paginationQuery)).rejects.toThrow();
      });
    });
  });
});
