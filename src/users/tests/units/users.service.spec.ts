import { Test, TestingModule } from '@nestjs/testing';

import { UsersController } from '../../users.controller';
import { UsersService } from '../../users.service';

describe('UsersService', () => {
  let service: UsersService;

  beforeEach(async () => {
    const usersModule: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
    }).compile();

    service = usersModule.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    const loggerSpy = jest.spyOn((service as any).logger, 'debug'); // you can also add on mockResponse type functions here like mockReturnValue and mockResolvedValue
    expect(loggerSpy).toHaveBeenCalled();
    expect(service).toBeDefined();
  });
});
