import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WinstonModule } from 'nest-winston';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { FakeThirdAppModule } from './fake-third-app/fake-third-app.module';
import { LoggerConfig } from './logger.config';

const logger: LoggerConfig = new LoggerConfig();

@Module({
  imports: [
    /*
    MongooseModule.forRootAsync({
      useFactory: () => ({
        uri: 'mongodb://127.0.0.1:27017/nest',
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true,
      }),
    }),
    UsersModule,*/
    WinstonModule.forRoot(logger.console()),
    FakeThirdAppModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
