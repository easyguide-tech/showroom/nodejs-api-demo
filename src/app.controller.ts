import {
  Controller,
  HttpStatus,
  Get,
  Inject,
  Injectable,
  Param,
  Post,
  Req,
  Res,
} from '@nestjs/common';
import { AppService } from './app.service';
import { join } from 'path';
import { Response, NextFunction, Request } from 'express';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';
import { ApiOperation } from '@nestjs/swagger';

@Controller()
export class AppController {
  errorContext: string;
  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
    private readonly appService: AppService,
  ) {
    this.errorContext = 'AppModule.AppController';
  }

  @Get('/')
  @ApiOperation({ description: 'Default home page' })
  getHello(@Req() req: Request, @Res() res: Response) {
    try {
      this.logger.debug(`${this.errorContext}.getHello().begun`);
      return this.appService.getHello();
    } catch (err) {
      const errorMessage = `${this.errorContext}.getHello().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: getHello not working ! ${errorMessage}`,
        status: 500,
      });
    }
  }

  // Browser
  @Get('/docs')
  @ApiOperation({ description: 'Redirect to /docs/compodoc' })
  getDocs(@Req() req: Request, @Res() res: Response) {
    try {
      this.logger.debug(`${this.errorContext}.getDocs().begun`);
      const SWAGGER_ENVS = ['local', 'dev', 'staging'];
      // console.debug(process.env.NODE_ENV);
      if (SWAGGER_ENVS.includes(process.env.NODE_ENV)) {
        return res.redirect(`/docs/compodoc`);
      }
    } catch (err) {
      const errorMessage = `${this.errorContext}.getDocs().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: getDocs not working ! ${errorMessage}`,
        status: 500,
      });
    }
  }

  @Get('/ping')
  @ApiOperation({ description: 'Ping request' })
  getPing(@Res() res: Response) {
    try {
      this.logger.debug(`${this.errorContext}.getPing().begun`);
      return res.json({ ping: 'pong' });
    } catch (err) {
      const errorMessage = `${this.errorContext}.getPing().failed: ${err.message}`;
      this.logger.error(errorMessage);
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: `Error: Ping Pong not working ! ${errorMessage}`,
        status: 500,
      });
    }
  }
}
