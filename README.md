Nodejs demo (NestJs and BDD)
============================

EasyGuide Nodejs boilerplate  (NestJs and BDD) 
- Powered by EasyGuide.tech Inc
- version 0.1
> **Alert note:**  This application is for demonstration purpose only and it is not intended for use in a specific production. It is too generic and not designed to be particularly efficient, stable, or secure. It does not support all the required features and specific needs of a complete production-ready solution !

---
Website : https://easyguide.tech

![EasyTech_IDE_handbook.jpg](EasyGuide_handbook.jpg)

---
**TABLE OF CONTENTS**
---

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Overview](#overview)
- [Installation](#installation)
- [Running the app](#running-the-app)
- [Test](#test)
- [BDD Test](#bdd-test)
- [Code source documentation (With compodoc)](#code-source-documentation-with-compodoc)
- [OpenApi/Swagger documentation](#openapiswagger-documentation)
- [Async Api / Kafka documentation (Optional)](#async-api-kafka-documentation-optional)
- [Fake third app and server-side render view with handlebar](#fake-third-app-and-server-side-render-view-with-handlebar)
- [Use our Custom Logger service](#use-our-custom-logger-service)
- [CI / CD](#ci-cd)
- [Support](#support)
- [Source](#source)
- [Copyright and License](#copyright-and-license)

<!-- /code_chunk_output -->

---

## Features

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

A progressive Node.js framework for building efficient and scalable server-side applications with BDD tools.

![preview-demo.gif](preview-demo.gif)



| Method      | Endpoint                    |
|-------------|-----------------------------|
| **Default**                               |
| GET         | /                           |
| GET         | /docs                       |
| GET         | /ping                       |
| **Backend fake third app**                |
| GET         | /units                      |
| POST        | /units                      |
| GET         | /units/{id}                 |
| PATCH       | /units/{id}                 |
| PUT         | /units/{id}                 |
| DELETE      | /units/{id}                 |
| **Front fake third app** |                |
| GET         | /front/assets/{imgId}       |
| GET         | /front/units/new            |
| GET         | /front/units/{id}/edit      |
| POST        | /front/units/{id}/update    |
| GET         | /front/units/{id}/delete    |
| GET         | /front/units/{id}           |
| GET         | /front/units                |
| POST        | /front/units                |
| GET         | /front/login                |
| POST        | /front/login                |
| GET         | /front/register             |
| POST        | /front/register             |
| GET         | /front/success              |
| GET         | /front/failed               |
| GET         | /front                      |

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov

```

## BDD Test
```sh
# acceptance tests
$ npm run bdd:test

# Refresh acceptance test coverage
$ npm run bdd:report
```

## Code source documentation (With compodoc)
This documentation is automatically generated and available at `/docs/compodoc` url

the command script for the generation is `npm run compodoc`

* **Note:** All the technical documentations of this application can be found by entering this main url `/docs/compodoc`
  * Async API / Kafka
  * BDD / Cucumber
  * OpenAPI / Swagger
  * Test reports

## OpenApi/Swagger documentation
This documentation is automatically generated and available at `/api` url

* Edit the `src\main.ts`  fie to update the title and description of the swagger documentation
```ts
const config = new DocumentBuilder()
      .setTitle('API Docs')
      .setDescription('The API documentation')
      .setVersion('1.0')
      //.addTag('Nestjs starter')
      .build();
```

* Use the tag `@ApiTags()` to specify a section in api documentation:
```ts
@ApiTags('backend fake third app')
@Controller('units')
// ...
```
* **Note:** Documentation will not detect your interface type definition as DTO or Entity (Typescript issue)
You must declare them as a class and not an interface. See more info  in this link : [https://github.com/nestjs/nest/issues/339]

```ts
import { ApiProperty } from '@nestjs/swagger';

export class UnitEntity {
  @IsString()
  // You can use the @ApiProperty tag to add some example and description in swagger documentation
  @ApiProperty({ example: '608ab7e27a84420dfc16b335', description: 'The ID' })
  id: string;
}
```
* **Notes:** Use the `@ApiProperty` tag to add some example and description for the swagger documentation of entities

* **Note:** Swagger doc `/api` url is not available in production mode for security reason


## Async Api / Kafka documentation (Optional)
You will have to manualy edit the `docs\asyncapi-kafka\asyncapi-spec.yml` file

the command script for the generation is `npm run asyncapi-doc` and is already included in the command `npm run compodoc` so this documentation is automatically generated and available at `/docs/compodoc` url

## Fake third app and server-side render view with handlebar
An optional module `fake-third-app` can be used to simulate a third api or third app with fake data and server-side frontend render view (using handlebars template engine)

## Use our Custom Logger service

* **Insert:**
```ts
@Injectable()
export class MyService {
  errorContext: string;
  units: UnitsDto;

  constructor(
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
  ) {
    this.errorContext = 'My-api.MyModule.MyService';
  }
```
* **Use:**
```ts
try {
  this.logger.info(`${this.errorContext}.getUnits().begun`);
  //...
  if (!units){
    throw Error(`Units data loading failed`);
  }
} catch (err) {
  const errorMessage: string = `${this.errorContext}.getUnits().failed: ${err}`;
  this.logger.error(errorMessage);
  throw errorMessage;
}
```

* **Note:** For more advanced logging functionality, you can make use of any Node.js logging package, such as Winston, to implement a completely custom, production grade logging system.

## CI / CD
See []()


## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Source

- Github: https://github.com/nestjs/typescript-starter
- Author - [Kamil Myśliwiec](https://twitter.com/kammysliwiec)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## Copyright and License

* This project is released under the MIT expat license and the EasyGuide.tech EE LICENSE
- see the [LICENSE.md](LICENSE.md) file for details.
- see the [EE-LICENSE.md](ee/EE-LICENSE.md) file for details.

* **Note:** Nest is [MIT licensed](https://github.com/nestjs/nest/blob/master/LICENSE).

The package is Open core Source released under the [MIT Expat](LICENSE) with DCO requirement for contributing (Developer Certificate of Origin).

It is developed by cheikhna diouf and derived from his official book "Easy Guide ! A new method" which you can buy as printed book or as ebook on official website, Amazon, Google Play and the iBooks Store.

![EasyGuide_Collection.jpg](EasyGuide_Collection.jpg)

