Feature: Backend Query data
  As an api
  I should be able to query and/or filter a data list

  Scenario: Query units
    Given The data list "units"
    When I send GET request to "/units"
    Then I should receive a data list which is not empty

  Scenario: Query units with filters
    Given The data list "units"
    And I provide below sample data
      | keyword | label 3 |
    When I send GET request to "/units"
    Then I should receive a data list which is not empty
