Feature: Backend Create data
  As an api
  I should be able to create a new data

  Scenario: Create a new unit
      Given I provide below sample data
        | code        | random |
        | label       | random |
        | photo       | random | 
      When I send POST request to "/units"
      Then I get response code 201
      