Feature: Backend Delete data
  As an api
  I should be able to delete an existing data

  Scenario: Delete an existing unit
    Given I provide below sample data 
      | code        | random |
      | label       | random |
      | photo       | random | 
    When I send POST request to "/units"
    Then I get response code 201
    Given The new data added exist with a new id
    When I send DELETE request to "/units"
    Then I get response code 200
