Feature: Backend Read data
  As an api
  I should be able to read an existing data

  Scenario: Read a unit
    Given The item with below data
      | id | 608da27a23ec6f382c27a644 |
    When I send GET request to "/units" with the given data
    Then I receive below data 
      | code        | fake code 3                     |
      | label       | fake label 3                    |
      | photo       | assets/images/icon-gitlab.png   |
