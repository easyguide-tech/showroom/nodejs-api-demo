Feature: Backend Update data
  As an api
  I should be able to update an existing data

  Scenario: Update an existing unit
    Given The item with below data
      | id | 608d89f923ec6f382c27a642 |
    When I send PATCH request to "/units" with below data
      | code | fake code 0 |
    Then I get response code 200
