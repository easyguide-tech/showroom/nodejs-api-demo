Feature: Backend Ping
  As a user or third api
  I should be able to ping the app

  Scenario: As a user or api I can ping
    When I send PING request to "/ping"
    Then I get PING response code 200

