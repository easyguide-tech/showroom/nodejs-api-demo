Feature: Frontend Create data
  As a user
  I should be able to create a new data
 
  @browser
  Scenario: Create Unit
    Given I am on the "list" page
    When I click the "new-btn" button
    Then I should be on the "form" page

    Given I have the "empty" form
    And I enter below sample data as inputs form
      | code    | label  | photo  |
      | random  | random | random |
    When I click the "submit-btn" button
    Then I should be on the "detail" page with below data
      | id          | random |
      | code        | random |
      | label       | random |
      | photo       | random |
    And I should see a success message
