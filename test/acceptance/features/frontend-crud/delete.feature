Feature: Frontend Delete data
  As a user
  I should be able to delete an existing data

  @browser
  Scenario: Delete an existing unit from detail page
    Given I am on the "list" page
    When I click the "new-btn" button
    Then I should be on the "form" page

    Given I have the "empty" form
    And I enter below sample data as inputs form
      | code    | label  | photo  |
      | random  | random | random |
    When I click the "submit-btn" button
    Then I should be on the "detail" page with below data
      | id          | random |
      | code        | random |
      | label       | random |
      | photo       | random |
    When I click the "delete-btn" button
    Then I should be on the "list" page
    And I should see a success message


  # @browser
  # Scenario: Delete an existing unit from list page
  #   Given I am on the "list" page
  #   And The item with below data
  #     | id | 608d89f923ec6f382c27a642 |
  #   When I click the "delete-btn-608d89f923ec6f382c27a642" button
  #   Then I should be on the "list" page
  #   And I should see a success message
