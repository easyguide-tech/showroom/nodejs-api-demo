Feature: Frontend Update data
As a user
I should be able to update an existing data

  @browser
  Scenario: Update an existing unit
    Given I am on the "list" page
    And The item with below data
      | id | 608d89f923ec6f382c27a642 |
    When I click the "form-btn-608d89f923ec6f382c27a642" button
    Then I should be on the "form" page

    Given I have the "filled" form
    And I enter below sample data as inputs form
      | code    | label  | photo  |
      | random  | random | random |
    When I click the "submit-btn" button
    Then I should be on the "detail" page with below data
      | id          | random |
      | code        | random |
      | label       | random |
      | photo       | random |
    And I should see a success message
