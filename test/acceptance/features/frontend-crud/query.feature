Feature: Frontend Query data
  As a user
  I should be able to query and/or filter a data list
  
  @browser
  Scenario: Query units
    Given I am on the "home" page
    When I click the "list-btn" button
    Then I should be on the "list" page
    And I should see a success message

  @browser
  Scenario: Query units with filters
    Given I am on the "list" page
    And I enter "label 3" as my "keyword"
    When I click the "search-btn" button
    Then I should be on the "list" page
    And I should see a success message
