Feature: Frontend Read new data
  As a user
  I should be able to read an existing data

  @browser
  Scenario: Read an existing unit
    Given I am on the "list" page
    And The item with below data
      | id | 608da27a23ec6f382c27a644 |
    When I click the "detail-btn-608da27a23ec6f382c27a644" button
    And I send GET request to "/units" with the given data
    Then I should be on the "detail" page with below data
      | id          | 608da27a23ec6f382c27a644       |
      | code        | fake code 3                    |
      | label       | fake label 3                   |
      | photo       | assets/images/icon-gitlab.png  |
    And I should see a success message
