Feature: Frontend Report
  As a user 
  I should be able to consult the BDD html report of the app

  Scenario: As a user I can get BDD report
    When I am at the end of the BDD tests
    Then I should consult the report page

