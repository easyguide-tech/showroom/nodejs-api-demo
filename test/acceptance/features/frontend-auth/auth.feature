Feature: Frontend authentication
  As a user 
  I should be able to login to the app
  Otherwise I should be shown a failed page.
  
  @browser
  Scenario: As a user I can login
    Given I am on the "login" page
    And I enter "foo" as my "name"
    And I enter "bar" as my "password"
    When I click the "login-btn" button
    Then I should be on the "success" page

  @browser
  Scenario: As a user I can register
    Given I am on the "register" page
    And I enter "foo" as my "name"
    And I enter "bar" as my "password"
    And I enter "bar" as my "confirm_password"
    When I click the "register-btn" button
    Then I should be on the "login" page
