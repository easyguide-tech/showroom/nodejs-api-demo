import { faker } from '@faker-js/faker';

export class RandomData {
  public data = {};
  constructor() {
    this.data = {
      userName: faker.internet.userName(),

      firstName: faker.person.firstName(),
      lastName: faker.person.lastName(),
      phone: faker.phone.number(),
      addressLine: faker.location.street(),
      postCode: faker.number.int(9999999),
      city: faker.location.city(),
      email: faker.internet.email(),

      code: faker.lorem.words() + faker.number.int(100),
      label: faker.person.fullName(),
      photo: faker.image.url(),
      active: faker.helpers.shuffle([true, false])[0],

      keyword: faker.lorem.word(),
    };
  }
}

export const staticRandomData = new RandomData().data;
