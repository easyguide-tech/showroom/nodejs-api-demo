import { RandomData } from './faker';

export const getFakerData = (args) => {
  const data = { ...args };
  const randomData = new RandomData().data;
  Object.keys(data).forEach((val) => {
    if (data[val] === 'random') {
      data[val] = randomData[val];
    }
  });
  return data;
};

export const getExistingData = (existant, args) => {
  const data = { ...args };
  const randomData = new RandomData().data;
  Object.keys(data).forEach((val) => {
    if (data[val] === 'random') {
      data[val] = existant[val];
    }
  });
  return data;
};
