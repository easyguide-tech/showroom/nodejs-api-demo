import { Given, When, Then } from '@cucumber/cucumber';
import { getFakerData } from '../../data/data-faker';
import { CustomWorld } from '../../support/world';

import * as restHelper from '../../util/restHelper';
import { strict as assert } from 'assert';

// GIVEN
Given(
  'The item with below data',
  { timeout: 60 * 1000 },
  function (this: CustomWorld, sampleData) {
    try {
      const keyVal = sampleData.raw();
      this.context['request'] = keyVal[0][1];
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Given(
  'I provide below sample data',
  { timeout: 60 * 1000 },
  function (this: CustomWorld, sampleData) {
    try {
      this.context['request'] = getFakerData(sampleData.rowsHash()); // sampleData.hashes()[0] // if reverse dataTable
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Given(
  'The data list {string}',
  { timeout: 60 * 1000 },
  function (this: CustomWorld, resourceName) {
    try {
      this.context['resourceName'] = resourceName;
      assert.ok(this.context['resourceName'] != '');
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Given(
  'The new data added exist with a new id',
  { timeout: 60 * 1000 },
  function (this: CustomWorld) {
    try {
      this.context['id'] = this.context['response'].data.id;
      assert.ok(this.context['id'] != '');
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

// WHEN
When(
  'I send POST request to {string}',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, path) {
    try {
      this.context['response'] = await restHelper.postData(
        `${process.env.API_URL}${path}`,
        this.context['request'],
      );
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

When(
  'I send GET request to {string}',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, path) {
    try {
      let queries = '';
      if (this.context['request']) {
        for (const [key, value] of Object.entries(this.context['request'])) {
          queries += `${key}=${value}&`;
        }
      }
      const response = await restHelper.getData(
        `${process.env.API_URL}${path}?${queries}`,
      );
      this.context['response'] = response;
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

When(
  'I send GET request to {string} with the given data',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, path) {
    try {
      const response = await restHelper.getData(
        `${process.env.API_URL}${path}/${this.context['request']}`,
      );
      this.context['response'] = response;
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

When(
  'I send PATCH request to {string} with below data',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, path, sampleData) {
    try {
      const response = await restHelper.patchData(
        `${process.env.API_URL}${path}/${this.context['request']}`,
        sampleData.rowsHash(), // sampleData.hashes()[0] // if reverse dataTable
      );
      this.context['response'] = response;
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

When(
  'I send DELETE request to {string}',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, path) {
    try {
      const response = await restHelper.deleteData(
        `${process.env.API_URL}${path}/${this.context['id']}`,
      );
      this.context['response'] = response;
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

// THEN
Then(
  'I get response code {int}',
  { timeout: 60 * 1000 },
  function (this: CustomWorld, code) {
    try {
      assert.equal(this.context['response'].status, code);
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Then(
  'I receive below data',
  { timeout: 60 * 1000 },
  function (this: CustomWorld, sampleData) {
    try {
      const sampleRecord = sampleData.rowsHash(); // sampleData.hashes()[0] // if reverse dataTable
      let result: boolean = true;
      Object.keys(sampleRecord).forEach((key: any) => {
        const key2 = this.context['response'].data[key];
        if (sampleRecord[key] != key2) {
          result = false;
        }
      });
      assert.ok(result == true);
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Then(
  'I should receive a data list which is not empty',
  { timeout: 60 * 1000 },
  function (this: CustomWorld) {
    try {
      const length: number = this.context['response'].data.results.length;
      assert.ok(length > 0);
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);
