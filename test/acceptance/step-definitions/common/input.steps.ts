import { Given, When, Then } from '@cucumber/cucumber';
import { CustomWorld } from '../../support/world';
import { staticRandomData } from '../../data/faker';
import { getFakerData } from '../../data/data-faker';

Given(
  'I have the {string} form',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, name) {
    try {
      const el = await this.page.waitForSelector(`[data-test="${name}"]`);
      return !!el;
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Given(
  'I enter {string} as my {string}',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, val, key) {
    try {
      const el = await this.page.waitForSelector(`[data-test="${key}"]`);
      await el.click({ clickCount: 3 });
      if (val == 'random') val = staticRandomData[key];
      await el.type(val);
      return el;
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Given(
  'I enter below sample data as inputs form',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, sampleData) {
    try {
      this.context['request'] = getFakerData(sampleData.hashes()[0]); // sampleData.rowsHash() // if reverse dataTable

      if (this.context['request']) {
        for (const [key, val] of Object.entries(this.context['request'])) {
          const el = await this.page.waitForSelector(`[data-test="${key}"]`);
          await el.click({ clickCount: 3 });
          let value;
          if (val == 'random') {
            value = staticRandomData[key];
            this.context['request'][key] = value;
            await el.type(val);
          } else {
            await el.type(val);
          }
          // return el;
        }
      }
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);
