import { Then, Given } from '@cucumber/cucumber';
import { strict as assert } from 'assert';
import { getFakerData, getExistingData } from '../../data/data-faker';
import { CustomWorld } from '../../support/world';

Given(
  'I am on the {string} page',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, pageName) {
    try {
      switch (pageName) {
        case 'home':
          return await this.page
            .goto(`${process.env.APP_URL}/front`, {
              waitUntil: 'domcontentloaded',
            })
            .catch((error) => {
              throw 'The following error occurred: ' + error; // <-- pass the error to main program
            });
        case 'login':
          return await this.page
            .goto(`${process.env.APP_URL}/front/login`, {
              waitUntil: 'domcontentloaded',
            })
            .catch((error) => {
              throw 'The following error occurred: ' + error; // <-- pass the error to main program
            });
        case 'register':
          return await this.page
            .goto(`${process.env.APP_URL}/front/register`, {
              waitUntil: 'domcontentloaded',
            })
            .catch((error) => {
              throw 'The following error occurred: ' + error; // <-- pass the error to main program
            });
        case 'list':
          return await this.page
            .goto(`${process.env.APP_URL}/front/units`, {
              waitUntil: 'domcontentloaded',
            })
            .catch((error) => {
              throw 'The following error occurred: ' + error; // <-- pass the error to main program
            });
        case 'report':
          return await this.page
            .goto(`${process.env.APP_URL}/report`, {
              waitUntil: 'domcontentloaded',
            })
            .catch((error) => {
              throw 'The following error occurred: ' + error; // <-- pass the error to main program
            });
        default:
          throw new Error(
            `${pageName} is not a predefined page name, define it first in steps definition`,
          );
      }
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Then(
  'I should be on the {string} page',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, string) {
    try {
      // await this.page.waitForNavigation({ waitUntil: 'networkidle1' });
      const el = await this.page.waitForSelector(`[data-test="${string}"]`);
      return !!el;
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Then(
  'I should be on the {string} page with below data',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, namePage, sampleData) {
    try {
      const expectedData = getExistingData(
        this.context['request'],
        sampleData.rowsHash(), // sampleData.hashes()[0] // if reverse dataTable
      );
      // await this.page.waitForNavigation({ waitUntil: 'networkidle1' });
      await this.page.waitForSelector(
        '[data-test="table-list"] tr, [data-test="detail-list"]',
        {
          visible: true,
        },
      );
      const elemData = await this.page.evaluate(() => {
        // Alert ! Do not use breakpoints here, Debugger can't inspect inside this block.
        const rows = document.querySelectorAll(
          '[data-test="table-list"] tr, [data-test="detail-list"]',
        );
        return Array.from(rows, (row) => {
          const columns = row.querySelectorAll('td, dd');
          return Array.from(columns, (column) => column.textContent);
        });
      });
      const elemDataDt = await this.page.evaluate(() => {
        // Alert ! Do not use breakpoints here, Debugger can't inspect inside this block.
        const rows = document.querySelectorAll(
          '[data-test="table-list"] tr, [data-test="detail-list"]',
        );
        return Array.from(rows, (row) => {
          const columnsDt = row.querySelectorAll('th, dt');
          return Array.from(columnsDt, (column) =>
            column.getAttribute('data-test'),
          );
        });
      });
      let checkit = false;
      elemData[0].forEach((item) => {
        let actualData = item;
        let checkit2 = true;
        elemDataDt[0].forEach((val: any, index: number) => {
          if (expectedData[val] && expectedData[val] != elemData[0][index]) {
            checkit2 = false;
          }
        });
        if (!checkit) checkit = checkit2;
      });
      const el = await this.page.waitForSelector(`[data-test="${namePage}"]`);
      if (!el) checkit = false;
      assert.ok(checkit);
      /*
      await this.page.on('response', async (response) => {
        console.debug('response code: ', await response.status());
        console.debug('response url: ', response.url());      
      });
      */
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Then(
  'I should see a success message',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld) {
    try {
      // await this.page.waitForNavigation({ waitUntil: 'networkidle1' });
      await this.page.waitForSelector('[data-test="message"]', {
        visible: true,
      });
      const elemData = await this.page.evaluate(() => {
        // Alert ! Do not use breakpoints here, Debugger can't inspect inside this block.
        const el = document.querySelector('[data-test="message"]');
        return el.textContent;
      });
      return elemData.includes('Success');
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Then(
  'I should see an error message',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld) {
    try {
      await this.page.waitForSelector('[data-test="message"]', {
        visible: true,
      });
      const elemData = await this.page.evaluate(() => {
        // Alert ! Do not use breakpoints here, Debugger can't inspect inside this block.
        const el = document.querySelector('[data-test="message"]');
        return el.textContent;
      });
      return elemData.includes('Error');
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);
