import { Given, When, Then } from '@cucumber/cucumber';
import { CustomWorld } from '../../support/world';

When(
  'I click the {string} button',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, buttonName) {
    try {
      const el = await this.page.$(`[data-test="${buttonName}"]`);
      await el.click();
      return el;
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

When(
  'I click the {string} button to {string} url api',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, buttonName, urlApi) {
    try {
      this.context['urlApi'] = urlApi;
      const el = await this.page.$(`[data-test="${buttonName}"]`);
      await el.click();
      return el;
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);
