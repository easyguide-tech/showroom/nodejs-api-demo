import { Given, When, Then } from '@cucumber/cucumber';
import { CustomWorld } from '../../support/world';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const assert = require('assert').strict;
import * as restHelper from '../../util/restHelper';

// WHEN
When(
  'I send PING request to {string}',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, path) {
    try {
      /*
      const navigationPromise = this.page.waitForNavigation({
        waitUntil: 'domcontentloaded',
      });
      await navigationPromise;
      */
      const response = await restHelper.getData(`http://localhost:3000${path}`);
      this.context['response'] = response;
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

// THEN

Then(
  'I get PING response code {int}',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld, code) {
    assert.equal(this.context['response'].status, code);
  },
);
