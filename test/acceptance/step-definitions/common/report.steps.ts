import { Given, When, Then } from '@cucumber/cucumber';
import { CustomWorld } from '../../support/world';
import _open from 'open';
import { strict as assert } from 'assert';

When(
  'I am at the end of the BDD tests',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld) {
    try {
      return assert.ok(true);
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);

Then(
  'I should consult the report page',
  { timeout: 60 * 1000 },
  async function (this: CustomWorld) {
    try {
      return await _open(`${process.env.APP_URL}/docs/bdd-cucumber/report`); // Opens the url in the default browser
    } catch (error) {
      throw 'The following error occurred: ' + error; // <-- pass the error to main program
    }
  },
);
