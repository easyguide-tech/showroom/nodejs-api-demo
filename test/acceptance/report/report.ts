const reporter = require('cucumber-html-reporter');

const options = {
  theme: 'bootstrap',
  jsonFile: 'test/acceptance/report/cucumber.json',
  output: 'docs/bdd-cucumber/report/index.html',
  reportSuiteAsScenarios: true,
  scenarioTimestamp: true,
  launchReport: false,
  name: 'Model micro Services',
  brandTitle: 'BDD Acceptance Tests',
  metadata: {
    'App Version': '0.1.0',
    'Test Environment': 'STAGING',
    Browser: 'Chrome  54.0.2840.98',
    Platform: 'PC',
    Parallel: 'Scenarios',
    Executed: 'Remote',
  },
};

reporter.generate(options);
