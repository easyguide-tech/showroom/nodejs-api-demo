import axios from 'axios';

const getData = (url) => {
  try {
    return axios.get(url).catch(function (error) {
      if (error.response) {
        // Request made and server responded
        throw `Response: data: ${JSON.stringify(
          error.response.data,
          null,
          4,
        )} |  status: ${error.response.status} |  headers: ${JSON.stringify(
          error.response.headers,
          null,
          4,
        )}`;
        /*
        console.debug(error.response.data);
        console.debug(error.response.status);
        console.debug(error.response.headers);
        */
      } else if (error.request) {
        // The request was made but no response was received
        throw `request: ${error.request}`;
        //console.debug(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        throw `message: ${error.message}`;
        //console.debug('Error', error.message);
      }
    });
  } catch (e) {
    console.debug('exception occurred while GET', e);
    throw e;
  }
};

const postData = (url, data) => {
  try {
    return axios.post(url, data).catch(function (error) {
      if (error.response) {
        // Request made and server responded
        throw `Response: data: ${JSON.stringify(
          error.response.data,
          null,
          4,
        )} |  status: ${error.response.status} |  headers: ${JSON.stringify(
          error.response.headers,
          null,
          4,
        )}`;
        /*
        console.debug(error.response.data);
        console.debug(error.response.status);
        console.debug(error.response.headers);
        */
      } else if (error.request) {
        // The request was made but no response was received
        throw `request: ${error.request}`;
        //console.debug(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        throw `message: ${error.message}`;
        //console.debug('Error', error.message);
      }
    });
  } catch (e) {
    console.debug('exception occurred while POST', e);
    throw e;
  }
};

const patchData = async (url, data) => {
  try {
    return await axios.patch(url, data).catch(function (error) {
      if (error.response) {
        // Request made and server responded
        throw `Response: data: ${JSON.stringify(
          error.response.data,
          null,
          4,
        )} |  status: ${error.response.status} |  headers: ${JSON.stringify(
          error.response.headers,
          null,
          4,
        )}`;
        /*
        console.debug(error.response.data);
        console.debug(error.response.status);
        console.debug(error.response.headers);
        */
      } else if (error.request) {
        // The request was made but no response was received
        throw `request: ${error.request}`;
        //console.debug(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        throw `message: ${error.message}`;
        //console.debug('Error', error.message);
      }
    });
  } catch (e) {
    console.debug('exception occurred while PATCH', e);
    throw e;
  }
};

const deleteData = async (url) => {
  try {
    return await axios.delete(url).catch(function (error) {
      if (error.response) {
        // Request made and server responded
        throw `Response: data: ${JSON.stringify(
          error.response.data,
          null,
          4,
        )} |  status: ${error.response.status} |  headers: ${JSON.stringify(
          error.response.headers,
          null,
          4,
        )}`;
        /*
        console.debug(error.response.data);
        console.debug(error.response.status);
        console.debug(error.response.headers);
        */
      } else if (error.request) {
        // The request was made but no response was received
        throw `request: ${error.request}`;
        //console.debug(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        throw `message: ${error.message}`;
        //console.debug('Error', error.message);
      }
    });
  } catch (e) {
    console.debug('exception occurred while DELETE', e);
    throw e;
  }
};

export { getData, postData, patchData, deleteData };
