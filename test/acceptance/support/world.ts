import { setWorldConstructor } from '@cucumber/cucumber';

export class CustomWorld {
  public context = {};
  public _browser: any;
  public page: any;
  public _scenario: any;
}

setWorldConstructor(CustomWorld);
