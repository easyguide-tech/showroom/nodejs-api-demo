import { Before, After, BeforeAll, Status, AfterAll } from '@cucumber/cucumber';
import { faker } from '@faker-js/faker';
import { CustomWorld } from './world';
import * as puppeteer from 'puppeteer';
// const puppeteer = require('puppeteer');

/*
BeforeAll(async function (this: CustomWorld) {
  // This hook will be executed before all scenarios
});
*/
Before(async function (this: CustomWorld, scenario) {
  // This hook will be executed before each scenarios tagged with this specific tag
  this._scenario = scenario;
});

Before({ tags: '@browser' }, async function (this: CustomWorld, scenario) {
  // This hook will be executed before each scenarios tagged with this specific tag

  // Create an instance of the chrome browser
  // But disable headless mode !
  const browser = await puppeteer.launch({
    headless: false,
    slowMo: 15, // slow down by 250ms
    product: 'chrome',
    executablePath: 'C:/Program Files/Google/Chrome/Application/chrome.exe',
    // 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe',
    // executablePath: 'C:/Program Files/Mozilla Firefox/firefox.exe',
    args: ['-wait-for-browser', '--disable-infobars', '--window-size=1200,800'],
    // dumpio: true,
    defaultViewport: null,
  });
  this._browser = browser;

  // Create a new page
  // this.page = await this._browser.newPage();
  const pages = await this._browser.pages();
  this.page = pages[0];

  // Configure the navigation timeout
  await this.page.setDefaultNavigationTimeout(0);

  // this.page.setViewport({ width: 768, height: 622 });

  this.page.goto(`${process.env.APP_URL}/front`, {
    waitUntil: 'domcontentloaded',
  });
});

/*
Before({ tags: '@mobile' }, async function (this: CustomWorld) {
  // This hook will be executed before each scenarios tagged with this specific tag
  this.page.setViewport({ width: 320, height: 466 });
});

Before({ tags: '@tablet' }, async function (this: CustomWorld) {
  // This hook will be executed before each scenarios tagged with this specific tag
  this.page.setViewport({ width: 768, height: 622 });
});
*/

After({ tags: '@browser' }, async function (this: CustomWorld, scenario) {
  // This hook will be executed after each scenarios tagged with this specific tag
  if (scenario.result?.status == Status.FAILED) {
    // await this.page.goto(url, {waitUntil: 'domcontentloaded'});
    await this.page.screenshot({
      path: `e2e/src/screenshots/frontend-error-${scenario.pickle.uri.endsWith(
        '/',
      )}-${faker.datatype.number(100)}.png`,
    });
  }
  return await this._browser.close();
});
