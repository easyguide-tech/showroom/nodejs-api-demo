AWS CLOUD SCALABLE INFRASTRUCTURE SERVICES
==================
Overview, Setup and Usages
---
**Configure CI/CD pipeline jobs** for: 
1. Managing AWS account user with **IAM service** and **AWS Cli**.
2. Building, pushing, checking code quality and testing Docker images to **AWS ECR** with **CodeBuild**.
3. Deploying applications to **AWS ECS** and Managing AWS infrastructure with **ECS Fargate** or **Terraform**. Using also scaling tools like **EC2 instances**, **Elastic Load Balancer (ELB)** and **RDS**.
4. Set up a domain name and secure your app with SSL using AWS Route 53 and AWS Certificate Manager, respectively.
5. Using **CloudWatch** monitoring tool

---
**TABLE OF CONTENTS**
---

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=2 orderedList=false} -->

<!-- code_chunk_output -->

- [1. Overview](#1-overview)
  - [Costs](#costs)
  - [AWS services used](#aws-services-used)
  - [All the routes](#all-the-routes)
- [2. IAM service](#2-iam-service)
  - [Create AWS account with IAM service and AWS CLI](#create-aws-account-with-iam-service-and-aws-cli)
  - [Install AWS CLI](#install-aws-cli)
  - [Create new IAM User](#create-new-iam-user)
  - [AWS Credential file](#aws-credential-file)
  - [AWS account](#aws-account)
- [3. ECR repository (Elastic Container Registry): Docker registry tool](#3-ecr-repository-elastic-container-registry-docker-registry-tool)
  - [Create online the repositories with immutable tags configured](#create-online-the-repositories-with-immutable-tags-configured)
  - [Build and push images to ECR](#build-and-push-images-to-ecr)
- [4. CodeBuild: CI tool for building and testing docker images](#4-codebuild-ci-tool-for-building-and-testing-docker-images)
  - [With the AWS console, navigate to the CodeBuild dashboard and click "Create project"](#with-the-aws-console-navigate-to-the-codebuild-dashboard-and-click-create-project)
  - [Create the buildspec.yml](#create-the-buildspecyml)
  - [ECS codebuild new revision to the task definition](#ecs-codebuild-new-revision-to-the-task-definition)
  - [Feature branch CodeBUild workflow](#feature-branch-codebuild-workflow)
- [5. Elastic Container Service (ECS): CD tool, Container orchestration service](#5-elastic-container-service-ecs-cd-tool-container-orchestration-service)
  - [EC2 instances, Elastic Load Balancer (ELB) and RDS : Scaling tool](#ec2-instances-elastic-load-balancer-elb-and-rds-scaling-tool)
  - [ECS Components infrastructure](#ecs-components-infrastructure)
  - [ECS Fargate and Terraform](#ecs-fargate-and-terraform)
- [6. Next steps](#6-next-steps)
- [7. CloudWatch: Monitoring tool](#7-cloudwatch-monitoring-tool)
- [Annexes](#annexes)
  - [Troubleshoots](#troubleshoots)
  - [Credits](#credits)

<!-- /code_chunk_output -->

<!-- /code_chunk_output -->

---

# 1. Overview
---
**Configure CI/CD pipeline jobs** for: 
- 1. Managing AWS account user with **IAM service** and **AWS Cli**.
- 2. Building, pushing, checking code quality and testing Docker images to **AWS ECR** with **CodeBuild**.
- 3. Deploying applications to **AWS ECS** and Managing AWS infrastructure with **ECS Fargate** or **Terraform**. Using also scaling tools like **EC2 instances**, **Elastic Load Balancer (ELB)** and **RDS**.
- 4. Set up a domain name and secure your app with SSL using AWS Route 53 and AWS Certificate Manager, respectively.
- 5. Using **CloudWatch** monitoring tool.

## Costs
---
How AWS Cloud Pricing works
- See https://www.clickittech.com/aws/aws-pricing/
- See also [./AWS-cost-reduction-checklist.pdf](./AWS-cost-reduction-checklist.pdf)


### How does the AWS Pricing Works?
Amazon services’ prices vary and depend on the service that you are using. These prices can be calculated by different methods, but the main one is for the time of usage, as Amazon (AWS) says so, you pay for what you use. Hours are the main factor on this method, followed by the minutes and seconds of use of certain services.

In this case, I’m going to detail different services which will be located in the region of US East – Virginia. Is important to mention that prices vary depending on the location of our resources.

The services we are always going to see on our AWS bill will be the basic ones:

#### Storage
EBS – $ 0.010 GB per hour
S3 – $ 0.023 GB per month

#### Computing power
In other words, servers and services that we are going to use to process all the information of our application.

EC2 (Price depends of our server type, starting from 5 USD to over 150 USD x month)
Load Balancer (15 USD x 750 hrs)
Elastic IP

#### Networking
Amazon charges all the data that is transferred outside of their own network, that is to say, that everything that we want to move in and out of AWS will cost us. This is one of the main services that shows our bill and there are different costs:

Inbound traffic – It’s free
Outbound traffic – Starting with 0.09 per GB <10 TB
I should mention that the traffic inside of Amazon (between instances, services or regions) is cheaper than the traffic handled outside of Amazon.

### Example Architectures

#### Basic Environment – (Website Hosting) $100 to $200 USD Monthly

![Basic-Environment-01-1-1024x819.jpg](Basic-Environment-01-1-1024x819.jpg)

#### Intermediate Environment – (High Performance Website Hosting) $250 to $600 USD Monthly


![Intermediate-Environment-02-1-1024x819.jpg](Intermediate-Environment-02-1-1024x819.jpg)

#### Advanced Environment – (Highly Scalable and Available) $600 to $2500 USD Monthly

![Advanced-Environment-03-1-1024x819.jpg](Advanced-Environment-03-1-1024x819.jpg)


Of course, all these prices can vary depending on the type of the EC2 instance, the type of RDS (if it is multi-region), the traffic/load, the CloudFront caching and the required storage, etc.

### Extra Tips for AWS Savings
- Use Spot Instances
- Use Reserved Instances
- Use fully managed services
- Use Dedicated instances for the required type of work
- Monitor Amazon CloudWatch cost
- Always consider the bandwidth


### To sum up
As you can see, determining a cost for any application can be a challenge if you ignore the cloud computing services. Every cost varies according to the type of app in the Cloud and the type of work that it manages. Here in **ClickIT**, we can help you optimize the AWS pricing and reduce your AWS billing, to prevent billing surprises.
> "At ClickIT we have built hundreds of websites and applications in the AWS cloud, and these are the classic scenarios of AWS that you will probably fall and their AWS costs involved."
https://www.clickittech.com/


## AWS services used
---

- AWS CLI
- AWS console
- AWS Billing
- **AWS cloud scalable infrastructure**:

  - **IAM** (User authentication and authorisation tool)
    > "Identity and Access Management (IAM) is a web service that helps you securely control access to AWS resources. You use IAM to control who is authenticated (signed in) and authorized (has permissions) to use resources."

  - Elastic Container Registry(**ECR**) docker registry tool like GitLab's built-in Docker registry, Docker Hub.

  - **CodeBuild**(CI tool) like gitlab CI, jenkins.

  - Elastic Container Service (**ECS** / CD tools/ container orchestration service) like Kubernetes.

    - ECS in **Fargate app mode**: Deploy and manage your applications, not infrastructure. Fargate removes the operational overhead of scaling, patching, securing, and managing servers.
        **Terraform**:  Infrastructure as Code (IaC) tool like Ansible.

    - ECS in **EC2/ELB infrastructure mode** :
      **EC2 instances**(Amazon Virtual Servers), Elastic Load Balancer (**ELB**, Scaling tool), Amazon Relational Database tool (**RDS**) for mysql, mariaDB, PostgreSql, etc.
      ECS components infrastructure: ELB app > Target groups > clusters > services > EC2 instances > Tasks > Container

  - **CloudWatch**(monitoring tool) like datadog

  - Amazon **VPC**:
    > "Amazon Virtual Private Cloud (Amazon VPC) enables you to launch AWS resources into a virtual network that you've defined. This virtual network closely resembles a traditional network that you'd operate in your own data center, with the benefits of using the scalable infrastructure of AWS."
    
  - Amazon **Lambda**: Serverless service
    > "Lambda is a compute service that lets you run code without provisioning or managing servers. Lambda runs your code on a high-availability compute infrastructure and performs all of the administration of the compute resources, including server and operating system maintenance, capacity provisioning and automatic scaling, code monitoring and logging. With Lambda, you can run code for virtually any type of application or backend service. All you need to do is supply your code in one of the languages that Lambda supports."

See more keywords in <https://expeditedsecurity.com/aws-in-plain-english/>



## All the routes
---
Demo: <http://easyguide-alb-468591163.eu-west-3.elb.amazonaws.com/about>

| Endpoint       | HTTP Method     | CRUD Method | Result                    |
|-----------------|----------------:|------------:|:--------------------------|
| /auth/register | POST           | CREATE     | register a new user       |
| /auth/login     | POST           | CREATE     | log a user in             |
| /auth/refresh   | POST           | CREATE     | obtain a new access token |
| /auth/status   | GET             | READ       | check user status         |
| /users         | GET             | READ       | get all users             |
| /users/:id     | GET             | READ       | get a single user         |
| /users         | POST           | CREATE     | add a user                |
| /users/:id     | PUT             | UPDATE     | update a user             |
| /users/:id     | DELETE         | DELETE     | delete a user             |

---


---

# 2. IAM service

---

## Create AWS account with IAM service and AWS CLI

---

Review the How do I create and activate a new Amazon Web Services account? guide:
<https://aws.amazon.com/fr/premiumsupport/knowledge-center/create-and-activate-aws-account/>

- **Note:** Billing Alarms
It's a good idea to set up a Billing Alert via CloudWatch to alert you if your AWS usage costs exceed a certain amount. Review Creating a Billing Alarm for more info.
See https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/free-tier-alarms.html

## Install AWS CLI

`$ pip install awscli`
`$ aws --version`

## Create new IAM User

IAM is used to manage access to AWS services:

WHO is trying to access (authentication)
WHICH service are they trying to access (authorization)
WHAT are they trying to do (authorization)

Go on aws webstite account, in /users section
See <https://testdriven.io/courses/aws-flask-react/aws-setup/>
See <https://docs.aws.amazon.com/IAM/latest/UserGuide/intro-structure.html>


## AWS Credential file
---

## AWS account

Next, we need to configure the CLI to use your access credentials. There are several ways to do this, but the easiest is to store your credentials in a file called credentials 
at `~/.aws/` on Linux or Mac 
or `C:\Users\USERNAME.aws\` on Windows. 
Review the Configuration and Credential Files for more information.

You can save your frequently used configuration settings and credentials in files that are maintained by the AWS CLI.

The files are divided into profiles. By default, the AWS CLI uses the settings found in the profile named default. To use alternate settings, you can create and reference additional profiles. For more information on named profiles, see Named profiles for the AWS CLI (https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)


- **Option 1: AUTOMATIC**
  - Install the AWS CLI This will give you access to running AWS commands from the command line. Then since you're using a windows machine, open up CMD and run the following command: `$ aws configure`

  - It will ask you for some details such as Access Key ID and Secret Access Key as well as your preferred region and data type.

  - When you enter these details, it will create the .aws/credentials file for you.

  - Where are configuration settings stored ?
  The AWS CLI stores sensitive credential information that you specify with aws configure in a local file named `credentials`, in a folder named `.aws` in your `home directory`. The less sensitive configuration options that you specify with aws configure are stored in a local file named `config`, also stored in the `.aws` folder in your `home directory`.
  ~/.aws/credentials (Linux & Mac) or %USERPROFILE%\.aws\credentials (Windows)


- **Option 2: MANUAL**
  - Open CMD in the C:\Users\USERNAME directory (should be default)

  - Run the following command in CMD: mkdir .aws

  - Create the credentials file and add your credentials manually


- See more on https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html#config-settings-and-precedence
- See also https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html

### Create AWS credentials file (automatic) 

---

- **aws configure:** 

Type `$ aws configure` then fillup fields

```sh
AWS Access Key ID [None]: 
AWS Secret Access Key [None]: 
Default region name [None]: eu-west-3
Default output format [None]: json
```

- **aws configure set:** You can set any credentials or configuration settings using aws configure set. Specify the profile that you want to view or modify with the --profile setting.
For example, the following command sets the region in the profile named integ.
`$ aws configure set region us-west-2 --profile integ`
- To remove a setting, use an empty string as the value, or manually delete the setting in your config and credentials files in a text editor.
`$ aws configure set cli_pager "" --profile integ`


- **aws configure get:** You can retrieve any credentials or configuration settings you've set using aws configure get. Specify the profile that you want to view or modify with the --profile setting.
For example, the following command retrieves the region setting in the profile named integ.
```sh
$ aws configure get region --profile integ
us-west-2
```

- **aws configure list:**
To list all configuration data, use the aws configure list command. This command displays the AWS CLI name of all settings you've configured, their values, and where the configuration was retrieved from.

```sh
$ aws configure list
      Name                    Value             Type    Location
      ----                    -----             ----    --------
   profile                <not set>             None    None
access_key     ****************ABCD  shared-credentials-file    
secret_key     ****************ABCD  shared-credentials-file    
    region                us-west-2             env    AWS_DEFAULT_REGION
```

- **aws configure list-profiles:**
To list all your profile names, use the aws configure list-profiles command.
```sh
$ aws configure list-profiles
default
test
```

- **Using named profiles --profile:**
  To use a named profile, add the --profile profile-name option to your command. The following example lists all of your Amazon EC2 instances using the credentials and settings defined in the user1 profile from the previous example files.

  `$ aws ec2 describe-instances --profile user1`
  To use a named profile for multiple commands, you can avoid specifying the profile in every command by setting the AWS_PROFILE environment variable at the command line.

  **Linux or macOS**
  `$ export AWS_PROFILE=user1`

  **Windows**
  `C:\> setx AWS_PROFILE user1`

  Using set to set an environment variable changes the value used until the end of the current command prompt session, or until you set the variable to a different value.

  Using setx to set an environment variable changes the value in all command shells that you create after running the command. It does not affect any command shell that is already running at the time you run the command. Close and restart the command shell to see the effects of the change.


- > **Note:** Keep in mind that this approach, of using environment variables, keeps sensitive variables out of the project files, but they are still stored in a secret file like `.env` in plain text. So, be sure to keep this file out of version control. 
Since keeping it out of version control doesn't work if other people on your team need access to it, 
look to either **encrypting the secrets** 
or using a secret store like **Vault** ( see https://www.vaultproject.io/)
or **AWS Secrets Manager**( see https://aws.amazon.com/secrets-manager/)

- **Note:** Example list of secret `.env` file variables that we will need
```
your AWS credentials
AWS Access Key ID: 
AWS Secret Access Key: 
Default region name: eu-west-3

AWS_ACCOUNT_ID: 538751540450 
Vendeur: AWS EMEA SARL 
Nom du compte: cheikhna 
Mot de passe: 

Database credentials(mysql)
identifier: webapp
pwd: 
Address(AWS_RDS_URI): mysql://webapp:<PASSWORD>@easyguide-db.ckzgl9dgncl5.eu-west-3.rds.amazonaws.com:3306/api_prod
```


# 3. ECR repository (Elastic Container Registry): Docker registry tool

---

## Create online the repositories with immutable tags configured

`$ aws ecr create-repository --repository-name easyguide-api --image-tag-mutability IMMUTABLE --region eu-west-3`
`$ aws ecr create-repository --repository-name easyguide-client --image-tag-mutability IMMUTABLE --region eu-west-3`

- **Note:** For being able to replace docker images with codebuild
navigate to Amazon ECS, click "Repositories", and then add two new repositories:
Keep the tags mutable.

## Build and push images to ECR
---

### Build the images

Note: Docker desktop is required on your computer to run docker commands

AWS_ACCOUNT_ID: 538751540450

DEV:

`$ docker build -f services/users/Dockerfile -t 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api:dev services/users`

`$ docker build -f services/client/Dockerfile -t 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-client:dev services/client`

PROD:

`$ docker build -f services/users/Dockerfile.prod -t 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api:prod ./services/users`

`$ docker build -f services/client/Dockerfile.prod -t 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-client:prod --build-arg NODE_ENV=production --build-arg REACT_APP_API_SERVICE_URL=$REACT_APP_API_SERVICE_URL} ./services/client`

### Authenticate the Docker CLI to use the ECR registry

`$ aws ecr get-login-password --region eu-west-3 | docker login --username AWS --password-stdin 538751540450.dkr.ecr.eu-west-3.amazonaws.com`

You should see: Login Succeeded

### Push the images to ECR

Note: Docker desktop is required on your computer to run docker commands

AWS_ACCOUNT_ID: 538751540450

DEV:

`$ docker push 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api:dev`

`$ docker push 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-client:dev`

PROD:
`$ docker push 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api:prod`

`$ docker push 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-client:prod`

# 4. CodeBuild: CI tool for building and testing docker images

- **Note:** Keep the tags mutable for being able to override docker images by codebuild services:
  - Navigate to Amazon ECS.
  - click "Repositories".
  - then add two new repositories and activate the checkbox "Keep the tags mutable".

## With the AWS console, navigate to the CodeBuild dashboard and click "Create project"

**Note:** How to add connection to github for codebuild or vscode editor, create a personal access token in your github account dashboard >
then past this token in codebuild or vs code git config file : see tutorial  <https://www.youtube.com/watch?v=IJf_Tryhzic&t=56s>.

```
url = https://<YOUR_PERSONAL_ACCESS_TOKEN>@github.com/<YOUR_USERNAME>/test-flask-react-aws.git
```

Link: <https://console.aws.amazon.com/>

**Project configuration**
"Project name" - easyguide-build
"Description" - build and test docker images
"Build badge" - check the flag to "Enable build badge"
![codebuild-source.png)](codebuild-source.png))

**Environment**
"Environment image" - use the "Managed image"
"Operating system" - "Ubuntu"
"Runtime" - "Standard"
"Image" - "aws/codebuild/standard:5.0"
"Image version" - "Always use the latest image for this runtime version"
"Privileged" - check the flag
"Service role" - "New service role"
"Role name" - easyguide-build-role

![codebuild-environment.png](codebuild-environment.png)

Under "Additional configuration":
set the "Timeout" to 10 minutes
add your AWS account ID as an environment variable called AWS_ACCOUNT_ID in plaintext

![codebuild-environment-additional.png](codebuild-environment-additional.png)

**Buildspec, Artifacts, and Logs**
Under "Build specifications", select "Use a buildspec file"
Skip the "Artifacts" section
Dump the logs to "CloudWatch"

![codebuild-spec-and-logs.png](codebuild-spec-and-logs.png)

## Create the buildspec.yml

- Create the buildspec.yml file in the project root:

Now, before building the production images, we:

- Build the development images
- Spin up the containers
- Run pytest, Flake8, Black, and isort against the Python/Flask code
- Test, lint, and run prettier against the JavaScript/React code
- building the production image

STRUCTURE

```yml
version: 0.2

env:
  variables:
    AWS_REGION: "eu-west-3"
    REACT_APP_API_SERVICE_URL: "http://localhost:5004"
    
phases:
  pre_build:
    commands:
      - echo login in to ECR...
  build:
    commands:
      - echo testing images...
      - echo building images...

  post_build:
    commands:
    - echo pushing images to ECR...
```

FULL VERSION

```yml
version: 0.2

env:
  variables:
    AWS_REGION: "eu-west-3"
    REACT_APP_API_SERVICE_URL: "http://localhost:5004"

phases:
  pre_build:
    commands:
      - echo logging in to ecr...
      - >
        aws ecr get-login-password --region $AWS_REGION \
          | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com
  build:
    commands:
      - echo testing dev images...
      - docker-compose up -d --build
      - docker-compose exec -T api python -m pytest "src/tests" -p no:warnings --cov="src"
      - docker-compose exec -T api flake8 src
      - docker-compose exec -T api black src --check
      - docker-compose exec -T api isort src --check-only
      - docker-compose exec -T client npm run lint
      - docker-compose exec -T client npm run prettier:check
      - docker-compose exec -T client npm run prettier:write
      - echo building prod images...
      - >
        docker build \
          -f services/users/Dockerfile.prod \
          -t $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-api:prod \
          ./services/users
      - >
        docker build \
          -f services/client/Dockerfile.prod \
          -t $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:prod \
          --build-arg NODE_ENV=production \
          --build-arg REACT_APP_API_SERVICE_URL=$REACT_APP_API_SERVICE_URL \
          ./services/client
  post_build:
    commands:
    - echo pushing prod images to ECR...
    - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-api:prod
    - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:prod

```

VERSION WITH CASHING
**Note:** **Docker Layer Caching**
Docker caches each layer as an image is built, and each layer will only be re-built if it or the layer above it has changed since the last build. So, you can significantly speed up builds with Docker cache. Let's take a look at a quick example.

The first Docker build can take several minutes to complete, depending on your connection speed. Subsequent builds should only take a few seconds since the layers get cached after that first build;
Test it out one last time. This build should take longer (~8 minutes vs ~ 6 minutes) since we're building more images; however, subsequent builds should be slightly faster (~5 minutes).

```yml
version: 0.2

env:
  variables:
    AWS_REGION: "eu-west-3"
    REACT_APP_API_SERVICE_URL: "http://localhost:5004"

phases:
  pre_build:
    commands:
      - echo login in to ECR...
      - >
        aws ecr get-login-password --region $AWS_REGION \
          | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com
      - docker pull $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-api:prod || true
      - docker pull $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:builder || true
      - docker pull $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:prod || true
  build:
    commands:
      - echo testing dev images...
      - docker-compose up -d --build
      - docker-compose exec -T api python -m pytest "src/tests" -p no:warnings --cov="src"
      - docker-compose exec -T api flake8 src
      - docker-compose exec -T api black src --check
      - docker-compose exec -T api isort src --check-only
      - docker-compose exec -T client npm run lint
      - docker-compose exec -T client npm run prettier:check
      - docker-compose exec -T client npm run prettier:write
      - echo building prod images...
      - >
        docker build \
          -f services/users/Dockerfile.prod \
          -t $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-api:prod \
          ./services/users
      - >
        docker build \
          --target builder \
          --cache-from $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:builder \
          -f services/client/Dockerfile.prod \
          -t $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:builder \
          --build-arg NODE_ENV=production \
          --build-arg REACT_APP_API_SERVICE_URL=$REACT_APP_API_SERVICE_URL \
          ./services/client
      - >
        docker build \
          -f services/client/Dockerfile.prod \
          -t $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:prod \
          --build-arg NODE_ENV=production \
          --build-arg REACT_APP_API_SERVICE_URL=$REACT_APP_API_SERVICE_URL \
          ./services/client
  post_build:
    commands:
    - echo pushing prod images to ECR...
    - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-api:prod
    - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:builder
    - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:prod

```

- An error will occure if you click on the build button of codebuild

![codebuild-new-build.png](codebuild-new-build.png)

```
An error occurred (AccessDeniedException) when calling the GetAuthorizationToken operation:
User: <omitted> is not authorized to perform: ecr:GetAuthorizationToken on resource: *
```

To fix, add the **AmazonEC2ContainerRegistryPowerUser** policy to the service role in the IAM dashboard.

![codebuild-iam-role.png](codebuild-iam-role.png)

**Note:** If you have a new error like you cannot override an existing image (due to your command `--image-tag-mutability IMMUTABLE`)
you can still remove manualy on the ECR dashboard this docker image file.

- Once done, kick off a new build from the console. It should pass.

## ECS codebuild new revision to the task definition

In the chapter, we'll update the CodeBuild CI/CD workflow to add a new revision to the Task Definition and update the Service.

- **Steps:**

1. Create local Task Definition JSON files
2. Update creation of Task Definitions on AWS via CodeBuild
3. Update Service via CodeBuild

### Task Definitions

- Let's create JSON files for the Task Definitions in a new folder at the project root called "ecs".

- `ecs_client_taskdefinition.json`
- `ecs_api_taskdefinition.json`

- **Client:**

```json
{
  "containerDefinitions": [
    {
      "name": "client",
      "image": "%s.dkr.ecr.eu-west-3.amazonaws.com/easyguide-client:prod",
      "essential": true,
      "memoryReservation": 300,
      "portMappings": [
        {
          "hostPort": 0,
          "protocol": "tcp",
          "containerPort": 80
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "easyguide-client-log",
          "awslogs-region": "eu-west-3"
        }
      }
    }
  ],
  "family": "easyguide-client-td"
}
```

- **API:**

```json
{
  "containerDefinitions": [
    {
      "name": "users",
      "image": "%s.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api:prod",
      "essential": true,
      "memoryReservation": 300,
      "portMappings": [
        {
          "hostPort": 0,
          "protocol": "tcp",
          "containerPort": 5000
        }
      ],
      "environment": [
        {
          "name": "APP_SETTINGS",
          "value": "src.config.ProductionConfig"
        },
        {
          "name": "DATABASE_TEST_URL",
          "value": "mysql://mysql:mysql@api-db:3306/api_test"
        },
        {
          "name": "DATABASE_URL",
          "value": "%s"
        },
        {
          "name": "SECRET_KEY",
          "value": "%s"
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "easyguide-api-log",
          "awslogs-region": "eu-west-3"
        }
      }
    }
  ],
  "family": "easyguide-api-td"
}
```

### CodeBuild Update task definition

Add a new file to the root called `deploy.sh`:

```sh
#!/bin/sh

JQ="jq --raw-output --exit-status"

configure_aws_cli() {
  aws --version
  aws configure set default.region eu-west-3
  aws configure set default.output json
  echo "AWS Configured!"
}

register_definition() {
  if revision=$(aws ecs register-task-definition --cli-input-json "$task_def" | $JQ '.taskDefinition.taskDefinitionArn'); then
    echo "Revision: $revision"
  else
    echo "Failed to register task definition"
    return 1
  fi
}

deploy_cluster() {

  # users
  template="ecs_api_taskdefinition.json"
  task_template=$(cat "ecs/$template")
  task_def=$(printf "$task_template" $AWS_ACCOUNT_ID $AWS_RDS_URI $PRODUCTION_SECRET_KEY)
  echo "$task_def"
  register_definition

  # client
  template="ecs_client_taskdefinition.json"
  task_template=$(cat "ecs/$template")
  task_def=$(printf "$task_template" $AWS_ACCOUNT_ID)
  echo "$task_def"
  register_definition

}

configure_aws_cli
deploy_cluster
```

- Here, the AWS CLI is configured and then the `deploy_cluster` function is fired, which updates the existing Task Definitions with the definitions found in the JSON files we just created.

- Add the `AWS_RDS_URI` and `PRODUCTION_SECRET_KEY` environment variables to the CodeBuild project.

- Open the the project, click the "Edit" dropdown, and then select "Environment".

![codebuild-env-vars-1.png](codebuild-env-vars-1.png)

- Then, add the two environment variables under the "Additional configuration" section.

![codebuild-env-vars-2.png](codebuild-env-vars-2.png)

- To create a key, open the Python shell and run:

```sh
>>> import binascii
>>> import os
>>> binascii.hexlify(os.urandom(24))
b'958185f1b6ec1290d5aec4eb4dc77e67846ce85cdb7a212a'
```

- Update post_build in buildspec.yml:

```yml
post_build:
    commands:
    - echo pushing prod images to ECR...
    - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-api:prod
    - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:builder
    - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:prod
    - chmod +x ./deploy.sh
    - bash deploy.sh
```

- Attach the `AmazonECS_FullAccess` policy to the `flask-react-build-role` service role in the IAM dashboard.

![codebuild-iam-role-2.png](codebuild-iam-role-2.png)

- Commit and push your code to GitHub. After the CodeBuild run passes, make sure new images were created and revisions to the Task Definitions were added.

![updated_task_def_1.png](updated_task_def_1.png)

![updated_task_def_2.png](updated_task_def_2.png)

- You can ensure that the correct Task Definition JSON files were used to create the revisions by reviewing the latest revisions added to each of the Task Definitions. For example, open the latest revision for flask-react-users-td. Under the "Container Definitions", click the drop-down next to the users container. Make sure the DATABASE_URL and SECRET_KEY environment variables are correct:

![updated_task_def_3.png](updated_task_def_3.png)

- Then, navigate to the Cluster. Update each of the Services so that they use the new Task Definitions.

![updated_service.png](updated_service.png)

- Again, ECS will instantiate the Task Definitions, creating new Tasks that will spin up on the Cluster instances. Then, as long as the health checks pass, the load balancer will start sending traffic to them.

- Make sure the instances associated with the Target Groups -- flask-react-client-tg and flask-react-users-tg -- are healthy.

### Update service

Now, update deploy.sh, like so, to automatically update the Services after new revisions are added to the Task Definitions:

```sh
#!/bin/sh

JQ="jq --raw-output --exit-status"

configure_aws_cli() {
  aws --version
  aws configure set default.region eu-west-3
  aws configure set default.output json
  echo "AWS Configured!"
}

register_definition() {
  if revision=$(aws ecs register-task-definition --cli-input-json "$task_def" | $JQ '.taskDefinition.taskDefinitionArn'); then
    echo "Revision: $revision"
  else
    echo "Failed to register task definition"
    return 1
  fi
}

# new
update_service() {
  if [[ $(aws ecs update-service --cluster $cluster --service $service --task-definition $revision | $JQ '.service.taskDefinition') != $revision ]]; then
    echo "Error updating service."
    return 1
  fi
}

deploy_cluster() {

  cluster="flask-react-cluster" # new

  # users
  service="flask-react-users-service"  # new
  template="ecs_users_taskdefinition.json"
  task_template=$(cat "ecs/$template")
  task_def=$(printf "$task_template" $AWS_ACCOUNT_ID $AWS_RDS_URI $PRODUCTION_SECRET_KEY)
  echo "$task_def"
  register_definition
  update_service  # new

  # client
  service="flask-react-client-service"  # new
  template="ecs_client_taskdefinition.json"
  task_template=$(cat "ecs/$template")
  task_def=$(printf "$task_template" $AWS_ACCOUNT_ID)
  echo "$task_def"
  register_definition
  update_service  # new

}

configure_aws_cli
deploy_cluster
```

- Commit and push your code to GitHub to trigger a new run on CodeBuild. Once done, you should see a new revision associated with each Task Definition and the Services should now be running a new Task based on that revision.

- Test everything out again.

### Domain Name and HTTPS #

Route 53 can be used to link a domain name to the instances running on the Cluster. The setup is fairly simple. Review Routing Traffic to an ELB Load Balancer for more details.
See <https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/routing-to-elb-load-balancer.html>

You should configure HTTPS as well. You'll need to set up an SSL certificate with Certificate Manager, add an HTTPS listener to the Application Load Balancer, and then proxy 443 traffic to HTTP ports 80 and 5000 on the instances via Target Groups.
See <https://aws.amazon.com/certificate-manager/>

## Feature branch CodeBUild workflow

---
In the chapter, we'll configure CodeBuild to handle the feature branch workflow.

**Feature Branch Workflow**
By the end of this chapter, we'll be able to use the following feature branch workflow:

- **Development**
  - Create a new feature branch from the master branch
  - Make a change to the code
  - Commit and push the code up to GitHub to trigger CodeBuild
  - After the build passes, open a PR against the dev branch to trigger CodeBuild
  - Merge the PR after the build passes
  - After the build passes images are created, tagged with the commit hash, and - pushed to ECR

- **Production**
  - Open PR from the dev branch against the master branch to trigger CodeBuild
  - Merge the PR after the build passes to trigger a new build
  - After the build passes:
  - images are created, tagged prod, and pushed to ECR
  - revisions are added to the Task Definitions, and the Service is updated

### Create new Git branch

Start by creating a new branch called dev and pushing it up to GitHub:

```sh
git checkout -b dev
git push origin dev
```

### CodeBuild prod

First, let's update the existing CodeBuild configuration so it will only update ECS when the branch is `master`. To do this, we can leverage the following CodeBuild environment variables:

- CODEBUILD_WEBHOOK_HEAD_REF
- CODEBUILD_WEBHOOK_TRIGGER

Update deploy.sh like so
Assuming you're on the dev branch, commit and push your code up to GitHub to trigger a new CodeBuild. It should not update ECS.

```sh
#!/bin/sh

JQ="jq --raw-output --exit-status"

configure_aws_cli() {
  aws --version
  aws configure set default.region eu-west-3
  aws configure set default.output json
  echo "AWS Configured!"
}

register_definition() {
  if revision=$(aws ecs register-task-definition --cli-input-json "$task_def" | $JQ '.taskDefinition.taskDefinitionArn'); then
    echo "Revision: $revision"
  else
    echo "Failed to register task definition"
    return 1
  fi
}

update_service() {
  if [[ $(aws ecs update-service --cluster $cluster --service $service --task-definition $revision | $JQ '.service.taskDefinition') != $revision ]]; then
    echo "Error updating service."
    return 1
  fi
}

deploy_cluster() {

  cluster="easyguide-cluster"

  # users
  service="easyguide-api-service"
  template="ecs_api_taskdefinition.json"
  task_template=$(cat "ecs/$template")
  task_def=$(printf "$task_template" $AWS_ACCOUNT_ID $AWS_RDS_URI $PRODUCTION_SECRET_KEY)
  echo "$task_def"
  register_definition
  update_service

  # client
  service="easyguide-client-service"
  template="ecs_client_taskdefinition.json"
  task_template=$(cat "ecs/$template")
  task_def=$(printf "$task_template" $AWS_ACCOUNT_ID)
  echo "$task_def"
  register_definition
  update_service

}

# new
echo $CODEBUILD_WEBHOOK_BASE_REF
echo $CODEBUILD_WEBHOOK_HEAD_REF
echo $CODEBUILD_WEBHOOK_TRIGGER
echo $CODEBUILD_WEBHOOK_EVENT

# new
if  [ "$CODEBUILD_WEBHOOK_TRIGGER" == "branch/master" ] && \
    [ "$CODEBUILD_WEBHOOK_HEAD_REF" == "refs/heads/master" ]
then
  echo "Updating ECS."
  configure_aws_cli
  deploy_cluster
fi
```

### CodeBuild dev

Next, let's update buildspec.yml so that the commit hash is used for the Docker tag when the branch is not master:

```yml
version: 0.2

env:
  variables:
    AWS_REGION: "eu-west-3"
    REACT_APP_API_SERVICE_URL: "http://easyguide-alb-468591163.eu-west-3.elb.amazonaws.com"

phases:
  pre_build:
    commands:
      - echo logging in to ecr...
      - >
        aws ecr get-login-password --region $AWS_REGION \
          | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com
      # new
      - |
        if expr "$CODEBUILD_WEBHOOK_TRIGGER" == "branch/master" >/dev/null  && expr "$CODEBUILD_WEBHOOK_HEAD_REF" == "refs/heads/master" >/dev/null; then
          DOCKER_TAG=prod
        else
          DOCKER_TAG=${CODEBUILD_RESOLVED_SOURCE_VERSION}
        fi
      # new
      - echo "Docker tag:" $DOCKER_TAG
      - docker pull $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-api:$DOCKER_TAG || true  # updated
      - docker pull $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:builder || true  # updated
      - docker pull $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:$DOCKER_TAG || true  # updated
  build:
    commands:
      - echo building and testing dev images...
      - docker-compose up -d --build
      - docker-compose exec -T api python -m pytest "src/tests" -p no:warnings --cov="src"
      - docker-compose exec -T api flake8 src
      - docker-compose exec -T api black src --check
      - docker-compose exec -T api isort src --check-only
      - docker-compose exec -T client npm run lint
      - docker-compose exec -T client npm run prettier:check
      - docker-compose exec -T client npm run prettier:write
      - echo building prod images...
      # updated
      - >
        docker build \
          --cache-from $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-api:$DOCKER_TAG \
          -f services/users/Dockerfile.prod \
          -t $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-api:$DOCKER_TAG \
          ./services/users
      - >
        docker build \
          --target builder \
          --cache-from $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:builder \
          -f services/client/Dockerfile.prod \
          -t $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:builder \
          --build-arg NODE_ENV=production \
          --build-arg REACT_APP_API_SERVICE_URL=$REACT_APP_API_SERVICE_URL \
          ./services/client
      # updated
      - >
        docker build \
          --cache-from $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:$DOCKER_TAG \
          -f services/client/Dockerfile.prod \
          -t $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:$DOCKER_TAG \
          ./services/client
  post_build:
    commands:
    - echo pushing prod images to ecr...
    - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-api:$DOCKER_TAG  # updated
    - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:builder
    - docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:$DOCKER_TAG  # updated
    - chmod +x ./deploy.sh
    - bash deploy.sh

```

- Take note of the changes. Commit and push your code up to GitHub to trigger a new CodeBuild.

![ecr_push_dev.png](ecr_push_dev.png)

### Git workflow

Before testing this workflow, within the GitHub repository, open the "Settings" page and under "Webhooks", make sure that both pull requests and pushes are selected.

```
- https://eu-west-3.webhooks.aws/trigger

- https://codebuild.eu-west-3.amazonaws.com/webhooks
```

![webhooks-git.png](webhooks-git.png)

![codebuild-github-webhook.png](codebuild-github-webhook.png)

Let's test out the feature branch workflow:

#### Development

- Create a new feature branch from the `dev` branch:

```sh
git checkout -b feature/testing-workflow
```

- Make a change to the code, commit, and push it up to GitHub:

```sh
git add -A
git commit -m "testing codebuild"
git push origin feature/testing-workflow
```

- This should trigger a `flask-react-build-dev` build. Wait until the build passes and then ensure new images are added to ECR. Then, on GitHub, open a PR against the dev branch to trigger another build. Again, wait for it to pass before merging. Another build should start. Once complete, new images should be added to ECR.

#### Production

- Open PR from the dev branch against the master branch to trigger a new build
- Merge the PR after the build passes to trigger yet another build
- After the build passes, images are created, tagged prod, and pushed to ECR
- Revisions are added to the Task Definitions and the Service is updated


# 5. Elastic Container Service (ECS): CD tool, Container orchestration service

## EC2 instances, Elastic Load Balancer (ELB) and RDS : Scaling tool

---

### Features

The Elastic Load Balancer (ELB) distributes incoming application traffic and scales resources as needed to meet traffic needs.

A load balancer is one of (if not) the most important parts of your application since it needs to always be up, routing traffic to healthy services, and ready to scale at a moment’s notice.

Load balancers:

1. Enable horizontal scaling
2. Improve throughput, which can help decrease latency
3. Prevent the overloading of a single service
4. Provide a framework for updating service on the fly
5. Improve tolerance for back-end failures

**There are currently four types of Elastic Load Balancers to choose from.**
See <https://aws.amazon.com/elasticloadbalancing/features/#details>

1. Application Load Balancer
2. Network Load Balancer
3. Gateway Load Balancer
4. Classic Load Balancer

We’ll be using the **Application Load Balancer**

### About horizontal and vertical scaling

**Horizontal scaling** means that you scale by adding more machines into your pool of resources
whereas **Vertical scaling** means that you scale by adding more power (CPU, RAM) to an existing machine.

An easy way to remember this is to think of a machine on a server rack, we add more machines across the horizontal direction and add more resources to a machine in the vertical direction.

![horiz-vert-scaling.png](horiz-vert-scaling.png)

In the database world, horizontal-scaling is often based on the partitioning of the data i.e. each node contains only part of the data, in vertical-scaling the data resides on a single node and scaling is done through multi-core i.e. spreading the load between the CPU and RAM resources of that machine.

With horizontal-scaling it is often easier to scale dynamically by adding more machines into the existing pool - Vertical-scaling is often limited to the capacity of a single machine, scaling beyond that capacity often involves downtime and comes with an upper limit.

Good examples of horizontal scaling are Cassandra, MongoDB, Google Cloud Spanner .. and a good example of vertical scaling is MySQL - Amazon RDS (The cloud version of MySQL). It provides an easy way to scale vertically by switching from small to bigger machines. This process often involves downtime.

Read more:

- <http://ht.ly/cAhPe>
- <http://ht.ly/cAhY6>

### Steps

Navigate to Amazon EC2, click "Load Balancers" on the sidebar, and then click the "Create Load Balancer" button. Select the "Create" button under "Application Load Balancer".

#### 1. Configure Load Balancer

"Name": easyguide-alb
"Scheme": internet-facing
"IP address type": ipv4
"Listeners": HTTP / Port 80
"VPC": Select the default VPC to keep things simple
"Availability Zones": Select at least two available subnets

![configure-load-balancer-1.png](configure-load-balancer-1.png)

#### 2. Configure Security Settings

Skip this for now.

#### 3. Configure Security Groups #

Select an existing Security Group or create a new Security Group (akin to a firewall) called easyguide-security-group, making sure at least HTTP 80 and SSH 22 are open.

![configure-load-balancer-2.png](configure-load-balancer-2.png)

#### 4. Configure Routing

1. "Name": easyguide-client-tg
2. "Target type": Instance
3. "Port": 80
4. "Path": /

![configure-load-balancer-3.png](configure-load-balancer-3.png)

#### 5. Register Targets

Do not assign any instances manually since this will be managed by ECS. Review and then create the new load balancer.

Once created, take note of the new Security Group:

![configure-load-balancer-4.png](configure-load-balancer-4.png)

With that, we also need to set up **Target Groups** and **Listeners**:

![alb-ecs.png](alb-ecs.png)

#### 6. Target Groups

Target Groups are attached to the Application Load Balancer and are used to route traffic to the containers found in the ECS Service.

You may not have noticed, but a Target Group called easyguide-tg was already created (which we'll use for the client app) when we set up the Application Load Balancer, so we just need to set up one more for the users service.

Within the EC2 Dashboard, click "Target Groups", and then create the following Target Group:

1. "Target type": Instances
2. "Target group name": easyguide-api-tg
3. "Port": 5000
4. Then, under "Health check settings" set the "Path" to /ping.

![configure-target-group-users.png](configure-target-group-users.png)

You should now have the following Target Groups:

![target-groups.png](target-groups.png)

#### 7. Listeners

Back on the "Load Balancers" page within the EC2 Dashboard, select the easyguide-alb Load Balancer, and then click the "Listeners" tab. Here, we can add Listeners to the load balancer, which are then forwarded to a specific Target Group.

There should already be a listener for "HTTP : 80". Click the "View/edit rules" link, and then insert a new rule that forwards to easyguide-api-tg with the following conditions: IF Path is /users*OR /ping OR /auth* OR /doc OR /swagger*.

![load-balancer-listeners.png](load-balancer-listeners.png)

#### 8. Update CodeBuild

- Finally, navigate back to the Load Balancer and grab the "DNS name" from the "Description" tab:

![load-balancer.png](load-balancer.png)

- We need to set this as the value of REACT_APP_API_SERVICE_URL in `buildspec.yml`:

```yml
env:
  variables:
    AWS_REGION: "eu-west-3"
    REACT_APP_API_SERVICE_URL: "http://<LOAD_BALANCER_DNS_NAME>"
```

- Replace <LOAD_BALANCER_DNS_NAME> with the actual DNS name. For example:

```yml
env:
  variables:
    AWS_REGION: "eu-west-3"
    REACT_APP_API_SERVICE_URL: "http://easyguide-alb-828458753.eu-west-3.elb.amazonaws.com"

```

- Commit and push your code to trigger a new build. Make sure new images are added to ECR once the build is done.

### Setting up RDS (Amazon SQL): Amazon Relational Database

---

Before configuring ECS, let's set up Amazon Relational Database Service (RDS).

#### Why RDS (Amazon SQL)?

First off, why should we use RDS rather than managing Postgres on our own within a Docker container?

1. **Set up and management:** With RDS, you are not responsible for configuring, securing, or maintaining the database -- AWS handles all this for you. Is this something that you need or want control over? Think about the costs associated with setting up and maintaining your own database. Do you have the time and knowledge to deal with backing up and restoring your database as well as failover and replication management? Will you need to hire a database administrator to do this for you?

2. **Data integrity:** How do you plan to manage data volumes and persist data across containers? What happens if the container crashes?

#### RDS Setup

Navigate to Amazon RDS, click "Databases" on the sidebar, and then click the "Create database" button.

- **Engine options and Templates:**

For the settings, select the latest version of the "PostgreSQL" engine.

Use the "Free Tier" template (choose MySql if no freetier for postgresql).

For more on the free tier, review the AWS Free Tier guide.

![rds-config-1.png](rds-config-1.png)

- **Settings**

"DB instance identifier": easyguide-db
"Master username": webapp
"Master password": Check "Auto generate a password"

![rds-config-2.png](rds-config-2.png)

- **Connectivity**

Select the "VPC" and "Security group" associated with ALB. Turn off "Public accessibility". Select one of the available "Subnets" as well -- either eu-west-3b or eu-west-3c.

![rds-config-3.png](rds-config-3.png)

- **Additional configuration:**

Change the "Initial database name" to api_prod and then create the new database.

Click the "View credential details" button to view the generated password. Take note of it.

You can quickly check the status using the AWS CLI.

Unix users:

```
aws --region eu-west-3 rds describe-db-instances --db-instance-identifier easyguide-db --query 'DBInstances[].{DBInstanceStatus:DBInstanceStatus}'
```

Windows users:

```
aws --region eu-west-3 rds describe-db-instances --db-instance-identifier easyguide-db --query 'DBInstances[].{DBInstanceStatus:DBInstanceStatus}'
```

You should see:

```
[
    {
        "DBInstanceStatus": "creating"
    }
]
```

Then, once the status is "available", you can grab the address.

Unix users:

```
aws --region eu-west-3 rds describe-db-instances --db-instance-identifier easyguide-db --query 'DBInstances[].{Address:Endpoint.Address}'
```

Windows users:

```
aws --region eu-west-3 rds describe-db-instances --db-instance-identifier easyguide-db --query 'DBInstances[].{Address:Endpoint.Address}'
```

Take note of the production URI:

`postgres://webapp:<YOUR_RDS_PASSWORD>@<YOUR_RDS_ADDRESS>:5432/api_prod`

For example:

`postgres://webapp:rnPf1FLqNYv3RZMkvD8E@easyguide-db.c7kxiqfnzo9e.eu-west-3.rds.amazonaws.com:5432/api_prod`

Keep in mind that you cannot access the DB outside the VPC. So, if you want to connect to the instance, you will need to use SSH tunneling via SSHing into an EC2 instance on the same VPC and, from there, connecting to the database. We'll go through this process in a future chapter.

## ECS Components infrastructure

---
ECS is a container orchestration system used for managing and deploying Docker-based containers.

ECS components infrastructure: ELB app > Target groups > clusters > services > EC2 instances > Tasks > Container

It has four main components:

1. Task Definitions
2. Tasks
3. Services
4. Clusters

In short, Task Definitions are used to spin up Tasks that get assigned to a Service, which is then assigned to a Cluster.

![05_ecs.png](05_ecs.png)

Let's configure a Task Definition along with a Cluster and a Service within Elastic Container Service (ECS).

### 1. Task definitions

Task Definitions define which containers make up the overall app and how much resources are allocated to each container. You can think of them as blueprints, similar to a Docker Compose file.

- Navigate to Amazon ECS, click "Task Definitions", and then click the button "Create new Task Definition". Then select "EC2" in the "Select launch type compatibility" screen.

#### Client

First, update the "Task Definition Name" to `easyguide-client-td`and then add a new container:

1. "Container name": client
2. "Image": YOUR_AWS_ACCOUNT_ID.dkr.ecr.eu-west-3.amazonaws.com/easyguide-client:prod
3. "Memory Limits (MB)": 300 soft limit
4. "Port mappings": 0 host, 80 container

We set the host port for the service to 0 so that a port is dynamically assigned when the Task is spun up.

![task_defs_client_1.png](task_defs_client_1.png)

It's important to note that you will not need to add the REACT_APP_API_SERVICE_URL environment variable to the container definition. This variable is required at the build-time, not the run-time, and is added during the building of the image on CodeBuild (within buildspec.yml):

```
docker build --target builder --cache-from $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:builder -f services/client/Dockerfile.prod -t $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/easyguide-client:builder --build-arg NODE_ENV=production --build-arg REACT_APP_API_SERVICE_URL=$REACT_APP_API_SERVICE_URL ./services/client
```

- It’s a good idea to configure logs, via LogConfiguration, to pipe logs to CloudWatch.

- To set up, we need first to create a new Log Group. Navigate to CloudWatch, in a new window, and click "Logs" on the navigation pane. If this is your first time setting up a group, click the "Let's get started" button and then the "Create log group" button. Otherwise, click the "Actions" drop-down button, and then select "Create log group". Name the group `easyguide-client-log`.

- Back in ECS, add the Log Configuration into the section `STORAGE AND LOGGING`:

![task_defs_client_2.png](task_defs_client_2.png)

- then add this client task definition before creating the API task definition

#### API

For the API service, use the name `easyguide-api-td`, and then add a single container:

1. "Container name": api
2. "Image": YOUR_AWS_ACCOUNT_ID.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api:prod
3. "Memory Limits (MB)": 300 soft limit
4. "Port mappings": 0 host, 5000 container
5. "Log configuration": easyguide-api-log

Then, add the following environment variables:

1. FLASK_ENV - production
2. APP_SETTINGS - src.config.ProductionConfig
3. DATABASE_URL - YOUR_RDS_URI
  Ex: mysql://webapp:<TOKEN_PWD>@easyguide-db.ckzgl9dgncl5.eu-west-3.rds.amazonaws.com:3306/api_prod
4. DATABASE_TEST_URL - mysql://mysql:mysql@api-db:5432/api_test
5. SECRET_KEY - my_precious
Change `my_precious` to a randomly generated string that's at least 50 characters. Use the actual RDS URI rather than `YOUR_RDS_URI`.

![task_defs_client_3.png](task_defs_client_3.png)

- Add the container and create the new Task Definition.

### 2. Clusters

---
Clusters are where the actual containers run. They are just groups of EC2 instances that run Docker containers managed by ECS. To create a Cluster, click "Clusters" on the ECS Console sidebar, and then click the "Create Cluster" button. Select "EC2 Linux + Networking".

Add:

1. "Cluster name": easyguide-cluster
2. "Provisioning Model": It's recommended to stick with On-Demand Instance instances, but feel free to use Spot if that's what you prefer.
3. "EC2 instance type": t2.micro
4. "Number of instances": 4
5. "Key pair": Select an existing Key Pair or create a new one (see below for details on how to create a new Key Pair)

- **Key Pair for SSH Security access**
To create a new EC2 Key Pair, so we can SSH into the EC2 instances managed by ECS, navigate to Amazon EC2, click "Key Pairs" on the sidebar, and then click the "Create Key Pair" button.
Name the new key pair `ecs` and add the automatically donloaded file `ecs.ppk` (by your browser) to "~/.ssh".

  - For Name, enter a descriptive name for the key pair. Amazon EC2 associates the public key with the name that you specify as the key name. A key name can include up to 255 ASCII characters. It can’t include leading or trailing spaces.

  - For Key pair type, choose either `RSA` or `ED25519`. Note that `ED25519` keys are not supported for Windows instances, EC2 Instance Connect, or EC2 Serial Console.

  - For Private key file format, choose the format in which to save the private key. To save the private key in a format that can be used **with OpenSSH**, choose `pem`. To save the private key in a format that can be used with **PuTTY**, choose `ppk`.
    - If you chose `ED25519` in the previous step, the Private key file format options do not appear, and the private key format defaults to pem.
    - Select the file format of the private key : .pem to use with OpenSSH
  
  - Choose Create key pair.

  - The private key file is automatically downloaded by your browser. The base file name is the name that you specified as the name of your key pair, and the file name extension is determined by the file format that you chose. Save the private key file in a safe place.
  **Important: This is the only chance for you to save the private key file.**
    - If you will use an SSH client on a macOS or Linux computer to connect to your Linux instance, use the following command to set the permissions of your private key file so that only you can read it.
    `$ chmod 400 my-key-pair.pem`
    - If you do not set these permissions, then you cannot connect to your instance using this key pair. For more information, see Error: Unprotected private key file.

  - See more options on : <https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html>

![configure-cluster-1.png](configure-cluster-1.png)

- Select the default VPC and the previously created Security Group along with the appropriate subnets.

![configure-cluster-2.png](configure-cluster-2.png)

### 3. Services

---
Services instantiate the containers from the Task Definitions and run them on EC2 boxes within the ECS Cluster. Such instances are called Tasks. To define a Service, on the "Services" tab within the newly created Cluster, click "Create".

Create the following Services...

#### CLient service

- **Configure service:**
- "Launch type": EC2
- "Task Definition":
  - "Family": easyguide-client-td
  - "Revision: LATEST_REVISION_NUMBER
- "Service name": easyguide-client-service
- "Number of tasks": 1

![configure-service-1.png](configure-service-1.png)

- Leave the "Deployment type" setting at Rolling update.
Review the Updating a Service guide to learn more about rolling vs blue/green deployments.

- You can configure how and where new Tasks are placed in a Cluster via "Task Placement" Strategies. We will use the basic "AZ Balanced Spread" in this course, which spreads Tasks evenly across Availability Zones (AZ), and then within each AZ, Tasks are spread evenly among Instances. For more, review Amazon ECS Task Placement Strategies

- Click "Next step".

- **Configure network:**
- Select the "Application Load Balancer" under "Load balancer type".
  - "Load balancer name": easyguide-alb
  - "Container name : port": client:0:80

![configure-service-2.png](configure-service-2.png)

- Click "Add to load balancer".
  - "Production listener port": 80:HTTP
  - "Target group name": easyguide-client-tg

![configure-service-3.png](configure-service-3.png)

- Do not enable "Service discovery". Click the next button a few times, and then "Create Service".

#### API service

- **Configure service:**
- "Launch type": EC2
- "Task Definition":
  - "Family": easyguide-api-td
  - "Revision: LATEST_REVISION_NUMBER
- "Service name": easyguide-api-service
- "Number of tasks": 1
- Leave the "Deployment type" setting at Rolling update. Click "Next".

- **Configure network:**
- Select the "Application Load Balancer" under "Load balancer type".
- "Load balancer name": easyguide-alb
- "Container name: port": users:0:5000

- Click "Add to load balancer".

- "Production listener port": 80:HTTP
- "Target group name": easyguide-api-tg
- Do not enable "Service discovery". Click the next button a few times, and then "Create Service".

- You should now have the following Services running, each with a single Task:

![cluster.png](cluster.png)

#### Sanity Check

Navigate to the EC2 Dashboard, and click "Target Groups".

Make sure easyguide-client-tg and easyguide-api-tg have a single registered instance each. Each of the instances should be unhealthy because they failed their respective health checks.

![unhealthy_target_groups.png](unhealthy_target_groups.png)

To get them to pass the health checks, we need to add another inbound rule to the Security Group associated with the containers (which we defined when we configured the Cluster), allowing traffic from the Load Balancer to reach the containers.

#### Inbound Rules

- Within the EC2 Dashboard, click "Security Groups" and select the Security Group associated with the containers, which is the same group assigned to the Load Balancer. Click the "Inbound" tab and then click "Edit"

- Add a new rule:
  - "Type": All traffic
  - "Source": Choose Custom, then add the Security Group ID

![inbound_rules.png](inbound_rules.png)

- Once added, the next time a container is added to each of the Target Groups, the instance should be healthy:

![healthy_target_groups.png](healthy_target_groups.png)

- Essentially, when the Service was spun up, ECS automatically discovered and associated the new Cluster instances with the Application Load Balancer.

![alb-ecs-expanded.png](alb-ecs-expanded.png)

- Next, navigate back to the Load Balancer and grab the "DNS name" from the "Description" tab, and navigate to <http://LOAD_BALANCER_DNS_NAME/ping> in your browser.

`easyguide-alb-468591163.eu-west-3.elb.amazonaws.com`

- If all went well, you should see:

```json
{
  "message": "pong!",
  "status": "success"
}
```

- Try the `/users` endpoint at <http://LOAD_BALANCER_DNS_NAME/users>. You should see a 500 error since the database schema has not been applied.

You should be able to see this in the logs as well:

![logs_500_error.png](logs_500_error.png)

#### Database Schema

We'll need to SSH into the EC2 instance associated with the users service to create the schema.

- First, on the "Services" tab within the created Cluster, click the link for the easyguide-api-service service.*

![ec2_from_service_1.png](ec2_from_service_1.png)

- From there, click on the "Tasks" tab and click the link for the associated Task.
ssh -i ~/.ssh/ecs.pem ec2-user@13.38.115.118
![ec2_from_service_2.png](ec2_from_service_2.png)

- Then, click the link for the EC2 Instance ID and grab the public IP:
![ec2_from_service_3.png](ec2_from_service_3.png)

- SSH into the instance:

```sh
ssh -i ~/.ssh/ecs.pem ec2-user@<EC2_PUBLIC_IP>
```

- You may need to update the permissions on the Pem file -- i.e., `chmod 400 ~/.ssh/ecs.pem`

- Next, grab the Container ID for users (via docker ps), enter the shell within the running container, and then update the database:

```sh
$ docker exec -it Container_ID bash
# python manage.py recreate_db
# python manage.py seed_db
```

![apply_migrations.png](apply_migrations.png)

- Navigate to `http://LOAD_BALANCER_DNS_NAME/users` again and you should see the users. Then, navigate to `http://LOAD_BALANCER_DNS_NAME/`. Manually test registering a new user, logging in, and logging out. Finally, ensure the Swagger UI works as well at `http://LOAD_BALANCER_DNS_NAME/doc`.


## ECS Fargate and Terraform
---

- ECS in **Fargate app mode**: Deploy and manage your applications, not infrastructure. Fargate removes the operational overhead of scaling, patching, securing, and managing servers.
- **Terraform**:  Infrastructure as Code (IaC) tool like Ansible.


### Add 2 new repositories in ECR

- Before jumping into Terraform, navigate to Amazon ECS, click "Repositories", and then add two new repositories:
  - `easyguide-api-fargate`
  - `easyguide-client-fargate`
  - Keep the tags mutable.

- Then, build the production images locally, making sure to use the --build-arg flag to pass in the appropriate arguments:
  AWS_ACCOUNT_ID: 538751540450
  **Note:** Docker desktop is required on your computer to run docker commands

  - `$ docker build -f services/users/Dockerfile.prod -t 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api-fargate:prod ./services/users`

  - `$ docker build -f services/client/Dockerfile.prod -t 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-client-fargate:prod --build-arg NODE_ENV=production --build-arg REACT_APP_API_SERVICE_URL=http://notreal ./services/client`

  - We'll update the REACT_APP_API_SERVICE_URL arg shortly.

### Authenticate the Docker CLI to use the ECR registry

`$ aws ecr get-login-password --region eu-west-3 | docker login --username AWS --password-stdin 538751540450.dkr.ecr.eu-west-3.amazonaws.com`

You should see: Login Succeeded

### Push the images to ECR

**Note:** Docker desktop is required on your computer to run docker commands

AWS_ACCOUNT_ID: 538751540450

Push the new images to ECR:
`$ docker push 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api-fargate:prod`

`$ docker push 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-client-fargate:prod`

### Terraform setup

- Start by installing Terraform. See https://learn.hashicorp.com/tutorials/terraform/install-cli
  - To install Terraform, find the appropriate package for your system and download it as a zip archive.
  - After downloading Terraform, unzip the package. Terraform runs as a single binary named terraform. Any other files in the package can be safely removed and Terraform will still function.
  - Finally, make sure that the terraform binary is available on your PATH. This process will differ depending on your operating system.
  - Run `$ terraform -version` to check the version command
    `Terraform v0.15.5`

- Add a "terraform" folder to your project's root. We'll add each of our Terraform configuration files to this folder.
- Next, define the AWS provider: add a new file to "terraform" called `01_provider.tf`: (See terraform doc for AWS cloud : https://registry.terraform.io/providers/hashicorp/aws/latest/docs)
  ```py 
  provider "aws" {
    region = var.region
  }
  ```

- You'll need to provide your AWS credentials in order to authenticate. Define them as environment variables:

  - On Windows
    - `C:\> setx AWS_ACCESS_KEY_ID "YOUR_AWS_ACCESS_KEY_ID"`
    - `C:\> setx AWS_SECRET_ACCESS_KEY "YOUR_AWS_SECRET_ACCESS_KEY"`

  - On windows (Powershell):
    - `$env:AWS_ACCESS_KEY_ID="YOUR_AWS_ACCESS_KEY_ID"`
    - `$env:AWS_SECRET_ACCESS_KEY="YOUR_AWS_SECRET_ACCESS_KEY"`

  - On linux:
    - `$ export AWS_ACCESS_KEY_ID="YOUR_AWS_ACCESS_KEY_ID"`
    - `$ export AWS_SECRET_ACCESS_KEY="YOUR_AWS_SECRET_ACCESS_KEY"`


- Create `variables.tf`
```py 
# core

variable "region" {
  description = "The AWS region to create resources in."
  default     = "eu-west-3"
}
```

- Run into terraform folder, `$ terraform init` with CMD to create a new Terraform working directory and download the AWS provider.
With that we can start defining each piece of the AWS infrastructure:
  - Networking:
    - Virtual Private Cloud (VPC)
    - Public and private subnets
    - Routing tables
    - Internet Gateway
    - Key Pairs
  - Security Groups
  - Load Balancers, Listeners, and Target Groups
  - IAM Roles and Policies
  - Health Checks and Logs
  - ECS:
    - Task Definitions
    - Cluster
    - Service 


### Terraform Networking
- Create `02_network.tf` to define our network resources
Here, we defined the following resources:

- Virtual Private Cloud (VPC): https://aws.amazon.com/vpc/
- Public and private subnets: https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html
- Route tables: https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Route_Tables.html
- Internet Gateway: https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html
- Key Pairs

```py 
# Production VPC
resource "aws_vpc" "production-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
}

# Public subnets
resource "aws_subnet" "public-subnet-1" {
  cidr_block        = var.public_subnet_1_cidr
  vpc_id            = aws_vpc.production-vpc.id
  availability_zone = var.availability_zones[0]
}
resource "aws_subnet" "public-subnet-2" {
  cidr_block        = var.public_subnet_2_cidr
  vpc_id            = aws_vpc.production-vpc.id
  availability_zone = var.availability_zones[1]
}

# Private subnets
resource "aws_subnet" "private-subnet-1" {
  cidr_block        = var.private_subnet_1_cidr
  vpc_id            = aws_vpc.production-vpc.id
  availability_zone = var.availability_zones[0]
}
resource "aws_subnet" "private-subnet-2" {
  cidr_block        = var.private_subnet_2_cidr
  vpc_id            = aws_vpc.production-vpc.id
  availability_zone = var.availability_zones[1]
}

# Route tables for the subnets
resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.production-vpc.id
}
resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.production-vpc.id
}

# Associate the newly created route tables to the subnets
resource "aws_route_table_association" "public-route-1-association" {
  route_table_id = aws_route_table.public-route-table.id
  subnet_id      = aws_subnet.public-subnet-1.id
}
resource "aws_route_table_association" "public-route-2-association" {
  route_table_id = aws_route_table.public-route-table.id
  subnet_id      = aws_subnet.public-subnet-2.id
}
resource "aws_route_table_association" "private-route-1-association" {
  route_table_id = aws_route_table.private-route-table.id
  subnet_id      = aws_subnet.private-subnet-1.id
}
resource "aws_route_table_association" "private-route-2-association" {
  route_table_id = aws_route_table.private-route-table.id
  subnet_id      = aws_subnet.private-subnet-2.id
}

# Elastic IP
resource "aws_eip" "elastic-ip-for-nat-gw" {
  vpc                       = true
  associate_with_private_ip = "10.0.0.5"
  depends_on                = [aws_internet_gateway.production-igw]
}

# NAT gateway
resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.elastic-ip-for-nat-gw.id
  subnet_id     = aws_subnet.public-subnet-1.id
  depends_on    = [aws_eip.elastic-ip-for-nat-gw]
}
resource "aws_route" "nat-gw-route" {
  route_table_id         = aws_route_table.private-route-table.id
  nat_gateway_id         = aws_nat_gateway.nat-gw.id
  destination_cidr_block = "0.0.0.0/0"
}

# Internet Gateway for the public subnet
resource "aws_internet_gateway" "production-igw" {
  vpc_id = aws_vpc.production-vpc.id
}

# Route the public subnet traffic through the Internet Gateway
resource "aws_route" "public-internet-igw-route" {
  route_table_id         = aws_route_table.public-route-table.id
  gateway_id             = aws_internet_gateway.production-igw.id
  destination_cidr_block = "0.0.0.0/0"
}

```

Add the required variables in `variables.tf`:
```py 

# networking

variable "public_subnet_1_cidr" {
  description = "CIDR Block for Public Subnet 1"
  default     = "10.0.1.0/24"
}
variable "public_subnet_2_cidr" {
  description = "CIDR Block for Public Subnet 2"
  default     = "10.0.2.0/24"
}
variable "private_subnet_1_cidr" {
  description = "CIDR Block for Private Subnet 1"
  default     = "10.0.3.0/24"
}
variable "private_subnet_2_cidr" {
  description = "CIDR Block for Private Subnet 2"
  default     = "10.0.4.0/24"
}
variable "availability_zones" {
  description = "Availability zones"
  type        = list(string)
  default     = ["eu-west-3b", "eu-west-3c"]
}
```

- Run `terraform plan` with CMD into terraform folder to generate and show the execution plan based on the defined configuration.


- create `07_keypair.tf`
Provides an EC2 key pair resource. A key pair is used to control login access to EC2 instances. 
See https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair
See tutorial https://jhooq.com/terraform-ssh-into-aws-ec2/

```py
resource "aws_key_pair" "production" {
  key_name   = "${var.ecs_cluster_name}_key_pair"
  public_key = file(var.ssh_pubkey_file)
}
```

Add the required variables in `variables.tf`:
```py 
# key pair

variable "ssh_pubkey_file" {
  description = "Path to an SSH public key"
  default     = "~/.ssh/id_rsa.pub"
}
```


- **About Key Pair for SSH Security access**
To create a new EC2 Key Pair, so we can SSH into the EC2 instances managed by ECS, navigate to Amazon EC2, click "Key Pairs" on the sidebar, and then click the "Create Key Pair" button.
Name the new key pair `ecs` and add the automatically donloaded file `ecs.ppk` (by your browser) to "~/.ssh".

  - For Name, enter a descriptive name for the key pair. Amazon EC2 associates the public key with the name that you specify as the key name. A key name can include up to 255 ASCII characters. It can’t include leading or trailing spaces.

  - For Key pair type, choose either `RSA` or `ED25519`. Note that `ED25519` keys are not supported for Windows instances, EC2 Instance Connect, or EC2 Serial Console.

  - For Private key file format, choose the format in which to save the private key. To save the private key in a format that can be used **with OpenSSH**, choose `pem`. To save the private key in a format that can be used with **PuTTY**, choose `ppk`.
    - If you chose `ED25519` in the previous step, the Private key file format options do not appear, and the private key format defaults to pem.
    - Select the file format of the private key : .pem to use with OpenSSH
  
  - Choose Create key pair.

  - The private key file is automatically downloaded by your browser. The base file name is the name that you specified as the name of your key pair, and the file name extension is determined by the file format that you chose. Save the private key file in a safe place.
  **Important: This is the only chance for you to save the private key file.**
    - If you will use an SSH client on a macOS or Linux computer to connect to your Linux instance, use the following command to set the permissions of your private key file so that only you can read it.
    `$ chmod 400 my-key-pair.pem`
    - If you do not set these permissions, then you cannot connect to your instance using this key pair. For more information, see Error: Unprotected private key file.

  - See more options on : <https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html>



### Terraform Security Groups

- Create `03_securitygroups.tf` to protect the apps and ECS cluster, let's configure Security Groups
```py 
# ALB Security Group (Traffic Internet -> ALB)
resource "aws_security_group" "load-balancer" {
  name        = "load_balancer_security_group"
  description = "Controls access to the ALB"
  vpc_id      = aws_vpc.production-vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# ECS Security group (traffic ALB -> ECS)
resource "aws_security_group" "ecs" {
  name        = "ecs_security_group"
  description = "Allows inbound access from the ALB only"
  vpc_id      = aws_vpc.production-vpc.id

  ingress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    security_groups = [aws_security_group.load-balancer.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
```

### Terraform Load Balancers, Listeners, and Target Groups

- Create `04_loadbalancer.tf` to configure an Application Load Balancer (ALB) along with the appropriate Target Group and Listener.

So, we configured our load balancer and listener to listen for HTTP requests on port 80.

Take note of the URLs for the health checks: / and /ping/.

```py 
# Production Load Balancer
resource "aws_lb" "production" {
  name               = "${var.ecs_cluster_name}-alb"
  load_balancer_type = "application"
  internal           = false
  security_groups    = [aws_security_group.load-balancer.id]
  subnets            = [aws_subnet.public-subnet-1.id, aws_subnet.public-subnet-2.id]
}

# Target group client
resource "aws_alb_target_group" "default-target-group" {
  name        = "${var.ecs_cluster_name}-client-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.production-vpc.id
  target_type = "ip"

  health_check {
    path                = var.health_check_path_client
    port                = "traffic-port"
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200"
  }
}

# Target group api
resource "aws_alb_target_group" "api-target-group" {
  name        = "${var.ecs_cluster_name}-api-tg"
  port        = 5000
  protocol    = "HTTP"
  vpc_id      = aws_vpc.production-vpc.id
  target_type = "ip"

  health_check {
    path                = var.health_check_path_api
    port                = "traffic-port"
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 5
    matcher             = "200"
  }
}

# Listener (redirects traffic from the load balancer to the client target group)
resource "aws_alb_listener" "ecs-alb-http-listener" {
  load_balancer_arn = aws_lb.production.id
  port              = "80"
  protocol          = "HTTP"
  depends_on        = [aws_alb_target_group.default-target-group]

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.default-target-group.arn
  }
}

// User Listener Rule (redirects traffic from the load balancer to the target group)
resource "aws_lb_listener_rule" "static" {
  listener_arn = aws_alb_listener.ecs-alb-http-listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.api-target-group.arn
  }

  condition {
    path_pattern {
      values = ["/users*", "/ping", "/auth*", "/doc", "/swagger"]
    }
  }
}

```

Add the required variables in `variables.tf`:
```py 

# load balancer

variable "health_check_path_client" {
  description = "Health check path for the default target group"
  default     = "/"
}
variable "health_check_path_api" {
  description = "Health check path for the default target group"
  default     = "/ping"
}


# ecs

variable "ecs_cluster_name" {
  description = "Name of the ECS cluster"
  default     = "easyguide-fargate"
}
```

### Terraform IAM Roles and Policies


- Create `05_iam.tf`
```py 
resource "aws_iam_role" "ecs-host-role" {
  name               = "ecs_host_role_prod"
  assume_role_policy = file("policies/ecs-role.json")
}

resource "aws_iam_role_policy" "ecs-instance-role-policy" {
  name   = "ecs_instance_role_policy"
  policy = file("policies/ecs-instance-role-policy.json")
  role   = aws_iam_role.ecs-host-role.id
}

resource "aws_iam_role" "ecs-service-role" {
  name               = "ecs_service_role_prod"
  assume_role_policy = file("policies/ecs-role.json")
}

resource "aws_iam_role_policy" "ecs-service-role-policy" {
  name   = "ecs_service_role_policy"
  policy = file("policies/ecs-service-role-policy.json")
  role   = aws_iam_role.ecs-service-role.id
}

resource "aws_iam_instance_profile" "ecs" {
  name = "ecs_instance_profile_prod"
  path = "/"
  role = aws_iam_role.ecs-host-role.name
}
```

- Add a new folder in "terraform" called "policies". Then, add the following role and policy definitions:

- `ecs-role.json`:
```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "ecs.amazonaws.com",
          "ec2.amazonaws.com",
          "ecs-tasks.amazonaws.com"
        ]
      },
      "Effect": "Allow"
    }
  ]
}
```

- `ecs-instance-role-policy.json`:
```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ecs:*",
        "ec2:*",
        "elasticloadbalancing:*",
        "ecr:*",
        "cloudwatch:*",
        "s3:*",
        "rds:*",
        "logs:*"
      ],
      "Resource": "*"
    }
  ]
}
```

- `ecs-service-role-policy.json`:
```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "elasticloadbalancing:Describe*",
        "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
        "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
        "ec2:Describe*",
        "ec2:AuthorizeSecurityGroupIngress",
        "elasticloadbalancing:RegisterTargets",
        "elasticloadbalancing:DeregisterTargets"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
```

### Terraform Health Checks and Logs

- Create `06_logs.tf`
```py 
resource "aws_cloudwatch_log_group" "client-log-group" {
  name              = "/ecs/client-app"
  retention_in_days = var.log_retention_in_days
}

resource "aws_cloudwatch_log_stream" "client-log-stream" {
  name           = "client-app-log-stream"
  log_group_name = aws_cloudwatch_log_group.client-log-group.name
}

resource "aws_cloudwatch_log_group" "api-log-group" {
  name              = "/ecs/api-app"
  retention_in_days = var.log_retention_in_days
}

resource "aws_cloudwatch_log_stream" "api-log-stream" {
  name           = "api-app-log-stream"
  log_group_name = aws_cloudwatch_log_group.api-log-group.name
}
```

- Add the required variables in `variables.tf`:
```py
# logs

variable "log_retention_in_days" {
  default = 30
}
```

### Terraform ECS

Now, we can configure our ECS cluster.

- Create `08_ecs.tf`
```py 
resource "aws_ecs_cluster" "fargate" {
  name = "${var.ecs_cluster_name}-cluster"
}

data "template_file" "client-app" {
  template = file("templates/client_app.json.tpl")

  vars = {
    docker_image_url_client = var.docker_image_url_client
    region                  = var.region
  }
}

data "template_file" "api-app" {
  template = file("templates/api_app.json.tpl")

  vars = {
    docker_image_url_api = var.docker_image_url_api
    region                 = var.region
    secret_key             = var.secret_key
  }
}

resource "aws_ecs_task_definition" "client-app" {
  family                   = "client-app"
  task_role_arn            = aws_iam_role.ecs-host-role.arn
  execution_role_arn       = aws_iam_role.ecs-host-role.arn
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = "512"
  cpu                      = "256"
  container_definitions    = data.template_file.client-app.rendered
}

resource "aws_ecs_task_definition" "api-app" {
  family                   = "api-app"
  task_role_arn            = aws_iam_role.ecs-host-role.arn
  execution_role_arn       = aws_iam_role.ecs-host-role.arn
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = "512"
  cpu                      = "256"
  container_definitions    = data.template_file.api-app.rendered
}

resource "aws_ecs_service" "client-fargate" {
  name            = "${var.ecs_cluster_name}-client-service"
  cluster         = aws_ecs_cluster.fargate.id
  task_definition = aws_ecs_task_definition.client-app.arn
  desired_count   = var.app_count
  depends_on      = [aws_alb_listener.ecs-alb-http-listener, aws_iam_role_policy.ecs-service-role-policy]
  launch_type     = "FARGATE"

  load_balancer {
    target_group_arn = aws_alb_target_group.default-target-group.arn
    container_name   = "client-app"
    container_port   = 80
  }

  network_configuration {
    security_groups  = [aws_security_group.ecs.id]
    subnets          = [aws_subnet.public-subnet-1.id, aws_subnet.public-subnet-2.id]
    assign_public_ip = true
  }
}

resource "aws_ecs_service" "api-fargate" {
  name            = "${var.ecs_cluster_name}-api-service"
  cluster         = aws_ecs_cluster.fargate.id
  task_definition = aws_ecs_task_definition.api-app.arn
  desired_count   = var.app_count
  depends_on      = [aws_alb_listener.ecs-alb-http-listener, aws_iam_role_policy.ecs-service-role-policy]
  launch_type     = "FARGATE"

  load_balancer {
    target_group_arn = aws_alb_target_group.api-target-group.arn
    container_name   = "api-app"
    container_port   = 5000
  }

  network_configuration {
    security_groups  = [aws_security_group.ecs.id]
    subnets          = [aws_subnet.public-subnet-1.id, aws_subnet.public-subnet-2.id]
    assign_public_ip = true
  }
}
```


- Add a `templates` folder to the "terraform" folder, and then add a new template file called `client_app.json.tpl`:
  See our container definition : https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_ContainerDefinition.html
```json
[
  {
    "name": "client-app",
    "image": "${docker_image_url_client}",
    "essential": true,
    "links": [],
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 80,
        "protocol": "tcp"
      }
    ],
    "environment": [],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "/ecs/client-app",
        "awslogs-region": "${region}",
        "awslogs-stream-prefix": "client-app-log-stream"
      }
    }
  }
]
```

- Do the same for the Flask app in a file called `api_app.json.tpl`
```json
[
  {
    "name": "api-app",
    "image": "${docker_image_url_api}",
    "essential": true,
    "links": [],
    "portMappings": [
      {
        "containerPort": 5000,
        "hostPort": 5000,
        "protocol": "tcp"
      }
    ],
    "environment": [
      {
        "name": "APP_SETTINGS",
        "value": "src.config.ProductionConfig"
      },
      {
        "name": "DATABASE_TEST_URL",
        "value": "postgres://postgres:postgres@api-db:5432/api_test"
      },
      {
        "name": "SECRET_KEY",
        "value": "${secret_key}"
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "/ecs/api-app",
        "awslogs-region": "${region}",
        "awslogs-stream-prefix": "api-app-log-stream"
      }
    }
  }
]
```


Add the required variables in `variables.tf`:
> Be sure to replace <AWS_ACCOUNT_ID> with your AWS account ID and update the value of the secret key. Refer to the Linux Amazon ECS-optimized AMIs guide to find a list of AMIs with Docker pre-installed.
See https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html#ecs-optimized-ami-linux

```py
# ecs

variable "ecs_cluster_name" {
  description = "Name of the ECS cluster"
  default     = "easyguide-fargate"
}
variable "docker_image_url_client" {
  description = "Docker client image to run in the ECS cluster"
  default     = "538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-client-fargate:prod"
}
variable "docker_image_url_api" {
  description = "Docker api image to run in the ECS cluster"
  default     = "538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api-fargate:prod"
}
variable "app_count" {
  description = "Number of Docker containers to run"
  default     = 1
}
variable "secret_key" {
  description = "Flask Secret Key"
  default     = "foobar"
}
```

- Since we added the Template provider, run `terraform init` again to download the new provider.
  > Remove the first declaration of the variable "ecs_cluster_name" into `variables.tf`

- Create `outputs.tf` for Sanity check
Here, we configured an outputs.tf file along with an output value called alb_hostname. After we execute the Terraform plan, to spin up the AWS infrastructure, the load balancer's DNS name will be outputted to the terminal.
```py 
output "alb_hostname" {
  value = aws_lb.production.dns_name
}
```


- **Ready?!?** View then execute the plan:
  - `$ terraform plan`
  - `$ terraform apply`

- It will take a few minutes to spin up the resources. Once complete, grab the Load Balancer's DNS hostname that was outputted to your terminal:

```sh
Outputs:

alb_hostname = flask-react-fargate-alb-715755245.us-west-1.elb.amazonaws.com
```

- Navigate to http://LOAD_BALANCER_DNS_NAME/ping in your browser.

- If all went well, you should see:
```json
{
  "message": "pong!",
  "status": "success"
}
```

### Terraform RDS

Next, let's configure RDS.

- Add a new Security Group to `03_securitygroups.tf` to ensure that only traffic from your ECS instance can talk to the database:

```py
# RDS Security Group (traffic ECS -> RDS)
resource "aws_security_group" "rds" {
  name        = "rds-security-group"
  description = "Allows inbound access from ECS only"
  vpc_id      = aws_vpc.production-vpc.id

  ingress {
    protocol        = "tcp"
    from_port       = "3306"
    to_port         = "3306"
    security_groups = [aws_security_group.ecs.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
```
- Next, add a new file called `09_rds.tf` for setting up the database itself:

```py
resource "aws_db_subnet_group" "production" {
  name       = "main"
  subnet_ids = [aws_subnet.private-subnet-1.id, aws_subnet.private-subnet-2.id]
}

resource "aws_db_instance" "production" {
  identifier              = "production"
  name                    = var.rds_db_name
  username                = var.rds_username
  password                = var.rds_password
  port                    = "3306"
  engine                  = "mysql"
  engine_version          = "8.0"
  instance_class          = var.rds_instance_class
  allocated_storage       = "20"
  storage_encrypted       = false
  vpc_security_group_ids  = [aws_security_group.rds.id]
  db_subnet_group_name    = aws_db_subnet_group.production.name
  multi_az                = false
  storage_type            = "gp2"
  publicly_accessible     = false
  backup_retention_period = 7
  skip_final_snapshot     = true
}
```

- Variables:
Note that we left off the default value for the password. More on this in a bit.

```py
# rds

variable "rds_db_name" {
  description = "RDS database name"
  default     = "api_prod"
}
variable "rds_username" {
  description = "RDS database username"
  default     = "webapp"
}
variable "rds_password" {
  description = "RDS database password"
}
variable "rds_instance_class" {
  description = "RDS instance type"
  default     = "db.t2.micro"
}

```

- Since we'll need to know the address for the instance in our Flask app, add a `depends_on` argument to the `aws_ecs_task_definition` in `08_ecs.tf`:
```py

resource "aws_ecs_task_definition" "users-app" {
  family                   = "users-app"
  task_role_arn            = aws_iam_role.ecs-host-role.arn
  execution_role_arn       = aws_iam_role.ecs-host-role.arn
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = "512"
  cpu                      = "256"
  container_definitions    = data.template_file.users-app.rendered
  depends_on               = [aws_db_instance.production]
}
```

- Do the same for the template:
We also passed in the database_url to the template.
```py
data "template_file" "users-app" {
  template = file("templates/users_app.json.tpl")
  depends_on               = [aws_db_instance.production]

  vars = {
    docker_image_url_users = var.docker_image_url_users
    region                 = var.region
    secret_key             = var.secret_key
    database_url           = "mysql://webapp:${var.rds_password}@${aws_db_instance.production.endpoint}/api_prod"
  }
}
```

- Update the environment section in the `api_app.json.tpl` template:
```py
"environment": [
  {
    "name": "APP_SETTINGS",
    "value": "src.config.ProductionConfig"
  },
  {
    "name": "DATABASE_TEST_URL",
    "value": "mysql://mysql:mysql@api-db:3306/api_test"
  },
  {
    "name": "SECRET_KEY",
    "value": "${secret_key}"
  },
  {
    "name": "DATABASE_URL",
    "value": "${database_url}"
  }
],
```

- To update the ECS Task Definition, create the RDS resources, and update the Service, run:

`$ terraform apply`

- Since we didn't set a default for the password, you'll be prompted to enter one:

```
var.rds_password
  RDS database password

  Enter a value:
```

- Rather than having to pass a value in each time, you could set an environment variable like so:

On Windows: `C:\> setx TF_VAR_rds_password foobarbaz`

```
$ export TF_VAR_rds_password=foobarbaz

$ terraform apply
```

- > **Note:** Keep in mind that this approach, of using environment variables, keeps sensitive variables out of the .tf files, but they are still stored in the `terraform.tfstate` file in plain text. So, be sure to keep this file out of version control. 
Since keeping it out of version control doesn't work if other people on your team need access to it, 
look to either **encrypting the secrets** 
or using a secret store like **Vault** ( see https://www.vaultproject.io/)
or **AWS Secrets Manager**( see https://aws.amazon.com/secrets-manager/)



- After the new Tasks are registered with the Target Group, try the `/users` endpoint at `http://LOAD_BALANCER_DNS_NAME/users`. You should see a 500 error since the database schema has not been applied.

- Since we don't have control over the underlying infrastructure with Fargate, we cannot SSH into the box to run docker exec commands like we did with ECS in EC2 mode. So, how do we get around this?
Let's add an entrypoint file to apply the migrations. Create a new file called `services/users/entrypoint.prod.sh`:
```sh
#!/bin/sh


python manage.py recreate_db

exec "$@"
```

- Add the entrypoint along with the command to `api_app.json.tpl`:
```sh
[
  {
    "name": "api-app",
    "image": "${docker_image_url_api}",
    "essential": true,
    "links": [],
    "portMappings": [
      {
        "containerPort": 5000,
        "hostPort": 5000,
        "protocol": "tcp"
      }
    ],
    "command": [
      "gunicorn",
      "manage:app",
      "-b",
      ":5000",
      "-w",
      "3",
      "--log-level=debug"
    ],
    "entrypoint": [
      "/usr/src/app/entrypoint.prod.sh"
    ],
    "environment": [
      {
        "name": "APP_SETTINGS",
        "value": "src.config.ProductionConfig"
      },
      {
        "name": "DATABASE_TEST_URL",
        "value": "mysql://mysql:mysql@api-db:3306/api_test"
      },
      {
        "name": "SECRET_KEY",
        "value": "${secret_key}"
      },
      {
        "name": "DATABASE_URL",
        "value": "${database_url}"
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "/ecs/api-app",
        "awslogs-region": "${region}",
        "awslogs-stream-prefix": "api-app-log-stream"
      }
    }
  }
]
```


- Remove the command from `services/users/Dockerfile.prod`:
```
# pull official base image
FROM python:3.9.5-slim-buster

# set working directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install system dependencies
RUN apt-get update \
  && apt-get -y install netcat \
  && apt-get clean

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# add app
COPY . .

# run server
# CMD gunicorn -b 0.0.0.0:5000 manage:app
```

- Next, update ECR:

```sh
$ chmod +x services/users/entrypoint.sh

$ docker build -f services/users/Dockerfile.prod -t 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api-fargate:prod ./services/users

$ docker push 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api-fargate:prod
```

- Update:

`$ terraform apply`


- Navigate to `http://LOAD_BALANCER_DNS_NAME/users` again and you should see and empty array. Then, navigate to `http://LOAD_BALANCER_DNS_NAME/`. Try registering a new user. You should see an error since we're making a request to `http://notreal/register`.

- Build and push the image again, making sure to use the real load Balancer DNS hostname:
```sh
docker build -f services/client/Dockerfile.prod -t 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api-fargate:prod --build-arg NODE_ENV=production --build-arg REACT_APP_API_SERVICE_URL=http://<LOAD_BALANCER_DNS_NAME> ./services/client

$ docker push 538751540450.dkr.ecr.eu-west-3.amazonaws.com/easyguide-api-fargate:prod
```

- Manually create a new task definition and update the service. 
- Once done, manually test registering a new user, logging in, and logging out.

- You can bring all infrastructure down via:

`$ terraform destroy`


# 6. Next steps

---
Looking for more?

- **Infrastructure as Code:** Replace the deploy.sh script and JSON templates with - either **Terraform** or Cloud​Formation templates, both of which are **infrastructure - as code tools** that you can leverage for building and managing infrastructure.
- **AWS CLI:** As your app and infrastructure scale, you'll want to manage your AWS - resources from the CLI rather than the console so you automate everything. This - ties in to infrastructure as code as well. Start by reviewing the official ECS - CLI. Alternatively, if you're using Fargate, try the Fargate CLI.
- **Deployment Strategies:** Review the different deployment types from the Amazon ECS - Deployment Types guide and try a different strategy.
- **Domain Name and HTTPS:** Set up Route 53 to link a domain name and Certificate - Manager to configure an SSL certificate. You'll need to add an HTTPS listener to - the Application Load Balancer, and then proxy 443 traffic to HTTP ports 80 and - 5000 on the instances via Target Groups.

There's plenty of refactoring that needs to be done on the application side. Try removing the complicated logic out of the React components and into basic JavaScript functions that you can import. This will make it easier to scale and test your app going forward.

- Some ideas:

- **Test coverage:** Add more tests to increase the overall test coverage, making sure - to cover any remaining edge cases.
- **Unit tests:** Add unit tests (via monkeypatch) to cover the auth routes.
- **DRY out the code:** There's plenty of places in the code base that could be - refactored.
- **Flask CORS:** Instead of allowing requests from any domain, lock down the Flask - service by only allowing requests that originate from the domain.
- **Caching:** Add caching (where appropriate) with Flask-Caching.
- **Duplicate usernames:** Prevent duplicate usernames in the database.
- **Invalidate refresh tokens:** Users can have a number of active refresh tokens. It - may be worth controlling this to prevent abuse by only allowing a user to have a - single refresh token at time. Create a database table for this and update the - client and server-side logic.
- **Blacklist tokens:** You may want to create a database table for used access and - refresh tokens to prevent abuse. Again, update the client and server-side as - necessary.
- **Role based authorization:** Add role based authorization. Refer to the "Auth" - section in Awesome Flask for more info.
- **Cross tab logout:** Incorporate cross browser tab logout by adding an event - listener on the refresh token in LocalStorage.
- **Transactional emailing:** Add the ability to send transactional emails for email - confirmation and password changes.
- **Client side:** Add the ability to update a user using the same modal configured - for adding a user and prevent the currently logged in user from deleting - themselves in the table.
- **Hooks:** Refactor the class-based React components to functions with React Hooks. - Refer to Primer on React Hooks for more info on Hooks.

Finally, make sure you tear down all the AWS infrastructure if you haven't - already done so. Run terraform destroy to bring down all the Terraform-based - infrastructure.

---
Be sure to check out the `Scalable Flask Applications on AWS` course to learn more about building scalable Flask applications running on AWS infrastructure managed by Terraform.
See <https://testdriven.io/courses/scalable-flask-aws/>

# 7. CloudWatch: Monitoring tool

---

# Annexes
---

## Troubleshoots

---

### API micro services

Are `/users` endpoints for crud services and `/auth` endpoints for authentification services should be in the same api folder called `users` (include the same docker container)
?

### Export env variables in your local desktop

---

on linux
`export REACT_APP_API_SERVICE_URL=http://localhost:5004`

become On Windows
`C:\> setx REACT_APP_API_SERVICE_URL "http://localhost:5004"`

or on windows (Powershell)
`$env:REACT_APP_API_SERVICE_URL="http://localhost:5004"`



### Modules pytest, flash8, etc. not found
?

### Github link for Codebuild: Error 404
?

### SSH public key file problem with terraform (see Terraform 'Key Pairs' section)
?



## Credits
---
- https://testdriven.io/courses/aws-flask-react/
- https://testdriven.io/courses/scalable-flask-aws/#Overview

