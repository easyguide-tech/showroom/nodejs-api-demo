DB Optimization
===========
Checkpoints and good practices for performance optimization
---

MongoDB gives you the ability to design your database schema to match the needs of your application. You can structure your data in MongoDB so that it adapts easily to change, and supports the queries and updates that you need to get the most out of your application. **Structure your data to match the ways that your application queries and updates it.**

---
**TABLE OF CONTENTS**
---

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Data structure and relationships](#data-structure-and-relationships)
  - [Highlight the main criterias of the relationships:](#highlight-the-main-criterias-of-the-relationships)
  - [Relationships patterns:](#relationships-patterns)
    - [For Modeling “one-to-few”](#for-modeling-one-to-few)
    - [For Modeling “one-to-many”](#for-modeling-one-to-many)
    - [For Modeling “one-to-squillions” (parent-referencing)](#for-modeling-one-to-squillions-parent-referencing)
    - [For Modeling  Two-Way Referencing](#for-modeling-two-way-referencing)
    - [Denormalizing patterns](#denormalizing-patterns)
- [Performance Optimization](#performance-optimization)
  - [Performance Results & Observations](#performance-results-observations)
  - [LEAN Function](#lean-function)
  - [SELECT function](#select-function)
  - [INDEXING operation](#indexing-operation)
  - [Minimise DB requests](#minimise-db-requests)
- [Annexes](#annexes)
  - [Tools](#tools)
  - [Sources](#sources)

<!-- /code_chunk_output -->
---


# Data structure and relationships
---

## Highlight the main criterias of the relationships:
* Define the cardinality of the relationship: is it “one-to-few”, “one-to-many”, or “one-to-squillions”?
* Check if you need to access the object on the “N” side separately, or only in the context of the parent object?
* Identify the ratio of updates to reads for a particular field?


## Relationships patterns:
---

### For Modeling “one-to-few”
you can use an array of embedded documents.

* **Advantage:** you don’t have to perform a separate query to get the embedded details.
* **Disadvantage:** you have no way of accessing the embedded details as stand-alone entities
* **Example:** the addresses for a person. This is a good use case for embedding.

```js
> db.person.findOne()
{
name: 'Kate Monster',
ssn: '123-456-7890',
addresses : [
    { street: '123 Sesame St', city: 'Anytown', cc: 'USA' },
    { street: '123 Avenue Q', city: 'New York', cc: 'USA' }
]
}
```
* **Alert:** Arrays should not grow without bound. If there are more than a couple of hundred documents on the “many” side, don’t embed them; if there are more than a few thousand documents on the “many” side, don’t use an array of ObjectID references. High-cardinality arrays are a compelling reason not to embed.


### For Modeling “one-to-many”
or on occasions when the “N” side must stand alone, you should use an array of references. You can also use a “parent-reference” on the “N” side if it optimizes your data access pattern.

* **Advantage:** Each Part is a stand-alone document, so it’s easy to search them and update them independently. This schema lets you have individual Parts used by multiple Products, so your One-to-N schema just became an N-to-N schema without any need for a join table!
* **Disadvantage:** Have to perform a second query to get details about the Parts for a Product. (But hold that thought until we get to denormalizing )
* **Example:** Parts for a product in a replacement parts ordering system. Each product may have up to several hundred replacement parts, but never more than a couple thousand or so. 

* Each Part would have its own document:
```js
> db.parts.findOne()
{
    _id : ObjectID('AAAA'),
    partno : '123-aff-456',
    name : '#4 grommet',
    qty: 94,
    cost: 0.94,
    price: 3.99
}
```

* Each Product would have its own document, which would contain an array of ObjectID references to the Parts that make up that Product:
```js
> db.products.findOne()
{
    name : 'left-handed smoke shifter',
    manufacturer : 'Acme Corp',
    catalog_number: 1234,
    parts : [     // array of references to Part documents
        ObjectID('AAAA'),    // reference to the #4 grommet above
        ObjectID('F17C'),    // reference to a different Part
        ObjectID('D2AA'),
        // etc
    ]
}
```

* You would then use an application-level join to retrieve the parts for a particular product:
```js
// Fetch the Product document identified by this catalog number
> product = db.products.findOne({catalog_number: 1234});
// Fetch all the Parts that are linked to this Product
> product_parts = db.parts.find({_id: { $in : product.parts } } ).toArray() ;
```

* **Denormalizing from Many -> One** so you don’t have to perform the application-level join when displaying all of the part names for the product, but you would have to perform that join if you needed any other information about a part.
```js
> db.products.findOne()
{
    name : 'left-handed smoke shifter',
    manufacturer : 'Acme Corp',
    catalog_number: 1234,
    parts : [
        { id : ObjectID('AAAA'), name : '#4 grommet' },         // Part name is denormalized
        { id: ObjectID('F17C'), name : 'fan blade assembly' },
        { id: ObjectID('D2AA'), name : 'power switch' },
        // etc
    ]
}
```

* **Denormalizing from One -> Many** so you don’t have to perform the application-level join when displaying all of the part names for the product, but if you’ve denormalized the Product name into the Part document, then when you update the Product name you must also update every place it occurs in the ‘parts’ collection.
```js
> db.parts.findOne()
{
    _id : ObjectID('AAAA'),
    partno : '123-aff-456',
    name : '#4 grommet',
    product_name : 'left-handed smoke shifter',   // Denormalized from the ‘Product’ document
    product_catalog_number: 1234,                     // Ditto
    qty: 94,
    cost: 0.94,
    price: 3.99
}
```

### For Modeling “one-to-squillions” (parent-referencing)
You should use a “parent-reference” in the document storing the “N” side.
* **Advantage:** 
* **Disadvantage:** 
* **Example:** An event logging system that collects log messages for different machines. 

* You’d have a document for the host, and then store the ObjectID of the host in the documents for the log messages.
```js
> db.hosts.findOne()
{
    _id : ObjectID('AAAB'),
    name : 'goofy.example.com',
    ipaddr : '127.66.66.66'
}

> db.logmsg.findOne()
{
    time : ISODate("2014-03-28T09:42:41.382Z"),
    message : 'cpu is on fire!',
    host: ObjectID('AAAB')       // Reference to the Host document
}
```
* You’d use a (slightly different) application-level join to find the most recent 5,000 messages for a host:
```js
    // find the parent ‘host’ document
> host = db.hosts.findOne({ipaddr : '127.66.66.66'});  // assumes unique index
// find the most recent 5000 log message documents linked to that host
> last_5k_msg = db.logmsg.find({host: host._id}).sort({time : -1}).limit(5000).toArray()
```

### For Modeling  Two-Way Referencing
To have both references from the “one” side to the “many” side and references from the “many” side to the “one” side.
* **Advantage:** All of the advantages of the “One-to-Many” schema. 
* **Disadvantage:** All of the disadvantages of the “One-to-Many” schema, but with some additions. If you need to reassign the task to another person, you need to perform two updates instead of just one. you’ll have to update both the reference from the Person to the Task document, and the reference from the Task to the Person.  
* **Example:** There’s a “people” collection holding Person documents, a “tasks” collection holding Task documents, and a One-to-N relationship from Person -> Task. The application will need to track all of the Tasks owned by a Person, so we will need to reference Person -> Task.

* With the array of references to Task documents, a single Person document might look like this:
```js
> db.person.findOne()
{
    _id: ObjectID("AAF1"),
    name: "Kate Monster",
    tasks [     // array of references to Task documents
        ObjectID("ADF9"), 
        ObjectID("AE02"),
        ObjectID("AE73") 
        // etc
    ]
}
```
* In some other contexts this application will display a list of Tasks and it will need to quickly find which Person is responsible for each Task. You can optimize this by putting an additional reference to the Person in the Task document. 
```js
> db.tasks.findOne()
{
    _id: ObjectID("ADF9"), 
    description: "Write lesson plan",
    due_date:  ISODate("2014-04-01"),
    owner: ObjectID("AAF1")     // Reference to Person document
}
```

* **Alert:** Arrays should not grow without bound. If there are more than a couple of hundred documents on the “many” side, don’t embed them; if there are more than a few thousand documents on the “many” side, don’t use an array of ObjectID references. High-cardinality arrays are a compelling reason not to embed.

* **Denormalizing With “One-To-Squillions” Relationships** 
This works in one of two ways: you can either put information about the “one” side (from the 'hosts’ document) into the “squillions” side (the log entries), or you can put summary information from the “squillions” side into the “one” side.

* An example of denormalizing into the “squillions” side. I’m going to add the IP address of the host (from the ‘one’ side) into the individual log message:
```js
> db.logmsg.findOne()
{
    time : ISODate("2014-03-28T09:42:41.382Z"),
    message : 'cpu is on fire!',
    ipaddr : '127.66.66.66',
    host: ObjectID('AAAB')
}
```
* Your query for the most recent messages from a particular IP address just got easier: it’s now just one query instead of two.
```js
> last_5k_msg = db.logmsg.find({ipaddr : '127.66.66.66'}).sort({time : -1}).limit(5000).toArray()
```

### Denormalizing patterns

You’d do this only for fields that are frequently read, get read much more often than they get updated, and where you don’t require strong consistency, since updating a denormalized value is slower, more expensive, and is not atomic.
Denormalizing only makes sense when there’s an high ratio of reads to updates. If you’ll be reading the denormalized data frequently, but updating it only rarely.

* Full embedding Document
```js
{
    'id': 10,
    'nom': 'dupont',
    'email':'me@palo-it.com',
    'adresse':
        {
        'pseudo':'10 rue du test',
        'ville':'paris',
        'pays':'France',
        'code postal':75009
        }    
}
```
* Partialy Duplicated document
```js
{
    'id': 10,
    'nom': 'dupont',
    'email':'me@palo-it.com',
    'adresse':
        {
        'ville':'paris',
        'pays':'France',
        }    
}        
```
* Duplicated key
```js
Auteur
{
    "id": 10,
    "nom": "dupont",
    "livres": [
        101,503,339,342
    ]            
}
Livre
{
    "id": 342,
    "titre": "NoSQL schema",
    "genre": "informatique",            
    "tags":["informatique","bigdata","nosql"]
    "auteurs": [
        10,234
    ]            
}
```



# Performance Optimization
---

## Performance Results & Observations
Below the optimised version of the query that uses .lean() .select() and Schema.index({}) was about 10x faster than the default query, Which is a huge win!
.lean() seems to have the highest impact on performance followed by .select()

* **With 1k users**
    * default_query: 135.646ms
    * query_with_index: 168.763ms
    * query_with_select: 27.781ms
    * query_with_select_index: 55.686ms
    * lean_query: 7.191ms
    * lean_with_index: 7.341ms
    * lean_with_select: 4.226ms
    * lean_select_index: 7.881ms

* **With 10k users in the database**
    * default_query: 323.278ms
    * query_with_index: 355.693ms
    * query_with_select: 212.403ms
    * query_with_select_index: 183.736ms
    * lean_query: 92.935ms
    * lean_with_index: 92.755ms
    * lean_with_select: 36.175ms
    * lean_select_index: 38.456ms

* **With 100K Users in the database**
    * default_query: 2425.857ms
    * query_with_index: 2371.716ms
    * query_with_select: 1580.393ms
    * query_with_select_index: 1583.015ms
    * lean_query: 858.839ms
    * lean_with_index: 944.712ms
    * lean_with_select: 383.548ms
    * lean_select_index: 458.000ms

## LEAN Function
Mongoose allows you to add .lean() at the end of your queries which dramatically improves your query’s performance by returning plain JSON objects instead of Mongoose Documents.

* However this comes at a cost, This means that lean docs don't have:
    * Change tracking
    * Casting and validation
    * Getters and setters
    * Virtuals (including “id”)
    * save() function

* **So its usually optimal for GET endpoints and .find() operations that don’t use .save() or virtuals.**

## SELECT function
Use .select() to select specific properties to return
To prevent the database from doing extra work to return these fields increasing the size of the returned documents you can use mongoose .select() to include/exclude the fields you want your query to return specifically as follows:
```js
Model.find({type: "Animal"}).select({name: 1})
```

## INDEXING operation
MongoDB allows you to create indexes on other properties in your Schema other than the default “_id” index. That way your documents can be indexed by your defined properties in the database for faster access.

```js
// The “1” or “-1” indicates the sort order of the properties. 
ModelSchema.index({type: 1, status: 1})
// Query
Model.find({type: “Animal”, status: “extinct"})
```

## Minimise DB requests 
* avoid .populate() if possible

* Try to define your schemas in a way such that you don’t have to rely much on .populate() and bidirectional relations between models. Because that’s where NoSQL databases aren’t very ideal. Every property you add in your model will be returned from your queries, so if you have an array or a nested object in some of these fields, your document will easily get huge slowing down the performance of your query.

* If your document includes an array of references to other Models, and you use .populate() to join data between collections, using .populate() will require running extra queries to fetch the actual documents inside that array for you, so its similar to running an extra query for each id for each document. Its better to favour using .aggregate() instead of .populate() if your really have to.

# Annexes
---

## Tools
---

* Check MongoDB Schema with **Schema Explorer** and generate coverage documentation: [https://studio3t.com/knowledge-base/articles/schema-explorer/](https://studio3t.com/knowledge-base/articles/schema-explorer/)


## Sources
---
* [https://www.mongodb.com/blog/post/6-rules-of-thumb-for-mongodb-schema-design-part-3](https://www.mongodb.com/blog/post/6-rules-of-thumb-for-mongodb-schema-design-part-3)

* [https://paloit.developpez.com/tutoriels/nosql/modelisation-schema-base-donnees-orientee-document/](https://paloit.developpez.com/tutoriels/nosql/modelisation-schema-base-donnees-orientee-document/)
* [https://itnext.io/performance-tips-for-mongodb-mongoose-190732a5d382](https://itnext.io/performance-tips-for-mongodb-mongoose-190732a5d382)