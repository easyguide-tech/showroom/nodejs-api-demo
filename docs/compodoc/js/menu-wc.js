'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">Nodejs-demo-NestJs-and-BDD documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="changelog.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>CHANGELOG
                            </a>
                        </li>
                        <li class="link">
                            <a href="contributing.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>CONTRIBUTING
                            </a>
                        </li>
                        <li class="link">
                            <a href="license.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>LICENSE
                            </a>
                        </li>
                        <li class="link">
                            <a href="todo.html"  data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>TODO
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                        <!-- NPM DOC -->
                        <li class="link">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#npm-links-miscellaneous-variables"' : 'data-target="#xs-npm-links-miscellaneous-variables"' }>
                                <span class="icon ion-ios-paper"></span>
                                <span>NPM doc</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="npm-links-miscellaneous-variables"' :
                                        'id="xs-npm-links-miscellaneous-variables"' }>
                                <li class="link">
                                    <a href="../npm-audit/report/index.html" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>Npm report
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="../npm-audit/guideline/index.pdf" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>Npm guideline
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- DATABASE DOC -->
                        <li class="link">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#db-links-miscellaneous-variables"' : 'data-target="#xs-db-links-miscellaneous-variables"' }>
                                <span class="icon ion-ios-paper"></span>
                                <span>DATABASE doc</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="db-links-miscellaneous-variables"' :
                                        'id="xs-db-links-miscellaneous-variables"' }>
                                <li class="link">
                                    <a href="../database-checkpoints/index.pdf" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>Database Checkpoints
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- API DOC -->
                        <li class="link">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#apidoc-links-miscellaneous-variables"' : 'data-target="#xs-apidoc-links-miscellaneous-variables"' }>
                                <span class="icon ion-ios-paper"></span>
                                <span>API doc</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="apidoc-links-miscellaneous-variables"' :
                                        'id="xs-apidoc-links-miscellaneous-variables"' }>
                                <li class="link">
                                    <a href="../../api" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>Open Api Swagger
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="../asyncapi-kafka/view" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>Async Api Kafka
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- TEST DOC -->
                        <li class="link">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#testdoc-links-miscellaneous-variables"' : 'data-target="#xs-testdoc-links-miscellaneous-variables"' }>
                                <span class="icon ion-ios-paper"></span>
                                <span>TEST doc</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="testdoc-links-miscellaneous-variables"' :
                                        'id="xs-testdoc-links-miscellaneous-variables"' }>
                                <li class="link">
                                    <a href="../../coverage/lcov-report" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>E2E and unit test report
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="../bdd-cucumber/report/index.html" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>Bdd test report
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="../bdd-cucumber/guideline/index.pdf" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>Bdd guideline
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- CI/CD DOC -->
                        <li class="link">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#cicddoc-links-miscellaneous-variables"' : 'data-target="#xs-cicddoc-links-miscellaneous-variables"' }>
                                <span class="icon ion-ios-paper"></span>
                                <span>CI/CD doc</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="cicddoc-links-miscellaneous-variables"' :
                                        'id="xs-cicddoc-links-miscellaneous-variables"' }>
                                <li class="link">
                                    <a href="../cicd/index.pdf" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>AWS CICD workflow
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="../cicd/AWS-cost-reduction-checklist.pdf" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>AWS Cost reduction checklist
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <!-- FRONTEND DOC -->
                        <li class="link">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#frontenddoc-links-miscellaneous-variables"' : 'data-target="#xs-frontenddoc-links-miscellaneous-variables"' }>
                                <span class="icon ion-ios-paper"></span>
                                <span>FRONTEND doc</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="frontenddoc-links-miscellaneous-variables"' :
                                        'id="xs-frontenddoc-links-miscellaneous-variables"' }>
                                <li class="link">
                                    <a href="../guides/angular-guideline.pdf" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>Angular guideline
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="../guides/data-vizualisation-guideline.pdf" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>D3.js guideline
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="../guides/ngrx-guideline.pdf" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>Ngrx guideline
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="../guides/scrum-guideline.pdf" target="_blank" data-type="chapter-link">
                                        <span class="icon ion-ios-paper"></span>Scrum guideline
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <hr/>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-37b3053db15b07d61a1f9416b5e7eee9a7722ba7545cc1f6e157db048e925fc42c04fc796c1901d1936558e62cfb8080209d8ddb3366443c106ccabea7ef35f3"' : 'data-target="#xs-controllers-links-module-AppModule-37b3053db15b07d61a1f9416b5e7eee9a7722ba7545cc1f6e157db048e925fc42c04fc796c1901d1936558e62cfb8080209d8ddb3366443c106ccabea7ef35f3"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-37b3053db15b07d61a1f9416b5e7eee9a7722ba7545cc1f6e157db048e925fc42c04fc796c1901d1936558e62cfb8080209d8ddb3366443c106ccabea7ef35f3"' :
                                            'id="xs-controllers-links-module-AppModule-37b3053db15b07d61a1f9416b5e7eee9a7722ba7545cc1f6e157db048e925fc42c04fc796c1901d1936558e62cfb8080209d8ddb3366443c106ccabea7ef35f3"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-37b3053db15b07d61a1f9416b5e7eee9a7722ba7545cc1f6e157db048e925fc42c04fc796c1901d1936558e62cfb8080209d8ddb3366443c106ccabea7ef35f3"' : 'data-target="#xs-injectables-links-module-AppModule-37b3053db15b07d61a1f9416b5e7eee9a7722ba7545cc1f6e157db048e925fc42c04fc796c1901d1936558e62cfb8080209d8ddb3366443c106ccabea7ef35f3"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-37b3053db15b07d61a1f9416b5e7eee9a7722ba7545cc1f6e157db048e925fc42c04fc796c1901d1936558e62cfb8080209d8ddb3366443c106ccabea7ef35f3"' :
                                        'id="xs-injectables-links-module-AppModule-37b3053db15b07d61a1f9416b5e7eee9a7722ba7545cc1f6e157db048e925fc42c04fc796c1901d1936558e62cfb8080209d8ddb3366443c106ccabea7ef35f3"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/FakeThirdAppModule.html" data-type="entity-link" >FakeThirdAppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-FakeThirdAppModule-11e307453305f6d8633713cd3d5c46c0c817c6fd15405a523e571858cc9d1db56cb8c1782af7456554bc8984a863f4cb0f71df72ca6cf6ae918f06f0176a51ff"' : 'data-target="#xs-controllers-links-module-FakeThirdAppModule-11e307453305f6d8633713cd3d5c46c0c817c6fd15405a523e571858cc9d1db56cb8c1782af7456554bc8984a863f4cb0f71df72ca6cf6ae918f06f0176a51ff"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-FakeThirdAppModule-11e307453305f6d8633713cd3d5c46c0c817c6fd15405a523e571858cc9d1db56cb8c1782af7456554bc8984a863f4cb0f71df72ca6cf6ae918f06f0176a51ff"' :
                                            'id="xs-controllers-links-module-FakeThirdAppModule-11e307453305f6d8633713cd3d5c46c0c817c6fd15405a523e571858cc9d1db56cb8c1782af7456554bc8984a863f4cb0f71df72ca6cf6ae918f06f0176a51ff"' }>
                                            <li class="link">
                                                <a href="controllers/FakeBackApiController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FakeBackApiController</a>
                                            </li>
                                            <li class="link">
                                                <a href="controllers/FakeFrontAppController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FakeFrontAppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-FakeThirdAppModule-11e307453305f6d8633713cd3d5c46c0c817c6fd15405a523e571858cc9d1db56cb8c1782af7456554bc8984a863f4cb0f71df72ca6cf6ae918f06f0176a51ff"' : 'data-target="#xs-injectables-links-module-FakeThirdAppModule-11e307453305f6d8633713cd3d5c46c0c817c6fd15405a523e571858cc9d1db56cb8c1782af7456554bc8984a863f4cb0f71df72ca6cf6ae918f06f0176a51ff"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-FakeThirdAppModule-11e307453305f6d8633713cd3d5c46c0c817c6fd15405a523e571858cc9d1db56cb8c1782af7456554bc8984a863f4cb0f71df72ca6cf6ae918f06f0176a51ff"' :
                                        'id="xs-injectables-links-module-FakeThirdAppModule-11e307453305f6d8633713cd3d5c46c0c817c6fd15405a523e571858cc9d1db56cb8c1782af7456554bc8984a863f4cb0f71df72ca6cf6ae918f06f0176a51ff"' }>
                                        <li class="link">
                                            <a href="injectables/FakeBackApiService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FakeBackApiService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link" >UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UsersModule-2c3b677f5a2a2b8a1f4c0e4b8a39cc4f25fb6c546c3e810002f62fddb6ef9d0c0add7fd616ee85d7befb267b76b817c98639e04d3afc88a2b4cfb8cf946b7fa6"' : 'data-target="#xs-controllers-links-module-UsersModule-2c3b677f5a2a2b8a1f4c0e4b8a39cc4f25fb6c546c3e810002f62fddb6ef9d0c0add7fd616ee85d7befb267b76b817c98639e04d3afc88a2b4cfb8cf946b7fa6"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UsersModule-2c3b677f5a2a2b8a1f4c0e4b8a39cc4f25fb6c546c3e810002f62fddb6ef9d0c0add7fd616ee85d7befb267b76b817c98639e04d3afc88a2b4cfb8cf946b7fa6"' :
                                            'id="xs-controllers-links-module-UsersModule-2c3b677f5a2a2b8a1f4c0e4b8a39cc4f25fb6c546c3e810002f62fddb6ef9d0c0add7fd616ee85d7befb267b76b817c98639e04d3afc88a2b4cfb8cf946b7fa6"' }>
                                            <li class="link">
                                                <a href="controllers/UsersController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UsersModule-2c3b677f5a2a2b8a1f4c0e4b8a39cc4f25fb6c546c3e810002f62fddb6ef9d0c0add7fd616ee85d7befb267b76b817c98639e04d3afc88a2b4cfb8cf946b7fa6"' : 'data-target="#xs-injectables-links-module-UsersModule-2c3b677f5a2a2b8a1f4c0e4b8a39cc4f25fb6c546c3e810002f62fddb6ef9d0c0add7fd616ee85d7befb267b76b817c98639e04d3afc88a2b4cfb8cf946b7fa6"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UsersModule-2c3b677f5a2a2b8a1f4c0e4b8a39cc4f25fb6c546c3e810002f62fddb6ef9d0c0add7fd616ee85d7befb267b76b817c98639e04d3afc88a2b4cfb8cf946b7fa6"' :
                                        'id="xs-injectables-links-module-UsersModule-2c3b677f5a2a2b8a1f4c0e4b8a39cc4f25fb6c546c3e810002f62fddb6ef9d0c0add7fd616ee85d7befb267b76b817c98639e04d3afc88a2b4cfb8cf946b7fa6"' }>
                                        <li class="link">
                                            <a href="injectables/UsersService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link" >AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/FakeBackApiController.html" data-type="entity-link" >FakeBackApiController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/FakeFrontAppController.html" data-type="entity-link" >FakeFrontAppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/UsersController.html" data-type="entity-link" >UsersController</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/CreateUserDto.html" data-type="entity-link" >CreateUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/LoggerConfig.html" data-type="entity-link" >LoggerConfig</a>
                            </li>
                            <li class="link">
                                <a href="classes/PaginationQueryDto.html" data-type="entity-link" >PaginationQueryDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/PaginationQueryDto-1.html" data-type="entity-link" >PaginationQueryDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UnitEntity.html" data-type="entity-link" >UnitEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/UnitsDto.html" data-type="entity-link" >UnitsDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateUserDto.html" data-type="entity-link" >UpdateUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserModel.html" data-type="entity-link" >UserModel</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link" >AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/FakeBackApiService.html" data-type="entity-link" >FakeBackApiService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ParseIntPipe.html" data-type="entity-link" >ParseIntPipe</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsersService.html" data-type="entity-link" >UsersService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/UserInterface.html" data-type="entity-link" >UserInterface</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise-inverted.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});