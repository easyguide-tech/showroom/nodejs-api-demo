Nodejs NPM Cybersecurity guideline
===
Guideline for prevention of the NPM packages 
---


<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Overview](#overview)
- [Setup](#setup)
- [Usage](#usage)
  - [Basic commands](#basic-commands)
  - [NPM audit security report](#npm-audit-security-report)
  - [If you’re developing locally](#if-youre-developing-locally)
  - [If you’re an organization](#if-youre-an-organization)
  - [stay on top of known vulnerabilities](#stay-on-top-of-known-vulnerabilities)
- [Troubleshoots](#troubleshoots)
  - [How to remove quick the node_modules folder ?](#how-to-remove-quick-the-node_modules-folder)
  - [Npm Install is Failing (Could not resolve dependency)](#npm-install-is-failing-could-not-resolve-dependency)
- [Annexe](#annexe)
  - [Source](#source)

<!-- /code_chunk_output -->





# Overview
---
How to protect yourself from malicious packages ?
As a consumer of npm packages, you can’t truly avoid this risk (note this is true for other package managers as well). The only way to mitigate the risk is by controlling which packages you install.

# Setup
---

To publish and install packages to and from the public npm registry or a private npm registry, you must install Node.js and the npm command line interface using either a Node version manager or a Node installer. 

We strongly recommend using a Node version manager* like **Nvm** to install Node.js and npm. We do not recommend using a Node installer, since the Node installation process installs npm in a directory with local permissions and can cause permissions errors when you run npm packages globally.

- **Nodejs**: https://nodejs.org
Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.

- **Npm**: https://www.npmjs.com/
npm is the world's largest software registry.
The **CLI** runs from a terminal, and is how most developers interact with npm.
The **registry** is a large public database of JavaScript software and the meta-information surrounding it.

- **Nvm**: https://github.com/nvm-sh/nvm
Node version manager to install Node.js and npm
Node version managers allow you to install and switch between multiple versions of Node.js and npm on your system so you can test your applications on multiple versions of npm to ensure they work for users on different versions.


# Usage
---


## Basic commands
---

- **Get the list of packages**
local: `npm ls --depth 0`: lists all packages at the top level
global: `npm ls -g`

- **Get the list of outdated packages**
lists report of package versions compared to versions specified in package.json file
- `npm outdated`: (at 0 depth)
**angular**
![./images/npm-outdated.jpg](./images/npm-outdated.jpg)
**express.js**
![./images/npm-outdated.jpg](./images/npm-outdated2.jpg)
**nest.js**
![./images/npm-outdated.jpg](./images/npm-outdated3.jpg)
  - To get all the outdated packages at an optimal level:
  `npm outdated --depth=10` 
  - Add `npm outdated -g --depth=10` for gobally packages
  ![./images/npm-outdated-globaly.jpg](./images/npm-outdated-globaly.jpg)


- **Update packages**
local: `npm update`
global: `npm update -g`
**angular**
![./images/npm-update.jpg](./images/npm-update.jpg)
**express.js**
![./images/npm-update2.jpg](./images/npm-update2.jpg)
**nest.js**
![./images/npm-update3.jpg](./images/npm-update3.jpg)



## NPM audit security report
---
See more details here: 
https://docs.npmjs.com/auditing-package-dependencies-for-security-vulnerabilities


- **Checks for vulnerabilities**
`npm audit` checks for security vulnerabilities or out-of-date versions

**angular**
![./images/npm-audit-fix.jpg](./images/npm-audit-fix.jpg)
**express.js**
![./images/npm-audit-fix2.jpg](./images/npm-audit-fix2.jpg)
**nest.js**
![./images/npm-audit-fix3.jpg](./images/npm-audit-fix3.jpg)


- `npm audit fix` to fix them, or `npm audit` for details

**angular**
![./images/npm-audit.jpg](./images/npm-audit.jpg)
![./images/npm-audit-result.jpg](./images/npm-audit-result.jpg)
Link of the High vulnerability: https://github.com/advisories/GHSA-ww39-953v-wcq6
Another link: https://www.giters.com/angular/angular-cli/issues/22206


**Solution 1: Angular security page**
https://angular.io/guide/security#report-issues 


**Solution 2: Update dependent packages if a fix exists**
If a fix exists but packages that depend on the package with the vulnerability have not been updated to include the fixed version, you may want to open a pull or merge request on the dependent package repository to use the fixed version.
  - 1. To find the package that must be updated, check the "Path" field for the location of the package with the vulnerability, then check for the package that depends on it. For example, if the path to the vulnerability is @package-name > dependent-package > package-with-vulnerability, you will need to update dependent-package.
  - 2. On the npm public registry, find the dependent package and navigate to its repository. For more information on finding packages, see "Searching for and choosing packages to download".
  - 3. In the dependent package repository, open a pull or merge request to update the version of the vulnerable package to a version with a fix.
  - 4. Once the pull or merge request is merged and the package has been updated in the npm public registry, update your copy of the package with npm update.

**express.js**
![./images/npm-audit2.jpg](./images/npm-audit2.jpg)
![./images/npm-audit-result2.jpg](./images/npm-audit-result2.jpg)
**nest.js**
![./images/npm-audit3.jpg](./images/npm-audit3.jpg)
![./images/npm-audit-result3.jpg](./images/npm-audit-result3.jpg)

- **To force or manually update of breaking changes**
use `npm audit fix --force` to install breaking changes; or refer to `npm audit` for steps to fix these manually





**express.js**
![./images/npm-audit-fix-force2.jpg](./images/npm-audit-fix-force2.jpg)
![./images/npm-audit-fix-force-result2.jpg](./images/npm-audit-fix-force-result2.jpg)

## If you’re developing locally
Consider using `npm shrinkwrap` to contain the package versions you use. shrinkwrap freezes the versions you use, not taking new versions unless explicitly asked, and so gives you the opportunity to and manually scrutinize every update. This is cumbersome, but will make you safer.
- https://docs.npmjs.com/cli/v8/commands/npm-shrinkwrap

## If you’re an organization
--- 
Consider using npm On-Site and whitelisting the packages you allow. Again, this will be a bit of a hassle, but will reduce the risk of malicious packages making their way in. To be truly safe here, make sure you set Read through cache to Off.

## stay on top of known vulnerabilities
---
In your npm dependencies, for instance by using **Snyk**.



# Troubleshoots
---

## How to remove quick the node_modules folder ?

Run `npx npkill`


## Npm Install is Failing (Could not resolve dependency)

- You can skip peer dependency conflict checks by running `npm install --legacy-peer-deps` 
or you can set npm config set legacy-peer-deps true and then run `npm install` once again.


- If you want to run the angular code of your friend in your machine then follow this steps.
  - step 1: this can be happen due to version mismatch in your node version and your friends node version and npm version. 
  C://projectFolder> `node --version` then C://projectFolder> `npm -v`

  - step 2: If the version is mismatch then uninstall node in your machine then restart then install the node version of your friend.
  npm install npm@[version]
  `npm install -g npm@6.2.0`
  now run `npm install` in your project folder



# Annexe
---

## Source
---

- https://snyk.io/blog/publishing-malicious-packages/