BDD: Cucumber testing tool Guideline
=================
Explains the behavior of the application in a simple English text using Gherkin language.
----------------------

---
> **Feature**: Serve coffee 
In order to earn money
As a system agent
I should be able to sell and serve coffee at all times

* **Scenario**: Sell last coffee
    * **Given** there are 1 coffees left in the machine
    * **And** customer have deposited 1 dollar
    * **When** customer press the coffee button
    * **Then** I should serve a coffee
---


**TABLE OF CONTENTS**

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Introduction](#introduction)
  - [Demonstration](#demonstration)
    - [Preview](#preview)
    - [Codes syntax](#codes-syntax)
  - [BDD](#bdd)
  - [Cucumber](#cucumber)
  - [Advantages of Cucumber](#advantages-of-cucumber)
  - [Syntaxe Rules](#syntaxe-rules)
    - [For Spec use case](#for-spec-use-case)
    - [Syntax template](#syntax-template)
- [Dependencies](#dependencies)
- [Vs Code Extensions](#vs-code-extensions)
- [Basic Usages](#basic-usages)
  - [Run Bdd Cucumber](#run-bdd-cucumber)
  - [Create or Update the features spec](#create-or-update-the-features-spec)
  - [Create or Update the steps definitions](#create-or-update-the-steps-definitions)
  - [Create or Update the data fakers](#create-or-update-the-data-fakers)
  - [Use a fake of third app module (Backend and/or Frontend)](#use-a-fake-of-third-app-module-backend-andor-frontend)
- [Annexes](#annexes)
  - [Source](#source)

<!-- /code_chunk_output -->
---

# Introduction
---

## Demonstration

### Preview
![Preview BDD](./images/preview-bdd.gif)

### Codes syntax
* **Frontend code:**
```ts
Feature: Frontend Query data
  As a user
  I should be able to query and/or filter a data list
  
  @browser
  Scenario: Query units
    Given I am on the "home" page
    When I click the "list-btn" button
    Then I should be on the "list" page
    And I should see a success message

  @browser
  Scenario: Query units with filters
    Given I am on the "list" page
    And I enter "label 3" as my "keyword"
    When I click the "search-btn" button
    Then I should be on the "list" page
    And I should see a success message
```

* **Backend code:**
```ts
Feature: Backend Query data
  As an api
  I should be able to query and/or filter a data list

  Scenario: Query units
    Given The data list "units"
    When I send GET request to "/units"
    Then I should receive a data list which is not empty

  Scenario: Query units with filters
    Given The data list "units"
    And I provide below sample data
      | keyword | label 3 |
    When I send GET request to "/units"
    Then I should receive a data list which is not empty
```


## BDD
> BDD is a way for software teams to work that **closes the gap between business people and technical people** by:

1. Encouraging collaboration across roles to build shared understanding of the problem to be solved
2. Working in rapid, small iterations to increase feedback and the flow of value
3. Producing system documentation that is automatically checked against the system’s behaviour.

BDD encourages working in rapid iterations, continuously breaking down your user’s problems into small pieces that can flow through your development process as quickly as possible."

## Cucumber
"Cucumber is a testing tool that supports Behavior Driven Development (BDD). It offers a way to write tests that anybody can understand, regardless of their technical knowledge. 
In BDD, users (business analysts, product owners) first write scenarios or acceptance tests that describe the behavior of the system from the customer’s perspective, for review and sign-off by the product owners **before developers write their codes**.

## Advantages of Cucumber
"It is helpful to involve business stakeholders who can't easily read code
Cucumber Testing tool focuses on end-user experience
Style of writing tests allow for easier reuse of code in the tests
Quick and easy set up and execution
Cucumber test tool is an efficient tool for testing."

## Syntaxe Rules
"Every scenario consists of a list of steps, which must start with one of the keywords **Given**, **When**, **Then**, **But** or **And** (or localized one)."

### For Spec use case

---

> **Feature**: Serve coffee 
In order to earn money
As a system agent
I should be able to sell and serve coffee at all times

* **Scenario**: Sell last coffee
    * **Given** there are 1 coffees left in the machine
    * **And** customer have deposited 1 dollar
    * **When** customer press the coffee button
    * **Then** I should serve a coffee



---

**Scenario:** Affichage du prix du produit

* **Given:** je visualise une fiche produit 
    | id produit | prix  |
    |------------|-------|
    | 255        | 300   |
    | 234        | 1000  |
    | 567        | 100   |

* **When:** Quand la promotion appliquée est de
    | promotion |
    |-----------|
    | 0%        | 
    | 20%       |
    | 50%       | 

* **Then:** Alors le prix affiché du produit est de
    | prix affiché  |
    |---------------|
    | 300           | 
    | 800           |
    | 50            |






### Syntax template

the `@tag` below can be used to specify a context like a `@browser` or a `@mobile` or any custom coontext.


---

> **Feature:** Some terse yet descriptive text of what is desired
In order to realize a named business value
As an explicit system actor
I want to gain some beneficial outcome which furthers the goal
  
* **@tag** (optional) group to a specific context
* **Scenario:** Some determinable business situation
    * **Given** some precondition
    **And** some other precondition
    * **When** some action by the actor
    **And** some other action
    **And** yet another action
    * **Then** some testable outcome is achieved
    **And** something else we can check happens too

---

@tag
**Feature:** Description du besoin recquis

**Scenario:** Description de divers scenarios

**Given:** Description du contexte et echantillon de données

|      |       |
|------|-------|
| key  | Value |
| key  | Value |

or

| key   | key   |
|-------|-------|
| Value | Value |
| Value | Value |

**When:** Description de l'évènement

**Then:** Description de l'action 

|      |       |
|------|-------|
| key  | Value |
| key  | Value |

or

| key   | key   |
|-------|-------|
| Value | Value |
| Value | Value |

* **tips:** use `random` value to get a random generated value (predefined by faker tool in step definitions files from `test/acceptance/step-definitions`)


# Dependencies
---

* **Cucumber JS:** 
Define the feature files with all your stakeholders using Behavior-Driven Development (BDD).
[https://cucumber.io/docs/installation/javascript/]

Translation in french (Cucumber example): [https://cucumber.io/docs/gherkin/languages/]

* **Cucumber-html-reporter:** 
Generates Cucumber HTML reports in three different themes
[https://www.npmjs.com/package/cucumber-html-reporter]

* **Puppeteer:**
A high-level API to control headless Chrome over the DevTools Protocol
[https://www.npmjs.com/package/puppeteer]

**Note:** puppeteer-core
Since version 1.7.0 we publish the puppeteer-core package, a version of Puppeteer that doesn't download any browser by default.

* **Axios:**
Promise based HTTP client for the browser and node.js
[https://www.npmjs.com/package/axios]

* **Store:**
A simple, lightweight JavaScript API for handling localStorage on nodejs
[https://www.npmjs.com/package/store]

* **Class-validator:**
Allows use of decorator and non-decorator based validation. Internally uses validator.js to perform validation. Class-validator works on both browser and node.js platforms
[https://www.npmjs.com/package/class-validator]

* **Env-cmd:**
A simple node program for executing commands using an environment by loafding an env file.
[https://www.npmjs.com/package/env-cmd]

* **Faker:**
Generate massive amounts of fake data in the browser and node.js
[https://www.npmjs.com/package/faker]

* **Hbs(Handlebar):**
Express.js view engine for handlebars.js
[https://www.npmjs.com/package/hbs]

* **Uuid:**
For the creation of RFC4122 UUIDs unique id
[https://www.npmjs.com/package/uuid]

* **Open:**
Open stuff like URLs, files, executables. Cross-platform.
This is meant to be used in command-line tools and scripts, not in the browser.
[https://www.npmjs.com/package/open]

* **Copyfiles:**
Copy files easily
[https://www.npmjs.com/package/copyfiles]

* **Rimraf:**
A deep deletion module for node (like `rm -rf`)
[https://www.npmjs.com/package/rimraf]


# Vs Code Extensions
---

* **Cucumber (Gherkin) Full Support:**
Id: alexkrechik.cucumberautocomplete
Description: VSCode Cucumber (Gherkin) Full Language Support + Formatting + Autocompletion of feture steps 
Version: 2.15.1
Publisher: Alexander Krechik
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=alexkrechik.cucumberautocomplete

* **Snippet:**
Id: devonray.snippet
Description: Extension for creating code and managing user defined code snippets
Version: 1.0.1
Publisher: Devon Ray
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=devonray.snippet



# Basic Usages
---

## Run Bdd Cucumber 
1. First, run the app command
```s
$ npm run start:dev
```

2. Run cucumber command
```s
$ npm run bdd:test
```

- **Tips:** If bdd report have an issue and is not generated, then run manualy command below
```s
$ npm run bdd:report
```

- **Tips:** you can get after an issue, some screenshots of the current errors reported by Cucumber at 
`test/acceptance/screenshots`

- **Tips:** you can customize the cucumber options command by editing `./cucumber.js` file
```js
let common = [
  'test/acceptance/features/**/*.feature', // Specify our feature files
  '--require-module ts-node/register', // Load TypeScript module
  '--require test/acceptance/step-definitions/**/*.ts', // Load step definitions
  '--require test/acceptance/support/**/*.ts', // Load hooks
  '-b',
  '--format progress-bar', // Load custom formatter
  '--format node_modules/cucumber-pretty', // Load custom formatter
  // '--parallel 3', // Allows parallel execution across multiple threads
  '-f json:./test/acceptance/report/cucumber.json', // Save report json
].join(' ');

module.exports = {
  default: common
};
```

## Create or Update the features spec
Go to `test/acceptance/features` and edit the files or add new onesfor your specific business logics

- **Tips:** In `.vscode/snippets.json` you can have autocompletion for the most frequent operations when adding a ne feature or a new scenario or a new steps (Given, When,Then)

- **Tips:** you can specify tags like `@browser` to indicate that this feature is under a frintend browser context.
see specifications in `test/acceptance/support`

## Create or Update the steps definitions
Go to `test/acceptance/step-definitions` and edit the files or add new ones for your specific business logics

- **Tips:** In `test/acceptance/step-definitions/common` you already have some standards and example of steps definitions for the most frequent operations when adding a ne feature or a new scenario or a new steps (Given, When,Then)

- **Tips:** In `test/acceptance/support` you already have the global variables and operations shared by all features

## Create or Update the data fakers
Go to `test/acceptance/data` and edit the files or add new ones for your specific data samples

## Use a fake of third app module (Backend and/or Frontend)
If no codes is yet implemented you can still use our faker third-app module for **backend API Rest services **(Nestjs module with a store service for data persistence) or **frontend mockup services** (server side rendering)







# Annexes
---

## Source



* Cucumber
[https://cucumber.io/]

[https://cucumber.io/docs/bdd/]

* Cucumber JS
[https://cucumber.io/docs/installation/javascript/]

* Translation in french (Cucumber example)
[https://cucumber.io/docs/gherkin/languages/]

* Cucumber and Jira
[https://cucumber.io/tools/cucumber-for-jira/]

* Definitions
[https://www.guru99.com/introduction-to-cucumber.html]

* Gherkin
[https://docs.behat.org/en/v2.5/guides/1.gherkin.html]