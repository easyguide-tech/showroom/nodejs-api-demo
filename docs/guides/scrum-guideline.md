SCRUM guideline
=====
Using agile methodology SCRUM
---

* **Scrum roadmap**
![Scrum roadmap](./images/scrum-roadmap.png)


* **Scrum team setup and meetings**
![Scrum meetings](./images/scrum-meetings.png)


* **SCRUM Plan:**
---
1. **Themes / Goals / Products** (trimesters, quarters)
    * **Estimation by complexity:** 4 trimesters 2-4-8  themes
    * **Example:**
      * Project, specs and planning
      * Visual identity
      * Internationalization
      * Rapid prototyping (Study & Poc, R&D)
      * Product Development (App)
      * Business logistic (Delivery)
      * Marketing plan
      * Pilotage and maintenance
  
2. **Epics / Missions / Projects / Milestones / Releases** (months)
a collection of missions that have to be completed to achieve a certain goal or produce a deliverable.
    * **Estimation by complexity:** 3 months 1-3-6 epics
    * **Example:**
      * Product: Frontend
      * Product: Backend
      * Product: Database
      * Product: Dev Secu Ops
  
3. **Features / Services / Requirements / Sprints**(weeks)  
    * **Estimation by complexity:** 4 weeks 2-4-8 features
    * **Example:**
      * Frontend: Authentication (and authorization)
      * Frontend: Theme, style and skin
      * Frontend: Internationalization
      * Frontend: Message notification
      * Frontend: Logger
      * Frontend: Error handler
      * Frontend: Resource (Query and CRUD operations)
        * User
        * Product
        * Order
        * Cart
        * Payment
        * Delivery
      * Frontend: Community portal

4. **Stories / Scenarios** (days)
    * **Estimation by complexity:** 5 days 2-5-10 stories
    * **Example:**
      * Auth: Login / Signin
      * Auth: Logout / Signout
      * Auth: Register / Signup
      * Auth: Profile / Account
      * Auth: Forgot / Reset password
      * Auth: Delete / Remove account

5. **Tasks** (hours)
    * **Estimation by complexity:** 8 hours 4-8-16 tasks
    * **Example:**
      * Login: JS code logic process
      * Login: Html and CSS code template
      * Login: API remote request with JWT strategy

6. **Scrum Tools**
    * Scrum roadmap
    * Labels: Eisenhower's Urgent/Important Principle
    * Kanban boards: Product backlog and Sprint dev backlog
    * Gitlab/Github mapping 

---

**TABLE OF CONTENTS**
---
<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Themes / Goals / Products](#themes-goals-products)
- [Epics / Missions / Projects / Milestones / Releases](#epics-missions-projects-milestones-releases)
- [Features / Services / Requirements / Sprints](#features-services-requirements-sprints)
- [Stories / Scenarios](#stories-scenarios)
- [Tasks](#tasks)
- [Scrum Tools](#scrum-tools)
  - [Scrum roadmap](#scrum-roadmap)
    - [Scrum roadmap](#scrum-roadmap-1)
    - [Scrum team setup and meetings](#scrum-team-setup-and-meetings)
    - [Product backlog](#product-backlog)
    - [Milestone](#milestone)
    - [Releases](#releases)
    - [Sprint backlog](#sprint-backlog)
    - [Sprint Planning (2-4h)](#sprint-planning-2-4h)
    - [Daily standup (15min)](#daily-standup-15min)
    - [Backlog grooming (0.5-2h)](#backlog-grooming-05-2h)
    - [Sprint review (2-4h)](#sprint-review-2-4h)
    - [Sprint retrospective (1h)](#sprint-retrospective-1h)
  - [Labels](#labels)
  - [Kanban boards: Product backlog and Sprint dev backlog](#kanban-boards-product-backlog-and-sprint-dev-backlog)
    - [Product Backlog (stories by features/epics)](#product-backlog-stories-by-featuresepics)
    - [Sprint dev Backlog (tasks by stories)](#sprint-dev-backlog-tasks-by-stories)
  - [Gitlab/Github mapping](#gitlabgithub-mapping)

<!-- /code_chunk_output -->

---


# Themes / Goals / Products
---
* **Estimation by complexity:** 4 trimesters 2-4-8  themes
* **Example:**
  * Project, specs and planning
  * Visual identity
  * Internationalization
  * Rapid prototyping (Study & Poc, R&D)
  * Product Development (App)
  * Business logistic (Delivery)
  * Marketing plan
  * Pilotage and maintenance

# Epics / Missions / Projects / Milestones / Releases
---
a collection of missions that have to be completed to achieve a certain goal or produce a deliverable.
* **Estimation by complexity:** 3 months 1-3-6 epics
* **Example:**
  * Product: Frontend
  * Product: Backend
  * Product: Database
  * Product: Dev Secu Ops

# Features / Services / Requirements / Sprints
---
* **Estimation by complexity:** 4 weeks 2-4-8 features
* **Example:**
  * Frontend: Authentication (and authorization)
  * Frontend: Theme, style and skin
  * Frontend: Internationalization
  * Frontend: Message notification
  * Frontend: Logger
  * Frontend: Error handler
  * Frontend: Resource (Query and CRUD operations)
    * User
    * Product
    * Order
    * Cart
    * Payment
    * Delivery
  * Frontend: Community portal

# Stories / Scenarios
---
* **Estimation by complexity:** 5 days 2-5-10 stories
* **Example:**
  * Auth: Login / Signin
  * Auth: Logout / Signout
  * Auth: Register / Signup
  * Auth: Profile / Account
  * Auth: Forgot / Reset password
  * Auth: Delete / Remove account

# Tasks
---
* **Estimation by complexity:** 8 hours 4-8-16 tasks
* **Example:**
  * Login: JS code logic process
  * Login: Html and CSS code template
  * Login: API remote request with JWT strategy


# Scrum Tools
---


## Scrum roadmap
---

### Scrum roadmap
![Scrum roadmap](./images/scrum-roadmap.png)


### Scrum team setup and meetings
![Scrum meetings](./images/scrum-meetings.png)


### Product backlog

Define the MVP (Minimal valuable product)

1. Epics / Projects / Sprints / Milestones  (can be delivered in months)
a collection of missions that have to be completed to achieve a certain goal or produce a deliverable.

2. Features / Requirements (can be delivered in weeks)
Services

* **Kanban board product backlog** with labels by priorites (see einsenhower labels chapter)
**Steps:** 
1.  priority 1, 2, 3, 4
2. draft, In Analysis
3. ready
![kanban board product backlog](./images/kanban-board-product-backlog.jpg)

### Milestone 
Milestone is typically associated with reaching a certain goal, before proceeding forward or stopping altogether. Checkpoint signifies points at which the team stops and checks progress against expectations, and possibly adjust.
Milestone (big granularity) means progress checking at iterative way;

### Releases
The goal of initial release planning is to estimate roughly which features will be delivered by the release deadline (presuming the deadline is fixed), or to choose a rough delivery date for a given set of features (if scope is fixed). We use this information to decide whether or not the project will produce enough ROI to at least pay for itself, and therefore whether or not we should proceed.


				
### Sprint backlog
Sprints (also called 'Iterations') are chunks for for development plan, with iterative activities, it cannot be considered as a Milestone.
The heart of Scrum is a Sprint, a time-box of two weeks or one month during which a potentially releasable product increment is created. A new Sprint starts immediately after the conclusion of the previous Sprint. Sprints consist of the Sprint planning, daily scrums, the development work, the Sprint review, and the Sprint retrospective.

1. Stories/ Issues/ Bugs / User stories /Scenarios/Product backlog items / work item (can be delivered in days)

There is also the question of scenario, which are usually a way a Feature/User story will be executed. They usually map cleanly to a specific acceptance test. For example

Scenario : Withdrawing money
Given I have 2000$ in my bank account
When I withdraw 100$
Then I receive 100$ in cash
And my balance is 1900$

2. Tasks (can be delivered in hours)
The elements of a story. Stepping stones to take the story to ‘Done’. Tasks often follow the SMART acronym: specific, measurable, achievable, relevant, time-boxed (although what the letters stand for seems to be hotly debated).

* **Kanban board Sprint dev Backlog** (tasks by stories)
* **Steps:**
  1. Todo
  2. In Progress
  3. Done
  4. In Review
  5. Deploy
  6. Final Approuval
  7. Release

![kanban board sprint backlog](./images/kanban-board-sprint-backlog.jpg)


### Sprint Planning (2-4h)

Goals of the meetings:
	• Agreed and verified about the PBIs that will be delivered in the sprint
	• Add all the team members capacity, days off
	• Assigned the PBIs/User Stories to team members
	• Planning individual tasks for all team members that respect the capacity for the sprint (Green)

User stories to Tasks : cutting the user story even into smaller tasks
In many cases, one User Story is developed by two or more developers or people who specialize in a certain part of a User Story. In this case, slicing the User Story into a Task will be the fastest and most efficient way to deliver it.

Tasks hours estimations
Estimation techniques :  the Planning poker session
In the Planning Poker session, 
1. First the team will usually have a short discussion about requirements.
2. Then the team will not use time duration for this estimation.
but they will use story points, which can use scales like the Fibonacci sequence, t-shirt sizes (XS, S, M, L, XL, XXL) or even a custom-made scale.

### Daily standup (15min)

Goals of the meetings:
	• What they did yesterday, that can help the team to successfully deliver items from Sprint Planning.
	• What they are going to do today, that will help the team to finish the Sprint.
	• Is there any blocking point or impediment on their way?


### Backlog grooming (0.5-2h)

Goals of the meetings:
	• We could consider prioritization during the refinement but don’t spend long time on this activity.
	• Approve or remove obsolete items (PBIs/User Stories) that are maybe no more needed
	• Refinement for the title and the description
	• Add initial or more acceptance criteria
	• Add new items, if this is necessary from the discussion
	• Estimate the items

The meeting is divided into two parts : 
	1. Refinements 75% 
	2. Estimation 25%


### Sprint review (2-4h)

Goals of the meetings:
	• Explains what items have been “Done” and what has not been “Done”;
	• Discusses what went well during the Sprint, what problems the team ran into, and how those problems were solved
	• Demonstration of the functionality built during the sprint


the team will present or demo the work items that met the Definition of Done (DoD), which means that this item doesn’t require any more work to complete it. It’s ready to be delivered.


### Sprint retrospective (1h)

Goals of the meetings:
	• Discuss openly what went well and what didn’t during the sprint. 
	• So that team can find together better ways to meet the project’s goals. 

During the Sprint Retrospective the team will address next questions:
	• Start doing
	• Stop doing
Continue doing





## Labels
---
Eisenhower's Urgent/Important Principle

![Eisenhower's Urgent/Important Principle](./images/Urgent-Important-Principle.jpg)

Eisenhower's Urgent/Important Principle helps you quickly identify the activities that you should focus on, as well as the ones you should ignore.

When you use this tool to prioritize your time, you can deal with truly urgent issues, at the same time as you work towards important, longer-term goals.

To use the tool, list all of your tasks and activities, and put each into one of the following categories/priorities:

1. Important and urgent.
2. Important but not urgent.
3. Not important but urgent.
4. Not important and not urgent.

Then schedule tasks and activities based on their importance and urgency.

1. **Important and Urgent**
There are two distinct types of urgent and important activities: ones that you could not have foreseen, and others that you've left until the last minute.
You can eliminate last-minute activities by planning ahead and avoiding procrastination
.
However, you can't always predict or avoid some issues and crises. Here, the best approach is to leave some time in your schedule to handle unexpected issues and unplanned important activities. (If a major crisis arises, then you'll need to reschedule other tasks.)
If you have a lot of urgent and important activities, identify which of these you could have foreseen, and think about how you could schedule similar activities ahead of time, so that they don't become urgent.

2. **Important but Not Urgent**
These are the activities that help you achieve your personal and professional goals, and complete important work.
Make sure that you have plenty of time to do these things properly, so that they do not become urgent. Also, remember to leave enough time in your schedule to deal with unforeseen problems. This will maximize your chances of keeping on track, and help you avoid the stress of work becoming more urgent than necessary.

3. **Not Important but Urgent**
Urgent but not important tasks are things that prevent you from achieving your goals. Ask yourself whether you can reschedule or delegate
them.
A common source of such activities is other people. Sometimes it's appropriate to say "no" to people politely, or to encourage them to solve the problem themselves. (Our article "'Yes' to the Person, 'No' to the Task"
will help here.)
Alternatively, try to have time slots when you are available, so that people know they can speak with you then. A good way to do this is to arrange regular meetings with those who interrupt you often, so that you can deal with all their issues at once. You'll then be able to concentrate on your important activities for longer.

4. **Not Important and Not Urgent**
These activities are just a distraction – avoid them if possible.
You can simply ignore or cancel many of them. However, some may be activities that other people want you to do, even though they don't contribute to your own desired outcomes. Again, say "no" politely, if you can, and explain why you cannot do it.
If people see that you are clear about your objectives and boundaries
, they will often avoid asking you to do "not important" activities in the future.

Source: https://www.mindtools.com/pages/article/newHTE_91.htm 


## Kanban boards: Product backlog and Sprint dev backlog
---
### Product Backlog (stories by features/epics)
**Steps:** 
1.  priority 1, 2, 3, 4
2. draft, In Analysis
3. ready

![kanban board product backlog](./images/kanban-board-product-backlog.jpg)


### Sprint dev Backlog (tasks by stories)
**Steps: **
1. Todo
2. In Progress
3. Done
4. In Review
5. Deploy
6. Final Approuval
7. Release

![kanban board sprint backlog](./images/kanban-board-sprint-backlog.jpg)

## Gitlab/Github mapping
---

Mapping Agile artifacts to GitLab features
**Agile artifact** | **GitLab feature**
* User story |	Issues
* Task |	Task lists
* Epic |	Epics
* Points and estimation |	Weights
* Product backlog |	Issue lists and prioritized labels
* Sprint/iteration |	Milestones
* Burndown chart |	Burndown charts
* Agile board |	Issue boards

Source: https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development 
