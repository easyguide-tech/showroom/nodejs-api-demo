Data vizualisation guideline
===
Guideline for the library D3.js with SVG and CSS
---

---
**TABLE OF CONTENTS**

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [About D3.js?](#about-d3js)
- [About SVG](#about-svg)
- [About Date internationalization](#about-date-internationalization)
- [Custom component chart](#custom-component-chart)
- [D3js Graph chart](#d3js-graph-chart)
- [D3.js Linebar](#d3js-linebar)
- [D3.js Map](#d3js-map)
- [D3.js Node](#d3js-node)
- [D3.js Pie](#d3js-pie)
- [D3.js Timeline](#d3js-timeline)

<!-- /code_chunk_output -->
---

# About D3.js?
---

> "D3.js is a data-driven JavaScript library for manipulating DOM elements.

“D3 helps you bring data to life using HTML, SVG, and CSS. D3’s emphasis on web standards gives you the full capabilities of modern browsers without tying yourself to a proprietary framework, combining powerful visualization components and a data-driven approach to DOM manipulation.” - d3js.org

charts are based on information coming from third-party resources which requires dynamic visualization during render time." - Source: [https://blog.risingstack.com/d3-js-tutorial-bar-charts-with-javascript/]


# About SVG
---

> "SVG stands for Scalable Vector Graphics which is technically an XML based markup language. It is commonly used to draw vector graphics, specify lines and shapes or modify existing images. You can find the list of available elements here: 
[https://developer.mozilla.org/en-US/docs/Web/SVG/Element](https://developer.mozilla.org/en-US/docs/Web/SVG/Element)

* **Pros:**
  * Supported in all major browsers;
  * It has DOM interface, requires no third-party lib;
  * Scalable, it can maintain high resolution;
  * Reduced size compared to other image formats.

* **Cons:**
  * It can only display two-dimensional images;
  * Long learning curve;
  * Render may take long with compute-intensive operations.

> Despite its downsides, SVG is a great tool to display icons, logos, illustrations or in this case, charts." - Source: [https://blog.risingstack.com/d3-js-tutorial-bar-charts-with-javascript/]


# About Date internationalization
---
Moment.js and its alternatives

Moment.js is a JavaScript date library for parsing, validating, manipulating, and formatting dates. its mutability and high size make it no more recommended for new application.
Use **INTL**, the JavaScript Internationalization API  as an alternative.
Source: [https://blog.logrocket.com/4-alternatives-to-moment-js-for-internationalizing-dates/](https://blog.logrocket.com/4-alternatives-to-moment-js-for-internationalizing-dates/)

```js
const date = new Date(Date.UTC(2014, 8, 19, 14, 5, 0));
const options = {
   dateStyle: 'short',
   timeStyle: 'full',
   hour12: true,
   day: 'numeric',
   month: 'long',
   year: '2-digit',
   minute: '2-digit',
   second: '2-digit',
};
// Sample output: 19 septembre 14 à 05:00
console.debug(new Intl.DateTimeFormat("fr", options).format(date));
// Sample output: 19. September 14, 05:00
console.debug(new Intl.DateTimeFormat("de-AT", options).format(date));
/* Sample output: [{"type":"day","value":"19"},{"type":"literal","value":" "},{"type":"month","value":"settembre"},{"type":"literal","value":" "},{"type":"year","value":"14"},{"type":"literal","value":", "},{"type":"minute","value":"05"},{"type":"literal","value":":"},{"type":"second","value":"00"}] */
console.debug(new Intl.DateTimeFormat("it", options).formatToParts(date));
```


# Custom component chart
---

1. Install D3.js 

```sh
npm install d3 --save 
```

2. Install some specific dependencies like below :

```sh
npm install --save-dev @types/d3
```

3. Include `import * as d3 from 'd3';`

4. Create the custom component chart (including ts, html and css files)

5. Declare in `app.module.ts` or `[your-name].module.ts`, the custom component and the schemas (troubleshoot to fix issue).

```ts
@NgModule({
  declarations: [TimelineD3ChartComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
```


# D3js Graph chart
---

```ts
import { ElementRef, Injectable } from '@angular/core';
import * as d3 from 'd3';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Component } from '@angular/core';
import eventsMock from '../mock/events.json';
import { GraphD3ChartComponent } from './graph-d3-chart.component';

@Injectable({
  providedIn: 'root',
})
export class GraphD3ChartService {
  private subscriptions: any = [];
  idName: string;
  myComponent!: GraphD3ChartComponent;
  isLoading: boolean = false;
  margin: any;
  width: number;
  height: number;
  innerWidth: number;
  innerHeight: number;
  svg: any;
  svgCanvas: any;
  zoomCanvas: any;
  clipPath: any;
  lineLegend: any;
  codesLegend: any;
  colorsLegend: any;
  xScale: any;
  xAxis: any;
  xAxisGrid: any;
  xAxisGroup: any;
  xAxisGridGroup: any;
  yScale: any;
  yAxis: any;
  yAxisGrid: any;
  yAxisGroup: any;
  yAxisGridGroup: any;
  itemsGroup: any;
  enterSelections: any;
  dataSet: any;
  edgesList: any;
  canvasGroup: any;
  zoom: any;
  nbTicks: number;
  zoomLevel: number;
  zoomPanX: number;
  zoomPanY: number;

  constructor() {
    // Initialize
    this.margin = {
      top: 40,
      bottom: 100,
      left: 100,
      right: 40,
    };
    this.idName = 'chart';
    this.width = 960;
    this.height = 480;
    this.innerWidth = this.width - this.margin.left - this.margin.right;
    this.innerHeight = this.height - this.margin.top - this.margin.bottom;
    this.nbTicks = 7;
    this.zoomLevel = 1;
    this.zoomPanX = 0;
    this.zoomPanY = 0;
    this.codesLegend = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.colorsLegend = d3
      .scaleOrdinal()
      .domain(this.codesLegend)
      .range(['yellow', 'blue', 'green', 'orange', 'purple', 'red']);
  }

  createChart(
    chartElement: any,
    data: any,
    width: any = undefined,
    height: any = undefined
  ): void {
    try {
      this.idName = chartElement.id;
      // CLean first
      d3.select(`#${this.idName} svg`).remove();
      this.zoomLevel = 1;
      this.zoomPanX = 0;
      this.zoomPanY = 0;

      if (!data) {
        throw 'No data provided';
        // return;
      }

      // Define responsive scale limits
      if (width != undefined) {
        // Height min size rule of 200px
        width = width > 600 ? width : 500;
        // width = (width > 3000 ) ? 3000 : width;
        this.innerWidth = width - this.margin.left - this.margin.right - 300; // Added a page margin of 300
        // Height min size rule of 200px
        height = height > 400 ? height : 300;
        // height = (height > 700 ) ? 600 : height;
        this.innerHeight = height - this.margin.top - this.margin.bottom;
      }

      // Create SVG Canvas viewport
      this.svg = d3
        .select(chartElement)
        .append('svg')
        .classed('chart', true)
        .attr('width', this.innerWidth + this.margin.left + this.margin.right)
        .attr('height', this.innerHeight + this.margin.top + this.margin.bottom)
        .append('g');

      // Append the zoom group
      this.zoomCanvas = this.svg
        .append('g')
        .append('rect')
        .attr('class', 'background')
        .attr('fill', '#ffffff')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .attr('pointer-events', 'fill')
        .attr('cursor', 'grab');

      // Render the axis
      this.renderXAxis();
      this.renderYAxis();

      // Add a clipPath: everything out of this area won't be drawn.
      this.clipPath = this.svg
        .append('defs')
        .append('SVG:clipPath')
        .attr('id', 'clip')
        .append('SVG:rect')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

      // Add the canvas group where items take place
      this.svgCanvas = this.svg
        .append('g')
        .classed('svg-canvas', true)
        .attr('clip-path', 'url(#clip)');

      // Update data
      this.mapData(data);

      // Creating a zoom object

      // max zoom is the ratio of the initial domain extent to the minimum
      // unit that you want to zoom to (1 minute == 1000*60 milliseconds)
      /*
      const minTimeMilli = 20000; // do not allow zooming beyond displaying 20 seconds
      const maxTimeMilli = 6.3072e+11; // approx 20 years

      const minStartTime: any = d3.min(this.dataSet, (d: any) => { return d.dateStart as any; });
      const maxEndTime: any = d3.max(this.dataSet, (d: any) => { return d.dateEnd as any; });

      const widthMilli = maxEndTime.getTime() - minStartTime.getTime();

      console.debug("minStartTime", minStartTime.getTime());
      console.debug("minStartTime", maxEndTime.getTime());
      console.debug("widthMilli", widthMilli);

      const minZoomFactor = widthMilli / maxTimeMilli;
      const maxZoomFactor = widthMilli / minTimeMilli; 
      */
      this.zoom = d3.zoom();
      // .scaleExtent([0.001, 1000]) // Limit the extent of the zoom (ex: from 10 years to 5 min)
      // .scaleExtent([minZoomFactor, maxZoomFactor])
      // .translateExtent([[0, 0], [this.innerWidth, this.innerHeight]])
      // .extent([[0, 0], [this.innerWidth, this.innerHeight]])

      // center via style css
      // this.svg.style("transform-origin", "50% 50% 0");

      // Zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.on('zoom', (event: any) => {
            this.zoomLevel = event.transform.k;
            this.zoomPanX = event.transform.x;
            this.zoomPanY = event.transform.y;
            console.debug('this.zoomLevel', this.zoomLevel);
            console.debug('this.zoomPanX', this.zoomPanX);
            console.debug('this.zoomPanY', this.zoomPanY);
            this.zoomActions(event, data);
          })
        );

      // Render items
      this.renderLegend();
      this.renderEdges();
      this.renderBars();
      this.renderSymbols();
      this.renderTexts();

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderXAxis() {
    // Create x-axis and scale x
    try {
      // creating a scale with a given range (the output of the scale)
      // this.xScale = d3.scaleLinear().range([this.innerHeight - this.paddingVert, this.paddingVert]);
      this.xScale = d3
        .scaleBand()
        .range([this.innerWidth, 0])
        .paddingInner(0) // edit the inner padding value in [0,1]
        .paddingOuter(0.5) // edit the outer padding value in [0,1]
        .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right.

      // creating an axis with a given orientation, linked to the scale
      this.xAxis = d3.axisBottom(this.xScale);

      // Add X vertical gridlines
      this.xAxisGrid = this.xAxis.tickSize(-this.innerHeight).tickFormat('');
      this.xAxisGridGroup = this.svg
        .append('g')
        .attr('class', 'x-axis-grid')
        .attr(
          'transform',
          `translate(${this.margin.left}, ${-(
            this.margin.top + this.innerHeight
          )})`
        )
        .call(this.xAxisGrid);

      // Customize axis
      this.xAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.xAxisGroup = this.svg
        .append('g')
        .classed('x-axis', true)
        .attr(
          'transform',
          `translate(${this.margin.left}, ${
            this.margin.top + this.innerHeight
          })`
        )
        .call(this.xAxis); // calling the axis from the selection

      // Title label for the x axis
      this.svg
        .append('text')
        // .attr("transform", "rotate(-90)")
        .attr('x', this.margin.left + this.innerWidth / 2)
        .attr('y', this.margin.top + this.innerHeight + this.margin.bottom - 15)
        .attr('dx', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Domain axis X');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderYAxis() {
    // Create y-axis and scale y
    try {
      // creating a scale with a given range (the output of the scale)
      // this.yScale = d3.scaleLinear().range([this.innerHeight - this.paddingVert, this.paddingVert]);
      this.yScale = d3
        .scaleBand()
        .range([this.innerHeight, 0])
        .paddingInner(0) // edit the inner padding value in [0,1]
        .paddingOuter(0.5) // edit the outer padding value in [0,1]
        .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right.

      // creating an axis with a given orientation, linked to the scale
      this.yAxis = d3.axisLeft(this.yScale);

      // Add Y horizontal gridlines
      this.yAxisGrid = this.yAxis.tickSize(-this.innerWidth).tickFormat('');
      this.yAxisGridGroup = this.svg
        .append('g')
        .attr('class', 'y-axis-grid')
        .attr(
          'transform',
          'translate(' + this.margin.left + ',' + this.margin.top + ')'
        )
        .call(this.yAxisGrid);

      // Customize axis
      this.yAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.yAxisGroup = this.svg
        .append('g')
        .classed('y-axis', true)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .call(this.yAxis); // calling the axis from the selection

      // Title label for the y axis
      this.svg
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 0)
        .attr('x', 0 - this.innerHeight / 2)
        .attr('dy', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Domain axis Y');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  mapData(data: any) {
    try {
      // Add items
      this.itemsGroup = this.svgCanvas
        .append('g')
        .classed('items', true)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

      // Mapping
      // For converting strings to Dates
      const parseTime = d3.timeParse('%Y-%m-%dT%H:%M:%S%Z');
      // const strictIsoParse = d3.utcParse("%Y-%m-%dT%H:%M:%S%Z");
      this.dataSet = data
        .map((d: any) => {
          // const currDate = new Date(d.startDate);
          const dateObjStart = parseTime(d.startDate);
          const dateObjEnd = parseTime(d.endDate);

          // Return the custom data structure for d3js
          return {
            id: d.id,
            code: d.code,
            label: d.label,
            photo: d.photo,
            dateStart: dateObjStart,
            dateEnd: dateObjEnd,
            amount: parseInt(`${d.duration}`, 10),
          };
        })
        .reverse(); // reverse array order to get a first default section in y axis

      this.edgesList = eventsMock.edges.map((d: any) => {
        // Return the custom edge data structure for d3js
        return {
          id: d.id,
          source: this.dataSet.find((item: any) => item.id === d.source),
          target: this.dataSet.find((item: any) => item.id === d.target),
          tag: d.tag,
        };
      });

      // Update our scales and axes

      // this.xScale.domain(d3.extent(this.dataSet, (d: any) => { return d.code;}));
      /*
      this.xScale.domain([
        d3.min(this.dataSet, (d: any) => { return d.dateStart as any; }),
        d3.max(this.dataSet, (d: any) => { return d.dateEnd as any; })
      ]);
      */

      // Fix style format issues with zoom effects
      // this.xScale.tickFormat(d3.timeParse("%Y-%m-%dT%H:%M:%S%Z"));
      /*
      this.xAxisGroup.call(this.xAxis)
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", "1em")
        .attr("transform", "rotate(-10)");
      */

      /*
      this.yScale.domain([
        d3.min(this.dataSet, (d: any)=> { return ((d.amount as any)); }),
        d3.max(this.dataSet, (d: any)=> { return d.amount as any; })
      ])
      */

      this.xScale.domain(this.dataSet.map((d: any) => d.code));

      this.xAxisGroup
        .call(this.xAxis)
        .selectAll('text')
        .attr('x', -(this.xScale.bandwidth() / 2))
        .attr('y', -(this.margin.top + this.innerHeight));

      // Background x axis
      this.xAxisGroup
        .call(this.xAxis)
        .selectAll('.x-axis > .tick')
        .data(this.dataSet)
        .append('rect')
        .attr('x', -this.xScale.bandwidth())
        .attr('y', -this.innerHeight)
        .attr('width', this.xScale.bandwidth())
        .attr('height', this.innerHeight + this.margin.bottom)
        .attr('stroke', '#B8B9BA')
        .attr('fill', 'none')
        /*
        .attr("fill", (d: any) => {
          if (d.code == "fake code") {
            return "#B8B9BA"
          } else { return "#F2F2F2" };
        })
        .attr("opacity", (d: any) => {
          if (d.code == "fake code") {
            return "0.5"
          } else { return "0.6" };
        })
        */
        .attr('pointer-events', 'none'); // disable event mouses for enable zoom tool

      this.yScale.domain(this.dataSet.map((d: any) => d.label));

      this.yAxisGroup
        .call(this.yAxis)
        .selectAll('text')
        .attr('y', -(this.yScale.bandwidth() / 2));

      // Background y axis
      this.yAxisGroup
        .call(this.yAxis)
        .selectAll('.y-axis > .tick')
        .data(this.dataSet)
        .append('rect')
        .attr('x', -this.margin.left)
        .attr('y', -this.yScale.bandwidth())
        .attr('width', this.innerWidth + this.margin.left)
        .attr('height', this.yScale.bandwidth())
        .attr('stroke', '#B8B9BA')
        .attr('fill', 'none')
        /*
        .attr("fill", (d: any) => {
          if (d.label == "fake label") {
            return "#B8B9BA"
          } else { return "#F2F2F2" };
        })
        .attr("opacity", (d: any) => {
          if (d.label == "fake label") {
            return "0.5"
          } else { return "0.6" };
        })
        */
        .attr('pointer-events', 'none'); // disable event mouses for enable zoom tool

      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(this.dataSet); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderLegend() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-legend`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items

      // this.lineLegend = this.svg.selectAll(".lineLegend").data(this.dataSet).enter().append("g")
      this.lineLegend = this.enterSelections
        .append('g')
        .attr('class', 'item-legend item-legend')
        .attr('transform', (d: any, i: any) => {
          return 'translate(10,' + (i * 20 + 10) + ')';
        });

      this.lineLegend
        .append('rect')
        .attr('fill', (d: any, i: any) => {
          return this.colorsLegend(d.amount);
        })
        .attr('stroke', 'white')
        .attr('stroke-width', 1.5)
        .attr('width', 10)
        .attr('height', 10);

      this.lineLegend
        .append('text')
        // .attr("dx", "1em")
        // .style("text-anchor", "middle")
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', 'black')
        .text((d: any) => {
          return d.code;
        })
        .attr('transform', 'translate(15,9)'); //align texts with boxes

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderBars() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-bar`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items

      // this.enterSelections.merge(dataBound as any).attr('transform', (d, i) => `translate(${xScale(d.date as Date)},${yScale(d.amount)})`);
      this.enterSelections
        .append('rect')
        .attr('class', 'item-bar item-rect')
        .attr(
          'x',
          (d: any) => this.xScale(d.code) - this.xScale.bandwidth() / 2
        )
        .attr('y', (d: any) => this.yScale(d.label) - 16)
        .attr('width', this.xScale.bandwidth())
        // .attr('height', this.yScale.bandwidth())
        .attr('height', 20)
        .attr('fill', (d: any) => {
          return this.colorsLegend(d.amount);
        })
        .attr('pointer-events', 'fill')
        .attr('cursor', 'pointer')
        .on('mouseover', (event: any, d: any) => {
          console.debug(d);
          console.debug(event);
          console.debug(d3.pointer(event));
          d3.select(event.target).attr('fill', 'green');
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).attr('fill', 'purple');
        });

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderTexts() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-text`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items
      //For converting Dates to strings
      const formatTime = d3.timeFormat('%Y-%m-%d %H:%M');

      // Generate the labels of the items first, so they are in back
      this.enterSelections
        .append('text')
        .attr('class', 'item-text')
        .text((d: any) => {
          const sampleText = `${d.code} - ${d.label}`;
          return sampleText.substring(0, 20).toUpperCase() + '...';
        })
        .attr('x', (d: any) => {
          return this.xScale(d.code) - this.xScale.bandwidth() / 2 + 15;
        })
        .attr('y', (d: any) => {
          return this.yScale(d.label);
        })
        .attr('font-family', 'sans-serif')
        .attr('font-size', '11px')
        .attr('fill', 'white');
      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderSymbols() {
    try {
      // Clear first all existing items
      d3.selectAll(`#${this.idName} .item-symbol`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Create a shape path

      this.enterSelections
        .append('path')
        .attr('class', 'item-symbol item-symbol-left1')
        .attr(
          'd',
          'M5.99999 6.0013C7.47332 6.0013 8.66666 4.80464 8.66666 3.33464C8.66666 1.8613 7.47332 0.667969 5.99999 0.667969C4.52666 0.667969 3.33332 1.8613 3.33332 3.33464C3.33332 4.80464 4.52666 6.0013 5.99999 6.0013ZM5.99999 7.33463C4.22332 7.33463 0.666656 8.22464 0.666656 10.0013V11.3346H11.3333V10.0013C11.3333 8.22464 7.77666 7.33463 5.99999 7.33463Z'
        )
        .attr('fill', '#333333')
        .attr(
          'transform',
          (d: any) =>
            `translate(${this.xScale(d.code) - this.xScale.bandwidth() / 2},${
              this.yScale(d.label) - this.yScale.bandwidth() / 2
            }) scale(1) `
        );

      this.enterSelections
        .append('circle')
        .attr('class', 'item-symbol item-circle')
        .attr('cx', (d: any) => {
          return this.xScale(d.code) - this.xScale.bandwidth() / 2;
        })
        .attr('cy', (d: any) => {
          return this.yScale(d.label) - this.yScale.bandwidth() / 2;
        })
        .attr('r', (d: any) => {
          return this.xScale.bandwidth() / 15;
        })
        .attr('fill', 'none')
        .attr('stroke', '#FF0000');

      const iconSize = 30;
      const icon = this.enterSelections
        .append('image')
        .attr('class', 'item-symbol item-image')
        .attr('x', (d: any) => {
          return this.xScale(d.code) - iconSize / 2;
        })
        .attr('y', (d: any) => {
          return this.yScale(d.label) - 50;
        })
        .attr('width', iconSize)
        .attr('height', iconSize)
        .attr('href', (d: any) => {
          return d.photo;
        })
        .attr('pointer-events', 'fill')
        // .attr("cursor", "pointer")
        .on('mouseover', (event: any, d: any) => {
          d3.select(event.target).style('cursor', 'pointer');
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px blue)'
          );
        })
        .on('mouseout', (event: any, d: any) => {
          d3.select(event.target).style('filter', null);
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px yellow)'
          );
          this.myComponent.showPopin(d);
          // this.event = null;
        });

      /*
      // dragstarted
      .on("start", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px red)");
        d3.select(event.target)
        .raise() // D3 Raise function display current node at the top
        .classed("active", true);
      })
      // dragged
      .on("drag", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px yellow)");
        d3.select(event.target).raise() // D3 Raise function display current node at the top
        .attr("x", ()=> {
          d.x = event.x; // Todo: Issue bad behavior
          return d.x;
          })    
      })
      // dragended
      .on("end", (event: any, d: any) => {
        d3.select(event.target).classed("active", false);
        d3.select(event.target).style("filter", null);
      });
      */
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderEdges() {
    try {
      console.debug('render edges', this.edgesList);
      //Draw connection
      const edgesLine = this.itemsGroup
        .append('g')
        .classed('item', true)
        .selectAll('.item-edge')
        .data(this.edgesList) // bind data
        .enter() // Add a corresponding number of placeholders for the data
        .append('path') // Generate a polyline on the placeholder (draw with path)
        .attr('class', 'item-edge')
        .attr('d', (d: any) => {
          const source = `M ${this.xScale(d.source.code) - 5} ${
            this.yScale(d.source.label) + 10
          }`;
          // const marginSource = ` H ${this.xScale(d.source.code) + 50 }`;
          const marginTarget = ` V ${this.yScale(d.target.label) - 35}`;
          const target = ` H ${this.xScale(d.target.code) - 20}`;
          return `${source + marginTarget + target}`;
        }) //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
        //.attr('d', (d: any) => { return d && 'M ' + d.source.x + ' ' + d.source.y + ' L ' + d.target.x + ' ' + d.target.y; }) //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
        .attr('id', (d: any, i: any) => {
          return `${this.idName}-edgepath-${i}`;
        }) // Set id, used for connecting text
        .attr('marker-start', 'url(#joinStart)') // Mark the arrow according to the id number marked by the arrow
        .attr('marker-mid', 'url(#joinMid)') // Mark the arrow according to the id number marked by the arrow
        .attr('marker-end', 'url(#joinEnd)') // Mark the arrow according to the id number marked by the arrow
        .style('stroke', 'black') // colour
        .style('fill', 'none') // colour
        .style('stroke-width', 2); // Thickness

      //Connection name
      const edgeLabel = this.itemsGroup
        .append('g')
        .classed('item', true)
        .selectAll(`${this.idName}-item-edge-label`)
        .data(this.edgesList) // bind data
        .enter() // Add a corresponding number of placeholders for the data
        .append('text') // Create a text area for each connection
        .attr('class', `${this.idName}-item-edge-label`)
        //.attr('dx', 80)
        .attr('dy', 15);
      // .style("text-anchor", "middle")
      //.attr("transform", "rotate(-60)");

      edgeLabel
        .append('textPath')
        .style('pointer-events', 'none') // Disable mouse events
        .attr('font-family', 'sans-serif')
        .attr('font-size', '12px')
        .attr('fill', 'black')
        .attr('startOffset', '50%')
        .attr('xlink:href', (d: any, i: any) => {
          return `#${this.idName}-edgepath-${i}`;
        }) // The text is arranged on the line corresponding to the id
        .text((d: any) => {
          return d.tag;
        }); // Set text content

      //Draw the arrow on the line
      const defs = this.itemsGroup.append('defs'); // defs defines reusable elements
      const joinEnd = defs
        .append('marker') // Create arrow
        .attr('id', 'joinEnd')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinEnd')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-5 -5 10 10') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinEnd
        .append('path')
        .attr('d', 'M 0,0 m -5,-5 L 5,0 L -5,5 Z') // d: path description, Bezier curve
        .attr('fill', 'red'); // fill color

      const joinStart = defs
        .append('marker') // Create arrow
        .attr('id', 'joinStart')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinStart')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-6 -6 12 12') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinStart
        .append('path')
        .attr('d', 'M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0') // Circle d: path description, Bezier curve
        .attr('fill', 'orange'); // fill color

      const joinMid = defs
        .append('marker') // Create arrow
        .attr('id', 'joinMid')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinMid')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-6 -6 12 12') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinMid
        .append('path')
        .attr('d', 'M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0') // Circle d: path description, Bezier curve
        .attr('fill', 'black'); // fill color
    } catch (e) {
      this.errorHandler(e);
    }
  }

  zoomActions(event: any, data: any) {
    // function to redraw while zooming
    try {
      // Get X axis new scale
      // this.xScale = event.transform.rescaleX(this.xScale);
      const newX = event.transform.rescaleX(this.xScale);
      this.xAxis.scale(newX);
      this.xAxisGroup.call(this.xAxis); // update the axes

      // Update text position
      /*
      this.xAxisGroup.selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", "1em")
        .attr("transform", "rotate(-10)");
      */

      // Get Y axis new scale
      // this.yScale = event.transform.rescaleY(this.yScale)
      const newY = event.transform.rescaleY(this.yScale);
      this.yAxis.scale(newY);
      this.yAxisGroup.call(this.yAxis); // update the axis

      // Update the items
      // this.enterSelections.attr('transform', event.transform);
      this.enterSelections
        .selectAll('.item-text')
        .attr('x', (d: any) => event.transform.applyX(this.xScale(d.code)) + 20)
        .attr('y', (d: any) => event.transform.applyY(this.yScale(d.label)));

      this.enterSelections
        .selectAll('.item-rect')
        .attr('width', (d: any) => {
          return event.transform.applyX(this.xScale(d.code));
        })
        .attr('x', (d: any) => event.transform.applyX(this.xScale(d.code)))
        .attr('y', (d: any) => event.transform.applyY(this.yScale(d.label)));

      this.enterSelections
        .selectAll('.item-symbol-left1')
        .attr(
          'transform',
          (d: any) =>
            `translate(${event.transform.applyX(this.xScale(d.code))},${
              this.yScale(d.label) - this.yScale.bandwidth() / 2
            }) scale(1)`
        );

      this.itemsGroup.selectAll('.item-edge').attr('d', (d: any) => {
        const source = `M ${event.transform.applyX(
          this.xScale(d.source.code)
        )} ${this.yScale(d.source.label) - 5}`;
        // const marginSource = ` H ${event.transform.applyX(this.xScale(d.source.code) + 50)}`;
        const marginTarget = ` V ${this.yScale(d.target.label) - 5}`;
        const target = ` H ${event.transform.applyX(
          this.xScale(d.target.code)
        )}`;
        return `${source + marginTarget + target}`;
      }); //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve

      this.enterSelections
        .selectAll('.item-circle')
        .attr('cx', (d: any) => event.transform.applyX(this.xScale(d.code)))
        .attr('cy', (d: any) =>
          event.transform.applyY(
            this.yScale(d.label) - this.yScale.bandwidth() / 2
          )
        );
      // .attr('transform', (d: any) => `translate(${event.transform.applyX(this.xScale(d.code))},${this.yScale(d.label) - (this.yScale.bandwidth() / 2)}) scale(1)`);

      this.enterSelections.selectAll('.item-image').attr('x', (d: any) => {
        const pos = this.xScale(d.code) / 2;
        return event.transform.applyX(this.xScale(d.code) + pos);
      });
    } catch (e) {
      this.errorHandler(e);
    }
  }

  rescale(type: string, data: any) {
    try {
      switch (type) {
        case 'second':
          this.zoomLevel = 124308;
          break;
        case 'minute':
          this.zoomLevel = 2155;
          break;
        case 'hour':
          this.zoomLevel = 46;
          break;
        case 'day':
          this.zoomLevel = 1.65;
          break;
        case 'week':
          this.zoomLevel = 1.83;
          break;
        case 'month':
          this.zoomLevel = 0.073;
          break;
        case 'year':
          this.zoomLevel = 0.004;
          break;
        default:
          this.zoomLevel = 1;
          break;
      }

      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  clearItems(data: any) {
    try {
      // First Delete extra items
      this.itemsGroup.selectAll('.item').remove();

      // Then rebuild the data map
      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(this.dataSet); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // Update the zoom level
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  errorHandler(error: {
    error: { message: string };
    status: any;
    message: any;
  }) {
    // console.error(error);
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = error.error?.message || error.message || error;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `error Code: ${status}\nMessage: ${
        error.message
      } | more message : ${error.error?.message || error.message || error}`;
    }
    console.debug(error);
    return throwError(errorMessage);
  }
}


```



# D3.js Linebar
---

```ts
import { ElementRef, Injectable } from '@angular/core';
import * as d3 from 'd3';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Component } from '@angular/core';
import eventsMock from '../mock/events.json';
import { LinebarD3ChartComponent } from './linebar-d3-chart.component';

@Injectable({
  providedIn: 'root',
})
export class LinebarD3ChartService {
  private subscriptions: any = [];
  idName: string;
  myComponent!: LinebarD3ChartComponent;
  isLoading: boolean = false;
  margin: any;
  width: number;
  height: number;
  innerWidth: number;
  innerHeight: number;
  svg: any;
  svgCanvas: any;
  zoomCanvas: any;
  clipPath: any;
  lineLegend: any;
  codesLegend: any;
  colorsLegend: any;
  xScale: any;
  xAxis: any;
  xAxisGrid: any;
  xAxisGroup: any;
  xAxisGridGroup: any;
  yScale: any;
  yAxis: any;
  yAxisGrid: any;
  yAxisGroup: any;
  yAxisGridGroup: any;
  itemsGroup: any;
  enterSelections: any;
  dataSet: any;
  canvasGroup: any;
  zoom: any;
  nbTicks: number;
  zoomLevel: number;
  zoomPanX: number;
  zoomPanY: number;

  constructor() {
    // Initialize
    this.margin = {
      top: 40,
      bottom: 100,
      left: 100,
      right: 40,
    };
    this.idName = 'chart';
    this.width = 960;
    this.height = 480;
    this.innerWidth = this.width - this.margin.left - this.margin.right;
    this.innerHeight = this.height - this.margin.top - this.margin.bottom;
    this.nbTicks = 7;
    this.zoomLevel = 1;
    this.zoomPanX = 0;
    this.zoomPanY = 0;
    this.codesLegend = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.colorsLegend = d3
      .scaleOrdinal()
      .domain(this.codesLegend)
      .range(['yellow', 'blue', 'green', 'orange', 'purple', 'red']);
  }

  createChart(
    chartElement: any,
    data: any,
    width: any = undefined,
    height: any = undefined
  ): void {
    try {
      this.idName = chartElement.id;
      // CLean first
      d3.select(`#${this.idName} svg`).remove();
      this.zoomLevel = 1;
      this.zoomPanX = 0;
      this.zoomPanY = 0;

      if (!data) {
        throw 'No data provided';
        // return;
      }

      // Define responsive scale limits
      if (width != undefined) {
        // Height min size rule of 200px
        width = width > 600 ? width : 500;
        // width = (width > 3000 ) ? 3000 : width;
        this.innerWidth = width - this.margin.left - this.margin.right - 300; // Added a page margin of 300
        // Height min size rule of 200px
        height = height > 400 ? height : 300;
        // height = (height > 700 ) ? 600 : height;
        this.innerHeight = height - this.margin.top - this.margin.bottom;
      }

      // Create SVG Canvas viewport
      this.svg = d3
        .select(chartElement)
        .append('svg')
        .classed('chart', true)
        .attr('width', this.innerWidth + this.margin.left + this.margin.right)
        .attr('height', this.innerHeight + this.margin.top + this.margin.bottom)
        .append('g');

      // Append the zoom group
      this.zoomCanvas = this.svg
        .append('g')
        .append('rect')
        .attr('class', 'background')
        .attr('fill', '#ffffff')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .attr('pointer-events', 'fill')
        .attr('cursor', 'grab');

      // Render the axis
      this.renderXAxis();
      this.renderYAxis();

      // Add a clipPath: everything out of this area won't be drawn.
      this.clipPath = this.svg
        .append('defs')
        .append('SVG:clipPath')
        .attr('id', 'clip')
        .append('SVG:rect')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

      // Add the canvas group where items take place
      this.svgCanvas = this.svg
        .append('g')
        .classed('svg-canvas', true)
        .attr('clip-path', 'url(#clip)');

      // Update data
      this.mapData(data);

      // Creating a zoom object

      // max zoom is the ratio of the initial domain extent to the minimum
      // unit that you want to zoom to (1 minute == 1000*60 milliseconds)
      /*
      const minTimeMilli = 20000; // do not allow zooming beyond displaying 20 seconds
      const maxTimeMilli = 6.3072e+11; // approx 20 years

      const minStartTime: any = d3.min(this.dataSet, (d: any) => { return d.dateStart as any; });
      const maxEndTime: any = d3.max(this.dataSet, (d: any) => { return d.dateEnd as any; });

      const widthMilli = maxEndTime.getTime() - minStartTime.getTime();

      console.debug("minStartTime", minStartTime.getTime());
      console.debug("minStartTime", maxEndTime.getTime());
      console.debug("widthMilli", widthMilli);

      const minZoomFactor = widthMilli / maxTimeMilli;
      const maxZoomFactor = widthMilli / minTimeMilli; 
      */
      this.zoom = d3.zoom();
      // .scaleExtent([0.001, 1000]) // Limit the extent of the zoom (ex: from 10 years to 5 min)
      // .scaleExtent([minZoomFactor, maxZoomFactor])
      // .translateExtent([[0, 0], [this.innerWidth, this.innerHeight]])
      // .extent([[0, 0], [this.innerWidth, this.innerHeight]])

      // center via style css
      // this.svg.style("transform-origin", "50% 50% 0");

      // Zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.on('zoom', (event: any) => {
            this.zoomLevel = event.transform.k;
            this.zoomPanX = event.transform.x;
            this.zoomPanY = event.transform.y;
            console.debug('this.zoomLevel', this.zoomLevel);
            console.debug('this.zoomPanX', this.zoomPanX);
            console.debug('this.zoomPanY', this.zoomPanY);
            this.zoomActions(event, data);
          })
        );

      // Render items
      this.renderLegend();
      this.renderBars();
      this.renderLines();
      this.renderSymbols();
      this.renderTexts();

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderXAxis() {
    // Create x-axis and scale x
    try {
      // creating a scale with a given range (the output of the scale)
      // this.xScale = d3.scaleLinear().range([this.innerHeight - this.paddingVert, this.paddingVert]);
      this.xScale = d3
        .scaleBand()
        .range([this.innerWidth, 0])
        .paddingInner(0) // edit the inner padding value in [0,1]
        .paddingOuter(0.5) // edit the outer padding value in [0,1]
        .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right.

      // creating an axis with a given orientation, linked to the scale
      this.xAxis = d3.axisBottom(this.xScale);

      // Add X vertical gridlines
      this.xAxisGrid = this.xAxis.tickSize(-this.innerHeight).tickFormat('');
      this.xAxisGridGroup = this.svg
        .append('g')
        .attr('class', 'x-axis-grid')
        .attr(
          'transform',
          `translate(${this.margin.left}, ${-(
            this.margin.top + this.innerHeight
          )})`
        )
        .call(this.xAxisGrid);

      // Customize axis
      this.xAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.xAxisGroup = this.svg
        .append('g')
        .classed('x-axis', true)
        .attr(
          'transform',
          `translate(${this.margin.left}, ${
            this.margin.top + this.innerHeight
          })`
        )
        .call(this.xAxis); // calling the axis from the selection

      // Title label for the x axis
      this.svg
        .append('text')
        // .attr("transform", "rotate(-90)")
        .attr('x', this.margin.left + this.innerWidth / 2)
        .attr('y', this.margin.top + this.innerHeight + this.margin.bottom - 15)
        .attr('dx', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Domain axis X');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderYAxis() {
    // Create y-axis and scale y
    try {
      // creating a scale with a given range (the output of the scale)
      /* this.yScale = d3.scaleBand().range([this.innerHeight, 0])
      .paddingInner(0) // edit the inner padding value in [0,1]
      .paddingOuter(0.5) // edit the outer padding value in [0,1]
      .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right. */
      this.yScale = d3.scaleLinear().range([this.innerHeight, 0]);

      // creating an axis with a given orientation, linked to the scale
      this.yAxis = d3.axisLeft(this.yScale);

      // Add Y horizontal gridlines
      this.yAxisGrid = this.yAxis.tickSize(-this.innerWidth).tickFormat('');
      this.yAxisGridGroup = this.svg
        .append('g')
        .attr('class', 'y-axis-grid')
        .attr(
          'transform',
          'translate(' + this.margin.left + ',' + this.margin.top + ')'
        )
        .call(this.yAxisGrid);

      // Customize axis
      this.yAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.yAxisGroup = this.svg
        .append('g')
        .classed('y-axis', true)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .call(this.yAxis); // calling the axis from the selection

      // Title label for the y axis
      this.svg
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 0)
        .attr('x', 0 - this.innerHeight / 2)
        .attr('dy', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Linear axis Y');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  mapData(data: any) {
    try {
      // Add items
      this.itemsGroup = this.svgCanvas
        .append('g')
        .classed('items', true)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

      // Mapping
      // For converting strings to Dates
      const parseTime = d3.timeParse('%Y-%m-%dT%H:%M:%S%Z');
      // const strictIsoParse = d3.utcParse("%Y-%m-%dT%H:%M:%S%Z");
      this.dataSet = data
        .map((d: any) => {
          // const currDate = new Date(d.startDate);
          const dateObjStart = parseTime(d.startDate);
          const dateObjEnd = parseTime(d.endDate);

          // Return the custom data structure for d3js
          return {
            id: d.id,
            code: d.code,
            label: d.amount,
            photo: d.photo,
            dateStart: dateObjStart,
            dateEnd: dateObjEnd,
            amount: parseInt(`${d.duration}`, 10),
          };
        })
        .reverse(); // reverse array order to get a first default section in y axis

      // Update our scales and axes

      // this.xScale.domain(d3.extent(this.dataSet, (d: any) => { return d.code;}));
      /*
      this.xScale.domain([
        d3.min(this.dataSet, (d: any) => { return d.dateStart as any; }),
        d3.max(this.dataSet, (d: any) => { return d.dateEnd as any; })
      ]);
      */

      // Fix style format issues with zoom effects
      // this.xScale.tickFormat(d3.timeParse("%Y-%m-%dT%H:%M:%S%Z"));
      /*
      this.xAxisGroup.call(this.xAxis)
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", "1em")
        .attr("transform", "rotate(-10)");
      */

      /*
      this.yScale.domain([
        d3.min(this.dataSet, (d: any)=> { return ((d.amount as any)); }),
        d3.max(this.dataSet, (d: any)=> { return d.amount as any; })
      ])
      */

      this.xScale.domain(this.dataSet.map((d: any) => d.code));

      this.xAxisGroup
        .call(this.xAxis)
        .selectAll('text')
        .attr('x', -(this.xScale.bandwidth() / 2))
        .attr('y', -(this.margin.top + this.innerHeight));

      // Background x axis
      this.xAxisGroup
        .call(this.xAxis)
        .selectAll('.x-axis > .tick')
        .data(this.dataSet)
        .append('rect')
        .attr('x', -this.xScale.bandwidth())
        .attr('y', -this.innerHeight)
        .attr('width', this.xScale.bandwidth())
        .attr('height', this.innerHeight + this.margin.bottom)
        .attr('stroke', '#B8B9BA')
        .attr('fill', 'none')
        /*
        .attr("fill", (d: any) => {
          if (d.code == "fake code") {
            return "#B8B9BA"
          } else { return "#F2F2F2" };
        })
        .attr("opacity", (d: any) => {
          if (d.code == "fake code") {
            return "0.5"
          } else { return "0.6" };
        })
        */
        .attr('pointer-events', 'none'); // disable event mouses for enable zoom tool

      // this.yScale.domain(this.dataSet.map((d: any) => d.amount));
      this.yScale.domain([
        0,
        // d3.min(this.dataSet, (d: any)=> { return ((d.amount as any)); }),
        (d3.max(this.dataSet, (d: any) => {
          return d.amount as any;
        }) as any) + 1,
      ]);

      this.yAxisGroup
        .call(this.yAxis)
        .selectAll('text')
        .attr('y', (d: any) => this.yScale(d.amount));

      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(this.dataSet); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderLegend() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-legend`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items

      // this.lineLegend = this.svg.selectAll(".lineLegend").data(this.dataSet).enter().append("g")
      this.lineLegend = this.enterSelections
        .append('g')
        .attr('class', 'item-legend item-legend')
        .attr('transform', (d: any, i: any) => {
          return 'translate(10,' + (i * 20 + 10) + ')';
        });

      this.lineLegend
        .append('rect')
        .attr('fill', (d: any, i: any) => {
          return this.colorsLegend(d.amount);
        })
        .attr('stroke', 'white')
        .attr('stroke-width', 1.5)
        .attr('width', 10)
        .attr('height', 10);

      this.lineLegend
        .append('text')
        // .attr("dx", "1em")
        // .style("text-anchor", "middle")
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', 'black')
        .text((d: any) => {
          return d.code;
        })
        .attr('transform', 'translate(15,9)'); //align texts with boxes

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderLines() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-line`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items
      const line = d3
        .line()
        .x((d: any) => {
          return this.xScale(d.code);
        })
        .y((d: any) => {
          return this.yScale(d.amount);
        });

      // this.enterSelections.merge(dataBound as any).attr('transform', (d, i) => `translate(${xScale(d.date as Date)},${yScale(d.amount)})`);
      this.enterSelections
        .append('path')
        .datum(this.dataSet)
        .attr('class', 'item-line item-line')
        .attr('fill', 'none')
        .attr('stroke', 'red')
        .attr('stroke-width', 1.5)
        .attr('d', line);

      // Data dots
      this.enterSelections
        .append('circle')
        .attr('class', 'item-dot-line')
        .attr('fill', 'yellow')
        .attr('r', 5)
        .attr('cx', (d: any) => {
          return this.xScale(d.code);
        })
        .attr('cy', (d: any) => {
          return this.yScale(d.amount);
        });

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderBars() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-bar`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items

      // this.enterSelections.merge(dataBound as any).attr('transform', (d, i) => `translate(${xScale(d.date as Date)},${yScale(d.amount)})`);
      this.enterSelections
        .append('rect')
        .attr('class', 'item-bar item-rect')
        .attr(
          'x',
          (d: any) => this.xScale(d.code) - this.xScale.bandwidth() / 2
        )
        .attr('y', (d: any) => this.yScale(d.amount))
        .attr('width', this.xScale.bandwidth())
        // .attr('height', this.yScale.bandwidth())
        .attr('height', (d: any) => this.innerHeight - this.yScale(d.amount))
        .attr('fill', (d: any) => {
          return this.colorsLegend(d.amount);
        })
        .attr('opacity', '0.6')
        .attr('pointer-events', 'fill')
        .attr('cursor', 'pointer')
        .on('mouseover', (event: any, d: any) => {
          console.debug(d);
          console.debug(event);
          console.debug(d3.pointer(event));
          d3.select(event.target).attr('fill', 'green');
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).attr('fill', 'purple');
        });

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderTexts() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-text`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items
      //For converting Dates to strings
      const formatTime = d3.timeFormat('%Y-%m-%d %H:%M');

      // Generate the labels of the items first, so they are in back
      this.enterSelections
        .append('text')
        .attr('class', 'item-text')
        .text((d: any) => {
          const sampleText = `${d.code} - ${d.amount}`;
          return sampleText.substring(0, 20).toUpperCase() + '...';
        })
        .attr('x', (d: any) => {
          return this.xScale(d.code) - this.xScale.bandwidth() / 2 + 15;
        })
        .attr('y', (d: any) => {
          return this.yScale(d.amount) + 15;
        })
        .attr('font-family', 'sans-serif')
        .attr('font-size', '11px')
        .attr('fill', 'white');
      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderSymbols() {
    try {
      // Clear first all existing items
      d3.selectAll(`#${this.idName} .item-symbol`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Create a shape path

      const iconSize = 30;
      const icon = this.enterSelections
        .append('image')
        .attr('class', 'item-symbol item-image')
        .attr('x', (d: any) => {
          return this.xScale(d.code) - iconSize / 2;
        })
        .attr('y', (d: any) => {
          return this.yScale(d.amount) + 50;
        })
        .attr('width', iconSize)
        .attr('height', iconSize)
        .attr('href', (d: any) => {
          return d.photo;
        })
        .attr('pointer-events', 'fill')
        // .attr("cursor", "pointer")
        .on('mouseover', (event: any, d: any) => {
          d3.select(event.target).style('cursor', 'pointer');
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px blue)'
          );
        })
        .on('mouseout', (event: any, d: any) => {
          d3.select(event.target).style('filter', null);
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px yellow)'
          );
          this.myComponent.showPopin(d);
          // this.event = null;
        });

      /*
      // dragstarted
      .on("start", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px red)");
        d3.select(event.target)
        .raise() // D3 Raise function display current node at the top
        .classed("active", true);
      })
      // dragged
      .on("drag", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px yellow)");
        d3.select(event.target).raise() // D3 Raise function display current node at the top
        .attr("x", ()=> {
          d.x = event.x; // Todo: Issue bad behavior
          return d.x;
          })    
      })
      // dragended
      .on("end", (event: any, d: any) => {
        d3.select(event.target).classed("active", false);
        d3.select(event.target).style("filter", null);
      });
      */
    } catch (e) {
      this.errorHandler(e);
    }
  }

  zoomActions(event: any, data: any) {
    // function to redraw while zooming
    try {
      // Get X axis new scale
      // this.xScale = event.transform.rescaleX(this.xScale);
      // const newX = event.transform.rescaleX(this.xScale);
      // this.xAxis.scale(newX);
      // this.xAxisGroup.call(this.xAxis); // update the axes

      // Update text position
      /*
      this.xAxisGroup.selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", "1em")
        .attr("transform", "rotate(-10)");
      */

      // Get Y axis new scale
      // this.yScale = event.transform.rescaleY(this.yScale)
      // d3.select(`#${ this.idName } tick`).remove();
      d3.select(`#${this.idName}`)
        .selectAll('.y-axis-grid')
        .selectAll('line')
        .remove();
      // d3.selectAll('.x-axis > .tick').remove();
      // this.yAxisGroup.call(this.yAxis).selectAll('tick').remove();
      const newY = event.transform.rescaleY(this.yScale);
      this.yAxis.scale(newY);
      this.yAxisGroup.call(this.yAxis); // update the axis

      // Update the items
      // this.enterSelections.attr('transform', event.transform);
      this.enterSelections
        .selectAll('.item-text')
        // .attr("x", (d: any) => event.transform.applyX(this.xScale(d.code)) + 20)
        .attr('y', (d: any) =>
          event.transform.applyY(this.yScale(d.amount) + 15)
        );

      this.enterSelections
        .selectAll('.item-rect')
        //.attr('width', (d: any) => { return (event.transform.applyX(this.xScale(d.code)) ) })
        .attr(
          'height',
          (d: any) =>
            this.innerHeight - event.transform.applyY(this.yScale(d.amount))
        )
        //.attr('height', (d: any) => { return (event.transform.applyY(this.yScale(d.amount)) ) })
        //.attr('x', (d: any) => event.transform.applyX(this.xScale(d.code)))
        .attr('y', (d: any) => event.transform.applyY(this.yScale(d.amount)));

      const line = d3
        .line()
        .x((d: any) => {
          return this.xScale(d.code);
        })
        .y((d: any) => {
          return event.transform.applyY(this.yScale(d.amount));
        });
      this.enterSelections.selectAll('.item-line').attr('d', line);

      // Data dots
      this.enterSelections
        .selectAll('.item-dot-line')
        //.attr("cx", (d: any) => { return event.transform.applyX(this.xScale(d.code)); })
        .attr('cy', (d: any) => {
          return event.transform.applyY(this.yScale(d.amount));
        });

      this.enterSelections
        .selectAll('.item-image')
        .attr('y', (d: any) =>
          event.transform.applyY(this.yScale(d.amount) + 50)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  rescale(type: string, data: any) {
    try {
      switch (type) {
        case 'second':
          this.zoomLevel = 124308;
          break;
        case 'minute':
          this.zoomLevel = 2155;
          break;
        case 'hour':
          this.zoomLevel = 46;
          break;
        case 'day':
          this.zoomLevel = 1.65;
          break;
        case 'week':
          this.zoomLevel = 1.83;
          break;
        case 'month':
          this.zoomLevel = 0.073;
          break;
        case 'year':
          this.zoomLevel = 0.004;
          break;
        default:
          this.zoomLevel = 1;
          break;
      }

      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  clearItems(data: any) {
    try {
      // First Delete extra items
      this.itemsGroup.selectAll('.item').remove();

      // Then rebuild the data map
      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(this.dataSet); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // Update the zoom level
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  errorHandler(error: {
    error: { message: string };
    status: any;
    message: any;
  }) {
    // console.error(error);
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = error.error?.message || error.message || error;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `error Code: ${status}\nMessage: ${
        error.message
      } | more message : ${error.error?.message || error.message || error}`;
    }
    console.debug(error);
    return throwError(errorMessage);
  }
}


```



# D3.js Map
---

```ts
import { ElementRef, Injectable } from '@angular/core';
import * as d3 from 'd3';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Component } from '@angular/core';
import eventsMock from '../mock/events.json';
import { MapD3ChartComponent } from './map-d3-chart.component';

@Injectable({
  providedIn: 'root',
})
export class MapD3ChartService {
  private subscriptions: any = [];
  idName: string;
  myComponent!: MapD3ChartComponent;
  isLoading: boolean = false;
  margin: any;
  width: number;
  height: number;
  innerWidth: number;
  innerHeight: number;
  svg: any;
  svgCanvas: any;
  zoomCanvas: any;
  clipPath: any;
  lineLegend: any;
  codesLegend: any;
  colorsLegend: any;
  xScale: any;
  xAxis: any;
  xAxisGrid: any;
  xAxisGroup: any;
  xAxisGridGroup: any;
  yScale: any;
  yAxis: any;
  yAxisGrid: any;
  yAxisGroup: any;
  yAxisGridGroup: any;
  itemsGroup: any;
  enterSelections: any;
  dataSet: any;
  edgesList: any;
  canvasGroup: any;
  zoom: any;
  nbTicks: number;
  zoomLevel: number;
  zoomPanX: number;
  zoomPanY: number;
  mapWidth: number;
  mapHeight: number;

  constructor() {
    // Initialize
    this.margin = {
      top: 40,
      bottom: 100,
      left: 100,
      right: 40,
    };
    this.idName = 'chart';
    this.width = 960;
    this.height = 480;
    this.innerWidth = this.width - this.margin.left - this.margin.right;
    this.innerHeight = this.height - this.margin.top - this.margin.bottom;
    this.nbTicks = 7;
    this.zoomLevel = 1;
    this.zoomPanX = 0;
    this.zoomPanY = 0;
    this.mapWidth = 200;
    this.mapHeight = 100;
    this.codesLegend = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.colorsLegend = d3
      .scaleOrdinal()
      .domain(this.codesLegend)
      .range(['yellow', 'blue', 'green', 'orange', 'purple', 'red']);
  }

  createChart(
    chartElement: any,
    data: any,
    width: any = undefined,
    height: any = undefined
  ): void {
    try {
      this.idName = chartElement.id;
      // CLean first
      d3.select(`#${this.idName} svg`).remove();
      this.zoomLevel = 1;
      this.zoomPanX = 0;
      this.zoomPanY = 0;

      if (!data) {
        throw 'No data provided';
        // return;
      }

      // Define responsive scale limits
      if (width != undefined) {
        // Height min size rule of 200px
        width = width > 600 ? width : 500;
        // width = (width > 3000 ) ? 3000 : width;
        this.innerWidth = width - this.margin.left - this.margin.right - 300; // Added a page margin of 300
        // Height min size rule of 200px
        height = height > 400 ? height : 300;
        // height = (height > 700 ) ? 600 : height;
        this.innerHeight = height - this.margin.top - this.margin.bottom;
      }

      // Create SVG Canvas viewport
      this.svg = d3
        .select(chartElement)
        .append('svg')
        .classed('chart', true)
        .attr('width', this.innerWidth + this.margin.left + this.margin.right)
        .attr('height', this.innerHeight + this.margin.top + this.margin.bottom)
        .append('g');

      // Append the zoom group
      this.zoomCanvas = this.svg
        .append('g')
        .append('rect')
        .attr('class', 'background')
        .attr('fill', '#ffffff')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .attr('pointer-events', 'fill')
        .attr('cursor', 'grab');

      // Render the axis
      this.renderXAxis();
      this.renderYAxis();

      // Add a clipPath: everything out of this area won't be drawn.
      this.clipPath = this.svg
        .append('defs')
        .append('SVG:clipPath')
        .attr('id', 'clip')
        .append('SVG:rect')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

      // Add the canvas group where items take place
      this.svgCanvas = this.svg
        .append('g')
        .classed('svg-canvas', true)
        .attr('clip-path', 'url(#clip)');

      // Update data
      this.mapData(data);

      // Creating a zoom object

      // max zoom is the ratio of the initial domain extent to the minimum
      // unit that you want to zoom to (1 minute == 1000*60 milliseconds)
      /*
      const minTimeMilli = 20000; // do not allow zooming beyond displaying 20 seconds
      const maxTimeMilli = 6.3072e+11; // approx 20 years

      const minStartTime: any = d3.min(this.dataSet, (d: any) => { return d.dateStart as any; });
      const maxEndTime: any = d3.max(this.dataSet, (d: any) => { return d.dateEnd as any; });

      const widthMilli = maxEndTime.getTime() - minStartTime.getTime();

      console.debug("minStartTime", minStartTime.getTime());
      console.debug("minStartTime", maxEndTime.getTime());
      console.debug("widthMilli", widthMilli);

      const minZoomFactor = widthMilli / maxTimeMilli;
      const maxZoomFactor = widthMilli / minTimeMilli; 
      */
      this.zoom = d3.zoom();
      // .scaleExtent([0.001, 1000]) // Limit the extent of the zoom (ex: from 10 years to 5 min)
      // .scaleExtent([minZoomFactor, maxZoomFactor])
      // .translateExtent([[0, 0], [this.innerWidth, this.innerHeight]])
      // .extent([[0, 0], [this.innerWidth, this.innerHeight]])

      // center via style css
      // this.svg.style("transform-origin", "50% 50% 0");

      // Zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.on('zoom', (event: any) => {
            this.zoomLevel = event.transform.k;
            this.zoomPanX = event.transform.x;
            this.zoomPanY = event.transform.y;
            console.debug('this.zoomLevel', this.zoomLevel);
            console.debug('this.zoomPanX', this.zoomPanX);
            console.debug('this.zoomPanY', this.zoomPanY);
            this.zoomActions(event, data);
          })
        );

      // Render items
      this.renderLegend();
      this.renderEdges();
      this.renderNodes();
      this.renderSymbols();
      this.renderTexts();

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderXAxis() {
    // Create x-axis and scale x
    try {
      // creating a scale with a given range (the output of the scale)
      /* this.xScale = d3.scaleBand().range([0, this.innerWidth])
      .paddingInner(0) // edit the inner padding value in [0,1]
      .paddingOuter(0.5) // edit the outer padding value in [0,1]
      .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right. */
      this.xScale = d3.scaleLinear().range([0, this.innerWidth]);

      // creating an axis with a given orientation, linked to the scale
      this.xAxis = d3.axisBottom(this.xScale);

      // Add X horizontal gridlines
      this.xAxisGrid = this.xAxis.tickSize(-this.innerHeight).tickFormat('');
      this.xAxisGridGroup = this.svg
        .append('g')
        .attr('class', 'x-axis-grid')
        .attr(
          'transform',
          'translate(' +
            this.margin.left +
            ',' +
            this.margin.top +
            this.innerHeight +
            ')'
        )
        .call(this.xAxisGrid);

      // Customize axis
      this.xAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.xAxisGroup = this.svg
        .append('g')
        .classed('x-axis', true)
        .attr(
          'transform',
          `translate(${this.margin.left},${this.margin.top + this.innerHeight})`
        )
        .call(this.xAxis); // calling the axis from the selection

      // Title label for the x axis
      this.svg
        .append('text')
        // .attr("transform", "rotate(-90)")
        .attr('x', this.margin.left + this.innerWidth / 2)
        .attr('y', this.margin.top + this.innerHeight + this.margin.bottom - 15)
        .attr('dx', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Linear axis X');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderYAxis() {
    // Create y-axis and scale y
    try {
      // creating a scale with a given range (the output of the scale)
      /* this.yScale = d3.scaleBand().range([this.innerHeight, 0])
      .paddingInner(0) // edit the inner padding value in [0,1]
      .paddingOuter(0.5) // edit the outer padding value in [0,1]
      .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right. */
      this.yScale = d3.scaleLinear().range([this.innerHeight, 0]);

      // creating an axis with a given orientation, linked to the scale
      this.yAxis = d3.axisLeft(this.yScale);

      // Add Y horizontal gridlines
      this.yAxisGrid = this.yAxis.tickSize(-this.innerWidth).tickFormat('');
      this.yAxisGridGroup = this.svg
        .append('g')
        .attr('class', 'y-axis-grid')
        .attr(
          'transform',
          'translate(' + this.margin.left + ',' + this.margin.top + ')'
        )
        .call(this.yAxisGrid);

      // Customize axis
      this.yAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.yAxisGroup = this.svg
        .append('g')
        .classed('y-axis', true)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .call(this.yAxis); // calling the axis from the selection

      // Title label for the y axis
      this.svg
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 0)
        .attr('x', 0 - this.innerHeight / 2)
        .attr('dy', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Linear axis Y');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  mapData(data: any) {
    try {
      // Add items
      this.itemsGroup = this.svgCanvas
        .append('g')
        .classed('items', true)
        // Align top left
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`);
      // Align center
      //.attr('transform', 'translate(' + (this.innerWidth + this.margin.left + this.margin.right)/2 +  ',' + (this.innerHeight + this.margin.top + this.margin.bottom)/2 +')');

      // Mapping
      // For converting strings to Dates
      const parseTime = d3.timeParse('%Y-%m-%dT%H:%M:%S%Z');
      // const strictIsoParse = d3.utcParse("%Y-%m-%dT%H:%M:%S%Z");
      this.dataSet = data
        .map((d: any) => {
          // const currDate = new Date(d.startDate);
          const dateObjStart = parseTime(d.startDate);
          const dateObjEnd = parseTime(d.endDate);

          // Return the custom data structure for d3js
          return {
            id: d.id,
            code: d.code,
            label: d.label,
            photo: d.photo,
            dateStart: dateObjStart,
            dateEnd: dateObjEnd,
            amount: parseInt(`${d.duration}`, 10),
            x: parseInt(`${d.x}`, 10),
            y: parseInt(`${d.y}`, 10),
          };
        })
        .reverse(); // reverse array order to get a first default section in y axis

      this.edgesList = eventsMock.edges.map((d: any) => {
        // Return the custom edge data structure for d3js
        return {
          id: d.id,
          source: this.dataSet.find((item: any) => item.id === d.source),
          target: this.dataSet.find((item: any) => item.id === d.target),
          tag: d.tag,
        };
      });

      // Update our scales and axes

      // this.xScale.domain(d3.extent(this.dataSet, (d: any) => { return d.code;}));
      /*
      this.xScale.domain([
        d3.min(this.dataSet, (d: any) => { return d.dateStart as any; }),
        d3.max(this.dataSet, (d: any) => { return d.dateEnd as any; })
      ]);
      */

      // Fix style format issues with zoom effects
      // this.xScale.tickFormat(d3.timeParse("%Y-%m-%dT%H:%M:%S%Z"));
      /*
      this.xAxisGroup.call(this.xAxis)
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", "1em")
        .attr("transform", "rotate(-10)");
      */

      /*
      this.yScale.domain([
        d3.min(this.dataSet, (d: any)=> { return ((d.y as any)); }),
        d3.max(this.dataSet, (d: any)=> { return d.y as any; })
      ])
      */

      this.xScale.domain([
        (d3.min(this.dataSet, (d: any) => {
          return d.x as any;
        }) as any) - this.mapWidth,
        (d3.max(this.dataSet, (d: any) => {
          return d.x as any;
        }) as any) + this.mapWidth,
      ]);

      this.xAxisGroup
        .call(this.xAxis)
        .selectAll('text')
        .attr('x', (d: any) => this.xScale(d.x));

      // this.yScale.domain(this.dataSet.map((d: any) => d.amount));
      this.yScale.domain([
        (d3.min(this.dataSet, (d: any) => {
          return d.y as any;
        }) as any) - this.mapHeight,
        (d3.max(this.dataSet, (d: any) => {
          return d.y as any;
        }) as any) + this.mapHeight,
      ]);

      this.yAxisGroup
        .call(this.yAxis)
        .selectAll('text')
        .attr('y', (d: any) => this.yScale(d.y));

      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(this.dataSet); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderLegend() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-legend`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items

      // this.lineLegend = this.svg.selectAll(".lineLegend").data(this.dataSet).enter().append("g")
      this.lineLegend = this.enterSelections
        .append('g')
        .attr('class', 'item-legend')
        .attr('transform', (d: any, i: any) => {
          return 'translate(10,' + (i * 20 + 10) + ')';
        });

      this.lineLegend
        .append('rect')
        .attr('fill', (d: any, i: any) => {
          return this.colorsLegend(d.amount);
        })
        .attr('stroke', 'white')
        .attr('stroke-width', 1.5)
        .attr('width', 10)
        .attr('height', 10);

      this.lineLegend
        .append('text')
        // .attr("dx", "1em")
        // .style("text-anchor", "middle")
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', 'black')
        .text((d: any) => {
          return d.code;
        })
        .attr('transform', 'translate(15,9)'); //align texts with boxes

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderNodes() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-bar`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items

      // this.enterSelections.merge(dataBound as any).attr('transform', (d, i) => `translate(${xScale(d.date as Date)},${yScale(d.amount)})`);
      this.enterSelections
        .append('rect')
        .attr('class', 'item-bar item-rect')
        .attr('x', (d: any) => this.xScale(d.x) - this.mapWidth / 2)
        .attr('y', (d: any) => this.yScale(d.y) - this.mapHeight / 2)
        .attr('width', this.mapWidth)
        // .attr('height', this.yScale.bandwidth())
        .attr('height', this.mapHeight)
        .attr('fill', (d: any) => {
          return this.colorsLegend(d.amount);
        })
        .attr('opacity', '0.6')
        .attr('pointer-events', 'fill')
        .attr('cursor', 'pointer')
        .on('mouseover', (event: any, d: any) => {
          console.debug(d);
          console.debug(event);
          console.debug(d3.pointer(event));
          d3.select(event.target).attr('fill', 'green');
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).attr('fill', 'purple');
        });

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderTexts() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-text`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items
      //For converting Dates to strings
      const formatTime = d3.timeFormat('%Y-%m-%d %H:%M');

      // Generate the labels of the items first, so they are in back
      this.enterSelections
        .append('text')
        .attr('class', 'item-text')
        .text((d: any) => {
          const sampleText = `${d.code} - ${d.label}`;
          return sampleText.substring(0, 20).toUpperCase() + '...';
        })
        .attr('x', (d: any) => {
          return this.xScale(d.x) - this.mapWidth / 2 + 15;
        })
        .attr('y', (d: any) => {
          return this.yScale(d.y) - this.mapHeight / 2 + 15;
        })
        .attr('font-family', 'sans-serif')
        .attr('font-size', '11px')
        .attr('fill', 'white');
      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderSymbols() {
    try {
      // Clear first all existing items
      d3.selectAll(`#${this.idName} .item-symbol`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Create a shape path

      const iconSize = 30;
      const icon = this.enterSelections
        .append('image')
        .attr('class', 'item-symbol item-image')
        .attr('x', (d: any) => {
          return this.xScale(d.x) - iconSize + this.mapWidth / 2 - 5;
        })
        .attr('y', (d: any) => {
          return this.yScale(d.y) + iconSize / 2 - this.mapHeight / 2 - 5;
        })
        .attr('width', iconSize)
        .attr('height', iconSize)
        .attr('href', (d: any) => {
          return d.photo;
        })
        .attr('pointer-events', 'fill')
        // .attr("cursor", "pointer")
        .on('mouseover', (event: any, d: any) => {
          d3.select(event.target).style('cursor', 'pointer');
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px blue)'
          );
        })
        .on('mouseout', (event: any, d: any) => {
          d3.select(event.target).style('filter', null);
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px yellow)'
          );
          this.myComponent.showPopin(d);
          // this.event = null;
        });

      /*
      // dragstarted
      .on("start", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px red)");
        d3.select(event.target)
        .raise() // D3 Raise function display current map at the top
        .classed("active", true);
      })
      // dragged
      .on("drag", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px yellow)");
        d3.select(event.target).raise() // D3 Raise function display current map at the top
        .attr("x", ()=> {
          d.x = event.x; // Todo: Issue bad behavior
          return d.x;
          })    
      })
      // dragended
      .on("end", (event: any, d: any) => {
        d3.select(event.target).classed("active", false);
        d3.select(event.target).style("filter", null);
      });
      */
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderEdges() {
    try {
      console.debug('render edges', this.edgesList);
      //Draw connection
      const edgesLine = this.itemsGroup
        .append('g')
        .classed('item', true)
        .selectAll('.item-edge')
        .data(this.edgesList) // bind data
        .enter() // Add a corresponding number of placeholders for the data
        .append('path') // Generate a polyline on the placeholder (draw with path)
        .attr('class', 'item-edge')
        .attr('d', (d: any) => {
          const source = `M ${this.xScale(d.source.x)} ${
            this.yScale(d.source.y) + 5
          } `;
          // const marginSource = ` H ${this.xScale(d.source.x) } `;
          const marginTarget = ` V ${this.yScale(d.target.y) + 5} `;
          const target = `H ${this.xScale(d.target.x)} `;
          // const target2 = `L ${this.xScale(d.target.x)} ${this.yScale(d.target.y) + 5}`;
          return `${source + marginTarget + target}`;
          //return `${source + target2}`;
        }) //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
        //.attr('d', (d: any) => { return d && 'M ' + d.source.x + ' ' + d.source.y + ' L ' + d.target.x + ' ' + d.target.y; }) //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
        .attr('id', (d: any, i: any) => {
          return `${this.idName}-edgepath-${i}`;
        }) // Set id, used for connecting text
        .attr('marker-start', 'url(#joinStart)') // Mark the arrow according to the id number marked by the arrow
        .attr('marker-mid', 'url(#joinMid)') // Mark the arrow according to the id number marked by the arrow
        .attr('marker-end', 'url(#joinEnd)') // Mark the arrow according to the id number marked by the arrow
        .style('stroke', 'black') // colour
        .style('fill', 'none') // colour
        .style('stroke-width', 2); // Thickness

      //Connection name
      const edgeLabel = this.itemsGroup
        .append('g')
        .classed('item', true)
        .selectAll(`${this.idName}-item-edge-label`)
        .data(this.edgesList) // bind data
        .enter() // Add a corresponding number of placeholders for the data
        .append('text') // Create a text area for each connection
        .attr('class', `${this.idName}-item-edge-label`)
        //.attr('dx', 80)
        .attr('dy', 15)
        .attr('x', (d: any) => this.xScale(d.x))
        .attr('y', (d: any) => this.yScale(d.y));
      // .style("text-anchor", "middle")
      //.attr("transform", "rotate(-60)");

      edgeLabel
        .append('textPath')
        .style('pointer-events', 'none') // Disable mouse events
        .attr('font-family', 'sans-serif')
        .attr('font-size', '12px')
        .attr('fill', 'black')
        .attr('startOffset', '50%')
        .attr('xlink:href', (d: any, i: any) => {
          return `#${this.idName}-edgepath-${i}`;
        }) // The text is arranged on the line corresponding to the id
        .text((d: any) => {
          return d.tag;
        }); // Set text content

      //Draw the arrow on the line
      const defs = this.itemsGroup.append('defs'); // defs defines reusable elements
      const joinEnd = defs
        .append('marker') // Create arrow
        .attr('id', 'joinEnd')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinEnd')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-5 -5 10 10') // viewBox
        .attr('refX', 6) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinEnd
        .append('path')
        .attr('d', 'M 0,0 m -5,-5 L 5,0 L -5,5 Z') // d: path description, Bezier curve
        .attr('fill', 'red'); // fill color

      const joinStart = defs
        .append('marker') // Create arrow
        .attr('id', 'joinStart')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinStart')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-6 -6 12 12') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinStart
        .append('path')
        .attr('d', 'M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0') // Circle d: path description, Bezier curve
        .attr('fill', 'orange'); // fill color

      const joinMid = defs
        .append('marker') // Create arrow
        .attr('id', 'joinMid')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinMid')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-6 -6 12 12') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinMid
        .append('path')
        .attr('d', 'M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0') // Circle d: path description, Bezier curve
        .attr('fill', 'black'); // fill color
    } catch (e) {
      this.errorHandler(e);
    }
  }

  zoomActions(event: any, data: any) {
    // function to redraw while zooming
    try {
      // Get X axis new scale
      // this.xScale = event.transform.rescaleX(this.xScale);
      // const newX = event.transform.rescaleX(this.xScale);
      // this.xAxis.scale(newX);
      // this.xAxisGroup.call(this.xAxis); // update the axes

      // Update text position
      /*
      this.xAxisGroup.selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", "1em")
        .attr("transform", "rotate(-10)");
      */

      // Get Y axis new scale
      // this.yScale = event.transform.rescaleY(this.yScale)
      // d3.select(`#${ this.idName } tick`).remove();
      d3.select(`#${this.idName}`)
        .selectAll('.x-axis-grid')
        .selectAll('line')
        .remove();
      d3.select(`#${this.idName}`)
        .selectAll('.y-axis-grid')
        .selectAll('line')
        .remove();
      // d3.selectAll('.x-axis > .tick').remove();
      // this.yAxisGroup.call(this.yAxis).selectAll('tick').remove();
      const newX = event.transform.rescaleX(this.xScale);
      this.xAxis.scale(newX);
      this.xAxisGroup.call(this.xAxis); // update the axis

      const newY = event.transform.rescaleY(this.yScale);
      this.yAxis.scale(newY);
      this.yAxisGroup.call(this.yAxis); // update the axis

      // Update the items
      // this.enterSelections.attr('transform', event.transform);
      this.enterSelections
        .selectAll('.item-text')
        // .attr("x", (d: any) => event.transform.applyX(this.xScale(d.code)) + 20)
        .attr('x', (d: any) =>
          event.transform.applyX(this.xScale(d.x) - this.mapWidth / 2 + 15)
        )
        .attr('y', (d: any) =>
          event.transform.applyY(this.yScale(d.y) - this.mapHeight / 2 + 15)
        );

      this.enterSelections
        .selectAll('.item-rect')
        .attr('transform', event.transform); // auto zoom and auto pan
      //.attr('width', (d: any) => { return (event.transform.applyX(this.xScale(d.code)) ) })
      // .attr('width', (d: any) => event.transform.applyX(this.xScale(d.x)))
      // .attr('height', (d: any) => event.transform.applyY(this.yScale(d.y)))
      //.attr('height', (d: any) => { return (event.transform.applyY(this.yScale(d.y)) ) })
      //.attr('x', (d: any) => event.transform.applyX(this.xScale(d.code)))
      //.attr('x', (d: any) => event.transform.applyX(this.xScale(d.x)))
      //.attr('y', (d: any) => event.transform.applyY(this.yScale(d.y)));

      // Data dots
      this.enterSelections
        .selectAll('.item-dot-line')
        .attr('cx', (d: any) => {
          return event.transform.applyX(this.xScale(d.x));
        })
        .attr('cy', (d: any) => {
          return event.transform.applyY(this.yScale(d.y));
        });

      this.enterSelections
        .selectAll('.item-image')
        .attr('transform', event.transform); // auto zoom and auto pan
      // .attr('x', (d: any) => event.transform.applyX(this.xScale(d.x) + 50))
      // .attr('y', (d: any) => event.transform.applyY(this.yScale(d.y) + 50));

      this.itemsGroup.selectAll('.item-edge').attr('d', (d: any) => {
        const source = `M ${event.transform.applyX(
          this.xScale(d.source.x)
        )} ${event.transform.applyY(this.yScale(d.source.y) + 5)} `;
        // const marginSource = ` H ${event.transform.applyX(this.xScale(d.source.x))} `;
        const marginTarget = ` V ${event.transform.applyY(
          this.yScale(d.target.y) + 5
        )} `;
        const target = `H ${event.transform.applyX(this.xScale(d.target.x))} `;
        // const target2 = `L ${event.transform.applyX(this.xScale(d.target.x))} ${event.transform.applyY(this.yScale(d.target.y) + 5)} `;
        return `${source + marginTarget + target}`;
        // return `${source + target2}`;
      }); //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
    } catch (e) {
      this.errorHandler(e);
    }
  }

  rescale(type: string, data: any) {
    try {
      switch (type) {
        case 'second':
          this.zoomLevel = 124308;
          break;
        case 'minute':
          this.zoomLevel = 2155;
          break;
        case 'hour':
          this.zoomLevel = 46;
          break;
        case 'day':
          this.zoomLevel = 1.65;
          break;
        case 'week':
          this.zoomLevel = 1.83;
          break;
        case 'month':
          this.zoomLevel = 0.073;
          break;
        case 'year':
          this.zoomLevel = 0.004;
          break;
        default:
          this.zoomLevel = 1;
          break;
      }

      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  clearItems(data: any) {
    try {
      // First Delete extra items
      this.itemsGroup.selectAll('.item').remove();

      // Then rebuild the data map
      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(this.dataSet); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // Update the zoom level
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  errorHandler(error: {
    error: { message: string };
    status: any;
    message: any;
  }) {
    // console.error(error);
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = error.error?.message || error.message || error;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `error Code: ${status}\nMessage: ${
        error.message
      } | more message : ${error.error?.message || error.message || error}`;
    }
    console.debug(error);
    return throwError(errorMessage);
  }
}


```



# D3.js Node
---

```ts
import { ElementRef, Injectable } from '@angular/core';
import * as d3 from 'd3';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Component } from '@angular/core';
import eventsMock from '../mock/events.json';
import { NodeD3ChartComponent } from './node-d3-chart.component';

@Injectable({
  providedIn: 'root',
})
export class NodeD3ChartService {
  private subscriptions: any = [];
  idName: string;
  myComponent!: NodeD3ChartComponent;
  isLoading: boolean = false;
  margin: any;
  width: number;
  height: number;
  innerWidth: number;
  innerHeight: number;
  svg: any;
  svgCanvas: any;
  zoomCanvas: any;
  clipPath: any;
  lineLegend: any;
  codesLegend: any;
  colorsLegend: any;
  xScale: any;
  xAxis: any;
  xAxisGrid: any;
  xAxisGroup: any;
  xAxisGridGroup: any;
  yScale: any;
  yAxis: any;
  yAxisGrid: any;
  yAxisGroup: any;
  yAxisGridGroup: any;
  itemsGroup: any;
  enterSelections: any;
  dataSet: any;
  edgesList: any;
  canvasGroup: any;
  zoom: any;
  nbTicks: number;
  zoomLevel: number;
  zoomPanX: number;
  zoomPanY: number;
  nodeWidth: number;
  nodeHeight: number;
  simulation: any;
  iconSize: number;
  nodesForceMargin: number;

  constructor() {
    // Initialize
    this.margin = {
      top: 40,
      bottom: 100,
      left: 100,
      right: 40,
    };
    this.idName = 'chart';
    this.width = 960;
    this.height = 480;
    this.innerWidth = this.width - this.margin.left - this.margin.right;
    this.innerHeight = this.height - this.margin.top - this.margin.bottom;
    this.nbTicks = 7;
    this.zoomLevel = 1;
    this.zoomPanX = 0;
    this.zoomPanY = 0;
    this.iconSize = 30;
    this.nodeWidth = 200;
    this.nodeHeight = 100;
    this.nodesForceMargin = 100;
    this.codesLegend = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.colorsLegend = d3
      .scaleOrdinal()
      .domain(this.codesLegend)
      .range(['yellow', 'blue', 'green', 'orange', 'purple', 'red']);
  }

  createChart(
    chartElement: any,
    data: any,
    zoom: string = '1',
    width: any = undefined,
    height: any = undefined
  ): void {
    try {
      if (this.simulation) {
        this.simulation.stop();
      }

      this.idName = chartElement.id;
      // CLean first
      d3.select(`#${this.idName} svg`).remove();
      this.zoomLevel = parseFloat(zoom) || 1;
      this.zoomPanX = 0;
      this.zoomPanY = 0;

      if (!data) {
        throw 'No data provided';
        // return;
      }

      // Define responsive scale limits
      if (width != undefined) {
        // Height min size rule of 200px
        width = width > 600 ? width : 500;
        // width = (width > 3000 ) ? 3000 : width;
        this.innerWidth = width - this.margin.left - this.margin.right - 300; // Added a page margin of 300
        // Height min size rule of 200px
        height = height > 400 ? height : 300;
        // height = (height > 700 ) ? 600 : height;
        this.innerHeight = height - this.margin.top - this.margin.bottom;
        // this.zoomLevel = (this.margin.left + this.innerWidth)/width;
      }

      // Create SVG Canvas viewport
      this.svg = d3
        .select(chartElement)
        .append('svg')
        .classed('chart', true)
        .attr('width', this.innerWidth + this.margin.left + this.margin.right)
        .attr('height', this.innerHeight + this.margin.top + this.margin.bottom)
        // better to keep the viewBox dimensions with variables
        // .attr("viewBox", `0 0 ${ this.innerWidth + this.margin.left + this.margin.right } ${ this.innerHeight + this.margin.top + this.margin.bottom }` )
        // .attr("preserveAspectRatio", "xMidYMid meet")
        .append('g');

      // Append the zoom group
      this.zoomCanvas = this.svg
        .append('g')
        .append('rect')
        .attr('class', 'background')
        .attr('fill', '#ffffff')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .attr('pointer-events', 'fill')
        .attr('cursor', 'grab');

      // Render the axis
      this.renderXAxis();
      this.renderYAxis();

      // Add a clipPath: everything out of this area won't be drawn.
      this.clipPath = this.svg
        .append('defs')
        .append('SVG:clipPath')
        .attr('id', 'clip')
        .append('SVG:rect')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

      // Add the canvas group where items take place
      this.svgCanvas = this.svg
        .append('g')
        .classed('svg-canvas', true)
        .attr('clip-path', 'url(#clip)');

      // Add simulation and force

      // Let's list the force we wanna apply on the network
      this.simulation = d3
        .forceSimulation()
        // pull nodes towards the centre with y stronger so nodes fill the landscape screen better.
        .force(
          'x',
          d3.forceX().x((d: any) => d.x)
        )
        .force(
          'y',
          d3.forceY().y((d: any) => d.y)
        )
        // pulls all nodes to the center
        .force(
          'center',
          d3.forceCenter(this.innerWidth / 2, this.innerHeight / 2)
        )
        // nodes repel from each other which prevents overlap
        .force('charge', d3.forceManyBody().strength(-this.nodesForceMargin))
        // specifies that id is the link variable
        .force(
          'link',
          d3
            .forceLink()
            .id((d: any) => d.id)
            .distance(this.nodesForceMargin)
        )
        // specify a ‘repel radius’ to prevent overlap and leave space for label
        .force(
          'collide',
          d3.forceCollide().radius((d) => this.nodesForceMargin * 1.3)
        );

      this.simulation.stop();

      // Update data
      this.mapData(data);

      // Creating a zoom object

      // max zoom is the ratio of the initial domain extent to the minimum
      // unit that you want to zoom to (1 minute == 1000*60 milliseconds)
      /*
      const minTimeMilli = 20000; // do not allow zooming beyond displaying 20 seconds
      const maxTimeMilli = 6.3072e+11; // approx 20 years

      const minStartTime: any = d3.min(this.dataSet, (d: any) => { return d.dateStart as any; });
      const maxEndTime: any = d3.max(this.dataSet, (d: any) => { return d.dateEnd as any; });

      const widthMilli = maxEndTime.getTime() - minStartTime.getTime();

      console.debug("minStartTime", minStartTime.getTime());
      console.debug("minStartTime", maxEndTime.getTime());
      console.debug("widthMilli", widthMilli);

      const minZoomFactor = widthMilli / maxTimeMilli;
      const maxZoomFactor = widthMilli / minTimeMilli; 
      */
      this.zoom = d3.zoom();
      // .scaleExtent([0.001, 1000]) // Limit the extent of the zoom (ex: from 10 years to 5 min)
      // .scaleExtent([minZoomFactor, maxZoomFactor])
      // .translateExtent([[0, 0], [this.innerWidth, this.innerHeight]])
      // .extent([[0, 0], [this.innerWidth, this.innerHeight]])

      // center via style css
      // this.svg.style("transform-origin", "50% 50% 0");

      // Zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.on('zoom', (event: any) => {
            this.zoomLevel = event.transform.k;
            this.zoomPanX = event.transform.x;
            this.zoomPanY = event.transform.y;
            this.zoomActions(event, data);
          })
        );

      // Update the zoom level
      /*
      if (this.zoomCanvas) this.zoomCanvas
        .call(this.zoom.transform, d3.zoomIdentity.translate(this.zoomPanX, this.zoomPanY).scale(this.zoomLevel));
      */

      // Render items
      this.renderLegend();
      this.renderNodes();
      this.renderEdges();
      this.renderSymbols();
      this.renderTexts();
      // Simulate forces
      this.simulateForces();

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderXAxis() {
    // Create x-axis and scale x
    try {
      // creating a scale with a given range (the output of the scale)
      /* this.xScale = d3.scaleBand().range([0, this.innerWidth])
      .paddingInner(0) // edit the inner padding value in [0,1]
      .paddingOuter(0.5) // edit the outer padding value in [0,1]
      .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right. */
      this.xScale = d3.scaleLinear().range([0, this.innerWidth]);

      // creating an axis with a given orientation, linked to the scale
      this.xAxis = d3.axisBottom(this.xScale);

      // Add X horizontal gridlines
      this.xAxisGrid = this.xAxis.tickSize(-this.innerHeight).tickFormat('');
      this.xAxisGridGroup = this.svg
        .append('g')
        .attr('class', 'x-axis-grid')
        .attr(
          'transform',
          'translate(' +
            this.margin.left +
            ',' +
            this.margin.top +
            this.innerHeight +
            ')'
        )
        .call(this.xAxisGrid);

      // Customize axis
      this.xAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.xAxisGroup = this.svg
        .append('g')
        .classed('x-axis', true)
        .attr(
          'transform',
          `translate(${this.margin.left},${this.margin.top + this.innerHeight})`
        )
        .call(this.xAxis); // calling the axis from the selection

      // Title label for the x axis
      this.svg
        .append('text')
        // .attr("transform", "rotate(-90)")
        .attr('x', this.margin.left + this.innerWidth / 2)
        .attr('y', this.margin.top + this.innerHeight + this.margin.bottom - 15)
        .attr('dx', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Linear axis X');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderYAxis() {
    // Create y-axis and scale y
    try {
      // creating a scale with a given range (the output of the scale)
      /* this.yScale = d3.scaleBand().range([this.innerHeight, 0])
      .paddingInner(0) // edit the inner padding value in [0,1]
      .paddingOuter(0.5) // edit the outer padding value in [0,1]
      .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right. */
      this.yScale = d3.scaleLinear().range([this.innerHeight, 0]);

      // creating an axis with a given orientation, linked to the scale
      this.yAxis = d3.axisLeft(this.yScale);

      // Add Y horizontal gridlines
      this.yAxisGrid = this.yAxis.tickSize(-this.innerWidth).tickFormat('');
      this.yAxisGridGroup = this.svg
        .append('g')
        .attr('class', 'y-axis-grid')
        .attr(
          'transform',
          'translate(' + this.margin.left + ',' + this.margin.top + ')'
        )
        .call(this.yAxisGrid);

      // Customize axis
      this.yAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.yAxisGroup = this.svg
        .append('g')
        .classed('y-axis', true)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .call(this.yAxis); // calling the axis from the selection

      // Title label for the y axis
      this.svg
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 0)
        .attr('x', 0 - this.innerHeight / 2)
        .attr('dy', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Linear axis Y');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  mapData(data: any) {
    try {
      // Add items
      this.itemsGroup = this.svgCanvas
        .append('g')
        .classed('items', true)
        // Align top left
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`);
      // Align center
      //.attr('transform', 'translate(' + (this.innerWidth + this.margin.left + this.margin.right)/2 +  ',' + (this.innerHeight + this.margin.top + this.margin.bottom)/2 +')');

      // Mapping
      // For converting strings to Dates
      const parseTime = d3.timeParse('%Y-%m-%dT%H:%M:%S%Z');
      // const strictIsoParse = d3.utcParse("%Y-%m-%dT%H:%M:%S%Z");
      this.dataSet = data
        .map((d: any) => {
          // const currDate = new Date(d.startDate);
          const dateObjStart = parseTime(d.startDate);
          const dateObjEnd = parseTime(d.endDate);

          // Return the custom data structure for d3js
          return {
            id: d.id,
            code: d.code,
            label: d.label,
            photo: d.photo,
            dateStart: dateObjStart,
            dateEnd: dateObjEnd,
            amount: parseInt(`${d.duration}`, 10),
            x: parseInt(`${d.x}`, 10),
            y: parseInt(`${d.y}`, 10),
          };
        })
        .reverse(); // reverse array order to get a first default section in y axis

      this.edgesList = eventsMock.edges.map((d: any) => {
        // Return the custom edge data structure for d3js
        return {
          id: d.id,
          source: this.dataSet.find((item: any) => item.id === d.source),
          target: this.dataSet.find((item: any) => item.id === d.target),
          tag: d.tag,
        };
      });

      // Update our scales and axes

      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(this.dataSet); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderLegend() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-legend`).remove();

      // reset zoom
      /*
      if (this.zoomCanvas) this.zoomCanvas
        .call(this.zoom.transform, d3.zoomIdentity.translate(this.zoomPanX, this.zoomPanY).scale(this.zoomLevel));
      */
      // Update all existing items

      // this.lineLegend = this.svg.selectAll(".lineLegend").data(this.dataSet).enter().append("g")
      this.lineLegend = this.enterSelections
        .append('g')
        .attr('class', 'item-legend')
        .attr('transform', (d: any, i: any) => {
          return 'translate(10,' + (i * 20 + 10) + ')';
        });

      this.lineLegend
        .append('rect')
        .attr('fill', (d: any, i: any) => {
          return this.colorsLegend(d.amount);
        })
        .attr('stroke', 'white')
        .attr('stroke-width', 1.5)
        .attr('width', 10)
        .attr('height', 10);

      this.lineLegend
        .append('text')
        // .attr("dx", "1em")
        // .style("text-anchor", "middle")
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', 'black')
        .text((d: any) => {
          return d.code;
        })
        .attr('transform', 'translate(15,9)'); //align texts with boxes

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderNodes() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-bar`).remove();

      // reset zoom
      /*
      if (this.zoomCanvas) this.zoomCanvas
        .call(this.zoom.transform, d3.zoomIdentity.translate(this.zoomPanX, this.zoomPanY).scale(this.zoomLevel));
      */

      // Update all existing items

      // this.enterSelections.merge(dataBound as any).attr('transform', (d, i) => `translate(${xScale(d.date as Date)},${yScale(d.amount)})`);
      const node = this.enterSelections
        /*
        this.itemsGroup
        .selectAll('.item-rect')
        .data(this.dataSet) // bind data
        .enter() // Add a corresponding number of placeholders for the data
        */
        .append('rect')
        .attr('class', 'item-bar item-rect')
        .attr('width', this.nodeWidth)
        // .attr('height', this.yScale.bandwidth())
        .attr('height', this.nodeHeight)
        .attr('fill', (d: any) => {
          return this.colorsLegend(d.amount);
        })
        .attr('opacity', '0.6')
        .attr('pointer-events', 'fill')
        .attr('cursor', 'pointer')
        .on('mouseover', (event: any, d: any) => {
          console.debug('EVENT ON MOUSEOVER', event);
          d3.select(event.target).attr('fill', 'green');
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).attr('fill', 'purple');
        });

      /*
      // dragstarted
      .on("start", (event: any, d: any) => {
        console.debug("***********************", JSON.stringify(event, null, 4))
        // event.sourceEvent.stopPropagation();
        d3.select(event.target)
        .raise() // D3 Raise function display current node at the top
        // .node().focus()
        .classed("dragging", true)
        .classed("active", true)
        .style("filter", "drop-shadow(0px 0px 6px red)");
      })
      // dragged
      .on("drag", (event: any, d: any) => {
        d3.select(event.target)
          // .raise() // D3 Raise function display current node at the top
          .style("filter", "drop-shadow(0px 0px 6px yellow)")
          .attr("x", () => {
            d.x = event.x; // Todo: Issue bad behavior
            return d.x;
          })
          .attr("y", () => {
            d.y = event.y; // Todo: Issue bad behavior
            return d.y;
          })
      })
      // dragended
      .on("end", (event: any, d: any) => {
        d3.select(event.target)
        .classed("dragging", false)
        .classed("active", false)
        .style("filter", null);
      });
      */

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderEdges() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-edge`).remove();
      d3.selectAll(`.${this.idName}-item-edge-label`).remove();

      // reset zoom
      /*
      if (this.zoomCanvas) this.zoomCanvas
        .call(this.zoom.transform, d3.zoomIdentity.translate(this.zoomPanX, this.zoomPanY).scale(this.zoomLevel));
      */

      // Update all existing items

      //Draw connection
      const edge = this.itemsGroup
        .append('g')
        .classed('item', true)
        .selectAll('.item-edge')
        .data(this.edgesList) // bind data
        .enter() // Add a corresponding number of placeholders for the data
        .append('path') // Generate a polyline on the placeholder (draw with path)
        .attr('class', 'item-edge')
        .attr('d', (d: any) => {
          return (
            d &&
            'M ' +
              d.source.x +
              ' ' +
              d.source.y +
              ' L ' +
              d.target.x +
              ' ' +
              d.target.y
          );
        }) //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
        .attr('id', (d: any, i: any) => {
          return `${this.idName}-edgepath-${i}`;
        }) // Set id, used for connecting text
        .attr('marker-start', 'url(#joinStart)') // Mark the arrow according to the id number marked by the arrow
        .attr('marker-mid', 'url(#joinMid)') // Mark the arrow according to the id number marked by the arrow
        .attr('marker-end', 'url(#joinEnd)') // Mark the arrow according to the id number marked by the arrow
        .style('stroke', 'black') // colour
        .style('fill', 'none') // colour
        .style('stroke-width', 2); // Thickness

      //Connection name
      const edgeLabel = this.itemsGroup
        .append('g')
        .classed('item', true)
        .selectAll(`${this.idName}-item-edge-label`)
        .data(this.edgesList) // bind data
        .enter() // Add a corresponding number of placeholders for the data
        .append('text') // Create a text area for each connection
        .attr('class', `${this.idName}-item-edge-label`)
        //.attr('dx', 80)
        .attr('dy', 15);
      /*
      .attr('x', (d: any) => d.x)
      .attr('y', (d: any) => d.y)
      */
      // .style("text-anchor", "middle")
      //.attr("transform", "rotate(-60)");

      edgeLabel
        .append('textPath')
        .style('pointer-events', 'none') // Disable mouse events
        .attr('font-family', 'sans-serif')
        .attr('font-size', '12px')
        .attr('fill', 'black')
        .attr('startOffset', '50%')
        .attr('xlink:href', (d: any, i: any) => {
          return `#${this.idName}-edgepath-${i}`;
        }) // The text is arranged on the line corresponding to the id
        .text((d: any) => {
          return d.tag;
        }); // Set text content

      //Draw the arrow on the line
      const defs = this.itemsGroup.append('defs'); // defs defines reusable elements
      const joinEnd = defs
        .append('marker') // Create arrow
        .attr('id', 'joinEnd')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinEnd')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-5 -5 10 10') // viewBox
        .attr('refX', 6) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinEnd
        .append('path')
        .attr('d', 'M 0,0 m -5,-5 L 5,0 L -5,5 Z') // d: path description, Bezier curve
        .attr('fill', 'red'); // fill color

      const joinStart = defs
        .append('marker') // Create arrow
        .attr('id', 'joinStart')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinStart')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-6 -6 12 12') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinStart
        .append('path')
        .attr('d', 'M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0') // Circle d: path description, Bezier curve
        .attr('fill', 'orange'); // fill color

      const joinMid = defs
        .append('marker') // Create arrow
        .attr('id', 'joinMid')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinMid')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-6 -6 12 12') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinMid
        .append('path')
        .attr('d', 'M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0') // Circle d: path description, Bezier curve
        .attr('fill', 'black'); // fill color

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  simulateForces() {
    try {
      this.simulation.nodes(this.dataSet);

      this.simulation.force('link').links(this.edgesList);

      // on Update = tick
      this.simulation.on('tick', () => {
        this.enterSelections
          .selectAll('.item-rect')
          .attr('transform', (d: any) => {
            return 'translate(' + d.x + ',' + d.y + ')';
          });
        // .attr("fixed", (d: any) => { d.fixed = true }); //disable force on load

        this.enterSelections
          .selectAll('.item-text')
          // .attr("x", (d: any) => event.transform.applyX(this.xScale(d.code)) + 20)
          .attr('x', (d: any) => d.x + 15)
          .attr('y', (d: any) => d.y + 15);

        this.enterSelections
          .selectAll('.item-image')
          .attr('x', (d: any) => {
            return d.x + this.nodeWidth / 2 - this.iconSize / 2;
          })
          .attr('y', (d: any) => {
            return d.y + this.nodeHeight - this.iconSize;
          });

        const edge = this.itemsGroup.selectAll('.item-edge');
        const edgeLabel = this.itemsGroup.selectAll(
          `.${this.idName}-item-edge-label`
        );

        edge.attr('d', (d: any) => {
          return (
            d &&
            'M ' +
              d.source.x +
              ' ' +
              d.source.y +
              ' L ' +
              d.target.x +
              ' ' +
              d.target.y
          );
        }); //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
        /*
        .attr("x1", (d: any) => { return d.source.x; })
        .attr("y1", (d: any) => { return d.source.y; })
        .attr("x2", (d: any) => { return d.target.x; })
        .attr("y2", (d: any) => { return d.target.y; });
        */

        let source_x_modifier = 0;
        let target_x_modifier = 0;
        let source_y_modifier = 0;
        let target_y_modifier = 0;

        edge.attr('d', (d: any) => {
          // Calculate X1
          if (
            d.target.x - this.nodeWidth < d.source.x &&
            d.source.x < d.target.x + this.nodeWidth
          ) {
            //if nodes are nearly on top of each other
            // x-modifier should be zero
            source_x_modifier = 0;
            target_x_modifier = 0;
            //y modifier is determined besed on placement
            if (d.source.y > d.target.y) {
              source_y_modifier = -(this.nodeHeight / 2);
              target_y_modifier = this.nodeHeight / 2;
            } else {
              source_y_modifier = this.nodeHeight / 2;
              target_y_modifier = -(this.nodeHeight / 2);
            }
          } else {
            // they are more side to side than on top of each other
            // y modifier should be in the middle
            source_y_modifier = 0;
            target_y_modifier = 0;
            if (d.source.x > d.target.x) {
              source_x_modifier = -(this.nodeWidth / 2);
              target_x_modifier = this.nodeWidth / 2;
            } else {
              source_x_modifier = this.nodeWidth / 2;
              target_x_modifier = +(this.nodeWidth / 2);
            }
          }

          const x1 = d.source.x + source_x_modifier + this.nodeWidth / 2;

          // Caculate Y1
          if (
            d.target.x - this.nodeWidth < d.source.x &&
            d.source.x < d.target.x + this.nodeWidth
          ) {
            //if nodes are nearly on top of each other
            // x-modifier should be zero
            source_x_modifier = 0;
            target_x_modifier = 0;
            //y modifier is determined besed on placement
            if (d.source.y > d.target.y) {
              source_y_modifier = -(this.nodeHeight / 2);
              target_y_modifier = this.nodeHeight / 2;
            } else {
              source_y_modifier = this.nodeHeight / 2;
              target_y_modifier = -(this.nodeHeight / 2);
            }
          } else {
            // they are more side to side than on top of each other
            // y modifier should be in the middle
            source_y_modifier = 0;
            target_y_modifier = 0;
            if (d.source.x > d.target.x) {
              source_x_modifier = -(this.nodeWidth / 2);
              target_x_modifier = this.nodeWidth / 2;
            } else {
              source_x_modifier = this.nodeWidth / 2;
              target_x_modifier = +(this.nodeWidth / 2);
            }
          }

          const y1 = d.source.y + source_y_modifier + this.nodeHeight / 2;

          // Calculate X2
          if (
            d.target.x - this.nodeWidth < d.source.x &&
            d.source.x < d.target.x + this.nodeWidth
          ) {
            //if nodes are nearly on top of each other
            // x-modifier should be zero
            source_x_modifier = 0;
            target_x_modifier = 0;
            //y modifier is determined besed on placement
            if (d.source.y > d.target.y) {
              source_y_modifier = -(this.nodeHeight / 2);
              target_y_modifier = this.nodeHeight / 2;
            } else {
              source_y_modifier = this.nodeHeight / 2;
              target_y_modifier = -(this.nodeHeight / 2);
            }
          } else {
            // they are more side to side than on top of each other
            // y modifier should be in the middle
            source_y_modifier = 0;
            target_y_modifier = 0;
            if (d.source.x > d.target.x) {
              source_x_modifier = -(this.nodeWidth / 2);
              target_x_modifier = this.nodeWidth / 2;
            } else {
              source_x_modifier = this.nodeWidth / 2;
              target_x_modifier = -(this.nodeWidth / 2);
            }
          }

          const x2 = d.target.x + target_x_modifier + this.nodeWidth / 2;

          // Calculate Y2
          if (
            d.target.x - this.nodeWidth < d.source.x &&
            d.source.x < d.target.x + this.nodeWidth
          ) {
            //if nodes are nearly on top of each other
            // x-modifier should be zero
            source_x_modifier = 0;
            target_x_modifier = 0;
            //y modifier is determined besed on placement
            if (d.source.y > d.target.y) {
              source_y_modifier = -(this.nodeHeight / 2);
              target_y_modifier = this.nodeHeight / 2;
            } else {
              source_y_modifier = this.nodeHeight / 2;
              target_y_modifier = -(this.nodeHeight / 2);
            }
          } else {
            // they are more side to side than on top of each other
            // y modifier should be in the middle
            source_y_modifier = 0;
            target_y_modifier = 0;
            if (d.source.x > d.target.x) {
              source_x_modifier = -(this.nodeWidth / 2);
              target_x_modifier = this.nodeWidth / 2;
            } else {
              source_x_modifier = this.nodeWidth / 2;
              target_x_modifier = +(this.nodeWidth / 2);
            }
          }

          const y2 = d.target.y + target_y_modifier + this.nodeHeight / 2;

          return d && 'M ' + x1 + ' ' + y1 + ' L ' + x2 + ' ' + y2;
        });

        edgeLabel.attr('x', (d: any) => d.x).attr('y', (d: any) => d.y);

        // Update the zoom level
        if (this.zoomCanvas)
          this.zoomCanvas.call(
            this.zoom.transform,
            d3.zoomIdentity
              .translate(this.zoomPanX, this.zoomPanY)
              .scale(this.zoomLevel)
          );
      });

      // When a simulation starts alpha is set to 1 and this value slowly decays,
      // based on the alphaDecay rate, until it reaches the alphaTarget of the simulation.
      this.simulation.alpha(1).alphaTarget(0.1).restart();

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderTexts() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-text`).remove();

      // reset zoom
      /*
      if (this.zoomCanvas) this.zoomCanvas
        .call(this.zoom.transform, d3.zoomIdentity.translate(this.zoomPanX, this.zoomPanY).scale(this.zoomLevel));
      */

      // Update all existing items
      //For converting Dates to strings
      const formatTime = d3.timeFormat('%Y-%m-%d %H:%M');

      // Generate the labels of the items first, so they are in back
      this.enterSelections
        .append('text')
        .attr('class', 'item-text')
        .text((d: any) => {
          const sampleText = `${d.code} - ${d.label}`;
          return sampleText.substring(0, 20).toUpperCase() + '...';
        })
        .attr('x', (d: any) => {
          return d.x + 15;
        })
        .attr('y', (d: any) => {
          return d.y + 15;
        })
        .attr('font-family', 'sans-serif')
        .attr('font-size', '11px')
        .attr('fill', 'white');
      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderSymbols() {
    try {
      // Clear first all existing items
      d3.selectAll(`#${this.idName} .item-symbol`).remove();

      // reset zoom
      /*
      if (this.zoomCanvas) this.zoomCanvas
        .call(this.zoom.transform, d3.zoomIdentity.translate(this.zoomPanX, this.zoomPanY).scale(this.zoomLevel));
      */

      // Create a shape path

      const icon = this.enterSelections
        .append('image')
        .attr('class', 'item-symbol item-image')
        .attr('x', (d: any) => {
          return d.x + this.nodeWidth / 2 - this.iconSize / 2;
        })
        .attr('y', (d: any) => {
          return d.y + this.nodeHeight - this.iconSize;
        })
        .attr('width', this.iconSize)
        .attr('height', this.iconSize)
        .attr('href', (d: any) => {
          return d.photo;
        })
        .attr('pointer-events', 'fill')
        // .attr("cursor", "pointer")
        .on('mouseover', (event: any, d: any) => {
          d3.select(event.target).style('cursor', 'pointer');
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px blue)'
          );
        })
        .on('mouseout', (event: any, d: any) => {
          d3.select(event.target).style('filter', null);
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px yellow)'
          );
          this.myComponent.showPopin(d);
          // this.event = null;
        });

      /*
      // dragstarted
      .on("start", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px red)");
        d3.select(event.target)
        .raise() // D3 Raise function display current node at the top
        .classed("active", true);
      })
      // dragged
      .on("drag", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px yellow)");
        d3.select(event.target).raise() // D3 Raise function display current node at the top
        .attr("x", ()=> {
          d.x = event.x; // Todo: Issue bad behavior
          return d.x;
          })    
      })
      // dragended
      .on("end", (event: any, d: any) => {
        d3.select(event.target).classed("active", false);
        d3.select(event.target).style("filter", null);
      });
      */
    } catch (e) {
      this.errorHandler(e);
    }
  }

  zoomActions(event: any, data: any) {
    // function to redraw while zooming
    try {
      // stop the simulation
      this.simulation.stop();
      // Get X axis new scale
      // this.xScale = event.transform.rescaleX(this.xScale);
      // const newX = event.transform.rescaleX(this.xScale);
      // this.xAxis.scale(newX);
      // this.xAxisGroup.call(this.xAxis); // update the axes

      // Update text position
      /*
      this.xAxisGroup.selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", "1em")
        .attr("transform", "rotate(-10)");
      */

      // Get Y axis new scale
      // this.yScale = event.transform.rescaleY(this.yScale)
      // d3.select(`#${ this.idName } tick`).remove();
      d3.select(`#${this.idName}`)
        .selectAll('.x-axis-grid')
        .selectAll('line')
        .remove();
      d3.select(`#${this.idName}`)
        .selectAll('.y-axis-grid')
        .selectAll('line')
        .remove();
      // d3.selectAll('.x-axis > .tick').remove();
      // this.yAxisGroup.call(this.yAxis).selectAll('tick').remove();
      const newX = event.transform.rescaleX(this.xScale);
      this.xAxis.scale(newX);
      this.xAxisGroup.call(this.xAxis); // update the axis

      const newY = event.transform.rescaleY(this.yScale);
      this.yAxis.scale(newY);
      this.yAxisGroup.call(this.yAxis); // update the axis

      // Update the items
      // this.enterSelections.attr('transform', event.transform);
      this.enterSelections
        .selectAll('.item-text')
        .attr('transform', event.transform); // auto zoom and auto pan
      // .attr("x", (d: any) => event.transform.applyX(this.xScale(d.code)) + 20)
      // .attr('x', (d: any) => event.transform.applyX(d.x))
      // .attr('y', (d: any) => event.transform.applyY(d.y + 15));

      this.enterSelections
        .selectAll('.item-rect')
        // .attr('transform', event.transform) // auto zoom and auto pan
        .attr('transform', (d: any) => {
          return (
            'translate(' +
            event.transform.applyX(d.x) +
            ',' +
            event.transform.applyY(d.y) +
            ')'
          );
        })
        // .attr('x', (d: any) => event.transform.applyX(d.x)) // issue with simulation force
        // .attr('y', (d: any) => event.transform.applyY(d.y)) // issue with simulation force
        .attr(
          'width',
          (d: any) =>
            event.transform.applyX(d.x + this.nodeWidth) -
            event.transform.applyX(d.x)
        )
        .attr(
          'height',
          (d: any) =>
            event.transform.applyY(d.y + this.nodeHeight) -
            event.transform.applyY(d.y)
        );
      //.attr("transform", (d: any) => { return "translate(" + (event.transform.applyX(d.x)) + "," + (event.transform.applyY(d.y)) + ")"; })

      // Data dots
      this.enterSelections
        .selectAll('.item-dot-line')
        .attr('cx', (d: any) => {
          return event.transform.applyX(this.xScale(d.x));
        })
        .attr('cy', (d: any) => {
          return event.transform.applyY(this.yScale(d.y));
        });

      this.enterSelections
        .selectAll('.item-image')
        .attr('transform', event.transform); // auto zoom and auto pan
      //.attr('x', (d: any) => event.transform.applyX(d.x + (this.nodeWidth/2) - (this.iconSize/2)))
      // .attr('y', (d: any) => event.transform.applyY(d.y + (this.nodeHeight) - this.iconSize));

      this.itemsGroup
        .selectAll('.item-edge')
        .attr('transform', event.transform); // auto zoom and auto pan
      /*
        .attr("x1", (d: any) => { return event.transform.applyX(d.source.x); })
        .attr("y1", (d: any) => { return event.transform.applyY(d.source.y); })
        .attr("x2", (d: any) => { return event.transform.applyX(d.target.x); })
        .attr("y2", (d: any) => { return event.transform.applyY(d.target.y); });
        */

      this.itemsGroup
        .selectAll(`.${this.idName}-item-edge-label`)
        .data(this.edgesList)
        .enter()
        // .attr('transform', event.transform) // auto zoom and auto pan
        .attr('x', (d: any) => event.transform.applyX(d.x))
        .attr('y', (d: any) => event.transform.applyY(d.y));

      // restart the simulation
      /*
      this.simulation
      .alpha(1)
      .alphaTarget(0.1)
      .restart();
      */
    } catch (e) {
      this.errorHandler(e);
    }
  }

  rescale(type: string, data: any) {
    try {
      switch (type) {
        case 'second':
          this.zoomLevel = 124308;
          break;
        case 'minute':
          this.zoomLevel = 2155;
          break;
        case 'hour':
          this.zoomLevel = 46;
          break;
        case 'day':
          this.zoomLevel = 1.65;
          break;
        case 'week':
          this.zoomLevel = 1.83;
          break;
        case 'month':
          this.zoomLevel = 0.073;
          break;
        case 'year':
          this.zoomLevel = 0.004;
          break;
        default:
          this.zoomLevel = 1;
          break;
      }

      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  clearItems(data: any) {
    try {
      // First Delete extra items
      this.itemsGroup.selectAll('.item').remove();

      // Then rebuild the data map
      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(this.dataSet); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // Update the zoom level
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  errorHandler(error: {
    error: { message: string };
    status: any;
    message: any;
  }) {
    // console.error(error);
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = error.error?.message || error.message || error;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `error Code: ${status}\nMessage: ${
        error.message
      } | more message : ${error.error?.message || error.message || error}`;
    }
    console.debug(error);
    return throwError(errorMessage);
  }
}


```



# D3.js Pie
---

```ts
import { ElementRef, Injectable } from '@angular/core';
import * as d3 from 'd3';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Component } from '@angular/core';
import eventsMock from '../mock/events.json';
import { PieD3ChartComponent } from './pie-d3-chart.component';

@Injectable({
  providedIn: 'root',
})
export class PieD3ChartService {
  private subscriptions: any = [];
  idName: string;
  myComponent!: PieD3ChartComponent;
  isLoading: boolean = false;
  margin: any;
  width: number;
  height: number;
  innerWidth: number;
  innerHeight: number;
  svg: any;
  svgCanvas: any;
  zoomCanvas: any;
  clipPath: any;
  lineLegend: any;
  codesLegend: any;
  colorsLegend: any;
  xScale: any;
  xAxis: any;
  xAxisGrid: any;
  xAxisGroup: any;
  xAxisGridGroup: any;
  yScale: any;
  yAxis: any;
  yAxisGrid: any;
  yAxisGroup: any;
  yAxisGridGroup: any;
  rScale: any;
  rAxis: any;
  rAxisGrid: any;
  rAxisGroup: any;
  rAxisGridGroup: any;
  itemsGroup: any;
  enterSelections: any;
  dataSet: any;
  edgesList: any;
  canvasGroup: any;
  zoom: any;
  nbTicks: number;
  zoomLevel: number;
  zoomPanX: number;
  zoomPanY: number;
  pieWidth: number;
  pieHeight: number;
  pieRadius: number;
  arc: any;
  arc2: any;
  arc3: any;
  arc4: any;

  constructor() {
    // Initialize
    this.margin = {
      top: 40,
      bottom: 100,
      left: 100,
      right: 80,
    };
    this.idName = 'chart';
    this.width = 960;
    this.height = 480;
    this.innerWidth = this.width - this.margin.left - this.margin.right;
    this.innerHeight = this.height - this.margin.top - this.margin.bottom;
    this.nbTicks = 7;
    this.zoomLevel = 1;
    this.zoomPanX = 0;
    this.zoomPanY = 0;
    this.pieWidth = 200;
    this.pieHeight = 100;
    this.pieRadius = Math.min(this.innerWidth, this.innerHeight) / 2 - 50;
    this.arc = d3.arc().innerRadius(0).outerRadius(this.pieRadius);
    this.arc2 = d3
      .arc()
      .innerRadius(this.pieRadius / 2)
      .outerRadius(this.pieRadius);
    this.arc3 = d3
      .arc()
      .innerRadius(this.pieRadius / 2)
      .outerRadius(this.pieRadius * 2);
    this.arc4 = d3
      .arc()
      .innerRadius(this.pieRadius)
      .outerRadius(this.pieRadius * 3);
    this.codesLegend = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.colorsLegend = d3
      .scaleOrdinal()
      .domain(this.codesLegend)
      .range(['yellow', 'blue', 'green', 'orange', 'purple', 'red']);
  }

  createChart(
    chartElement: any,
    data: any,
    width: any = undefined,
    height: any = undefined
  ): void {
    try {
      this.idName = chartElement.id;
      // CLean first
      d3.select(`#${this.idName} svg`).remove();
      this.zoomLevel = 1;
      this.zoomPanX = 0;
      this.zoomPanY = 0;

      if (!data) {
        throw 'No data provided';
        // return;
      }

      // Define responsive scale limits
      if (width != undefined) {
        // Height min size rule of 200px
        width = width > 600 ? width : 500;
        // width = (width > 3000 ) ? 3000 : width;
        this.innerWidth = width - this.margin.left - this.margin.right - 300; // Added a page margin of 300
        // Height min size rule of 200px
        height = height > 400 ? height : 300;
        // height = (height > 700 ) ? 600 : height;
        this.innerHeight = height - this.margin.top - this.margin.bottom;
      }

      // Create SVG Canvas viewport
      this.svg = d3
        .select(chartElement)
        .append('svg')
        .classed('chart', true)
        .attr('width', this.innerWidth + this.margin.left + this.margin.right)
        .attr('height', this.innerHeight + this.margin.top + this.margin.bottom)
        .append('g');

      // Append the zoom group
      this.zoomCanvas = this.svg
        .append('g')
        .append('rect')
        .attr('class', 'background')
        .attr('fill', '#ffffff')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .attr('pointer-events', 'fill')
        .attr('cursor', 'grab');

      // Render the axis
      this.renderXAxis();
      this.renderYAxis();
      this.renderRadiusAxis();

      // Add a clipPath: everything out of this area won't be drawn.
      this.clipPath = this.svg
        .append('defs')
        .append('SVG:clipPath')
        .attr('id', 'clip')
        .append('SVG:rect')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

      // Add the canvas group where items take place
      this.svgCanvas = this.svg
        .append('g')
        .classed('svg-canvas', true)
        .attr('clip-path', 'url(#clip)');

      // Update data
      this.mapData(data);

      // Creating a zoom object

      // max zoom is the ratio of the initial domain extent to the minimum
      // unit that you want to zoom to (1 minute == 1000*60 milliseconds)
      /*
      const minTimeMilli = 20000; // do not allow zooming beyond displaying 20 seconds
      const maxTimeMilli = 6.3072e+11; // approx 20 years

      const minStartTime: any = d3.min(this.dataSet, (d: any) => { return d.dateStart as any; });
      const maxEndTime: any = d3.max(this.dataSet, (d: any) => { return d.dateEnd as any; });

      const widthMilli = maxEndTime.getTime() - minStartTime.getTime();

      console.debug("minStartTime", minStartTime.getTime());
      console.debug("minStartTime", maxEndTime.getTime());
      console.debug("widthMilli", widthMilli);

      const minZoomFactor = widthMilli / maxTimeMilli;
      const maxZoomFactor = widthMilli / minTimeMilli; 
      */
      this.zoom = d3.zoom();
      // .scaleExtent([0.001, 1000]) // Limit the extent of the zoom (ex: from 10 years to 5 min)
      // .scaleExtent([minZoomFactor, maxZoomFactor])
      // .translateExtent([[0, 0], [this.innerWidth, this.innerHeight]])
      // .extent([[0, 0], [this.innerWidth, this.innerHeight]])

      // center via style css
      // this.svg.style("transform-origin", "50% 50% 0");

      // Zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.on('zoom', (event: any) => {
            this.zoomLevel = event.transform.k;
            this.zoomPanX = event.transform.x;
            this.zoomPanY = event.transform.y;
            console.debug('this.zoomLevel', this.zoomLevel);
            console.debug('this.zoomPanX', this.zoomPanX);
            console.debug('this.zoomPanY', this.zoomPanY);
            this.zoomActions(event, data);
          })
        );

      // Render items
      this.renderLegend();
      this.renderEdges();
      this.renderNodes();
      this.renderSymbols();
      this.renderTexts();

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderXAxis() {
    // Create x-axis and scale x
    try {
      // creating a scale with a given range (the output of the scale)
      /* this.xScale = d3.scaleBand().range([0, this.innerWidth])
      .paddingInner(0) // edit the inner padding value in [0,1]
      .paddingOuter(0.5) // edit the outer padding value in [0,1]
      .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right. */
      this.xScale = d3.scaleLinear().range([0, this.innerWidth]);

      // creating an axis with a given orientation, linked to the scale
      this.xAxis = d3.axisBottom(this.xScale);

      // Add X horizontal gridlines
      this.xAxisGrid = this.xAxis.tickSize(-this.innerHeight).tickFormat('');
      this.xAxisGridGroup = this.svg
        .append('g')
        .attr('class', 'x-axis-grid')
        .attr(
          'transform',
          'translate(' +
            this.margin.left +
            ',' +
            this.margin.top +
            this.innerHeight +
            ')'
        )
        .call(this.xAxisGrid);

      // Customize axis
      this.xAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.xAxisGroup = this.svg
        .append('g')
        .classed('x-axis', true)
        .attr(
          'transform',
          `translate(${this.margin.left},${this.margin.top + this.innerHeight})`
        )
        .call(this.xAxis); // calling the axis from the selection

      // Title label for the x axis
      this.svg
        .append('text')
        // .attr("transform", "rotate(-90)")
        .attr('x', this.margin.left + this.innerWidth / 2)
        .attr('y', this.margin.top + this.innerHeight + this.margin.bottom - 15)
        .attr('dx', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Linear axis X');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderYAxis() {
    // Create y-axis and scale y
    try {
      // creating a scale with a given range (the output of the scale)
      /* this.yScale = d3.scaleBand().range([this.innerHeight, 0])
      .paddingInner(0) // edit the inner padding value in [0,1]
      .paddingOuter(0.5) // edit the outer padding value in [0,1]
      .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right. */
      this.yScale = d3.scaleLinear().range([this.innerHeight, 0]);

      // creating an axis with a given orientation, linked to the scale
      this.yAxis = d3.axisLeft(this.yScale);

      // Add Y horizontal gridlines
      this.yAxisGrid = this.yAxis.tickSize(-this.innerWidth).tickFormat('');
      this.yAxisGridGroup = this.svg
        .append('g')
        .attr('class', 'y-axis-grid')
        .attr(
          'transform',
          'translate(' + this.margin.left + ',' + this.margin.top + ')'
        )
        .call(this.yAxisGrid);

      // Customize axis
      this.yAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.yAxisGroup = this.svg
        .append('g')
        .classed('y-axis', true)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .call(this.yAxis); // calling the axis from the selection

      // Title label for the y axis
      this.svg
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 0)
        .attr('x', 0 - this.innerHeight / 2)
        .attr('dy', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Linear axis Y');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderRadiusAxis() {
    // Create y-axis and scale y
    try {
      // creating a scale with a given range (the output of the scale)
      /* this.yScale = d3.scaleBand().range([this.innerHeight, 0])
      .paddingInner(0) // edit the inner padding value in [0,1]
      .paddingOuter(0.5) // edit the outer padding value in [0,1]
      .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right. */
      this.rScale = d3.scaleLinear().range([this.innerHeight, 0]);

      // creating an axis with a given orientation, linked to the scale
      this.rAxis = d3.axisRight(this.rScale);

      // Add Y horizontal gridlines
      /*
      this.rAxisGrid = this.rAxis.tickSize(-(this.innerWidth)).tickFormat('');
      this.rAxisGridGroup = this.svg.append('g')
        .attr('class', 'r-axis-grid')
        .attr('transform', 'translate(' + this.margin.left + this.innerWidth + ',' + this.margin.top + ')')
        .call(this.rAxisGrid);
      */

      // Customize axis
      this.rAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.rAxisGroup = this.svg
        .append('g')
        .classed('r-axis', true)
        .attr(
          'transform',
          `translate(${this.margin.left + this.innerWidth},${this.margin.top})`
        )
        .call(this.rAxis); // calling the axis from the selection

      // Title label for the r axis
      this.svg
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', this.margin.left + this.innerWidth + this.margin.right - 30)
        .attr('x', 0 - this.innerHeight / 2)
        .attr('dy', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Linear axis R');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  mapData(data: any) {
    try {
      // Add items
      this.itemsGroup = this.svgCanvas
        .append('g')
        .classed('items', true)
        // Align top left
        // .attr('transform', `translate(${this.margin.left},${this.margin.top})`);
        // Align center
        .attr(
          'transform',
          'translate(' +
            (this.innerWidth + this.margin.left + this.margin.right) / 2 +
            ',' +
            (this.innerHeight + this.margin.top + this.margin.bottom) / 2 +
            ')'
        );

      // Mapping
      // For converting strings to Dates
      const parseTime = d3.timeParse('%Y-%m-%dT%H:%M:%S%Z');
      // const strictIsoParse = d3.utcParse("%Y-%m-%dT%H:%M:%S%Z");
      this.dataSet = data
        .map((d: any) => {
          // const currDate = new Date(d.startDate);
          const dateObjStart = parseTime(d.startDate);
          const dateObjEnd = parseTime(d.endDate);

          // Return the custom data structure for d3js
          return {
            id: d.id,
            code: d.code,
            label: d.label,
            photo: d.photo,
            dateStart: dateObjStart,
            dateEnd: dateObjEnd,
            amount: parseFloat(`${d.duration}`),
            x: parseInt(`${d.x}`, 10),
            y: parseInt(`${d.y}`, 10),
          };
        })
        .reverse(); // reverse array order to get a first default section in y axis

      this.edgesList = eventsMock.edges.map((d: any) => {
        // Return the custom edge data structure for d3js
        return {
          id: d.id,
          source: this.dataSet.find((item: any) => item.id === d.source),
          target: this.dataSet.find((item: any) => item.id === d.target),
          tag: d.tag,
        };
      });

      // Update our scales and axes

      // this.xScale.domain(d3.extent(this.dataSet, (d: any) => { return d.code;}));
      /*
      this.xScale.domain([
        d3.min(this.dataSet, (d: any) => { return d.dateStart as any; }),
        d3.max(this.dataSet, (d: any) => { return d.dateEnd as any; })
      ]);
      */

      // Fix style format issues with zoom effects
      // this.xScale.tickFormat(d3.timeParse("%Y-%m-%dT%H:%M:%S%Z"));
      /*
      this.xAxisGroup.call(this.xAxis)
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", "1em")
        .attr("transform", "rotate(-10)");
      */

      /*
      this.yScale.domain([
        d3.min(this.dataSet, (d: any)=> { return ((d.y as any)); }),
        d3.max(this.dataSet, (d: any)=> { return d.y as any; })
      ])
      */

      this.xScale.domain([
        (d3.min(this.dataSet, (d: any) => {
          return d.x as any;
        }) as any) - this.pieWidth,
        (d3.max(this.dataSet, (d: any) => {
          return d.x as any;
        }) as any) + this.pieWidth,
      ]);

      this.xAxisGroup
        .call(this.xAxis)
        .selectAll('text')
        .attr('x', (d: any) => this.xScale(d.x));

      // this.yScale.domain(this.dataSet.map((d: any) => d.amount));
      this.yScale.domain([
        (d3.min(this.dataSet, (d: any) => {
          return d.y as any;
        }) as any) - this.pieHeight,
        (d3.max(this.dataSet, (d: any) => {
          return d.y as any;
        }) as any) + this.pieHeight,
      ]);

      this.yAxisGroup
        .call(this.yAxis)
        .selectAll('text')
        .attr('y', (d: any) => this.yScale(d.y));

      // this.yScale.domain(this.dataSet.map((d: any) => d.amount));
      this.rScale.domain([
        (d3.min(this.dataSet, (d: any) => {
          return d.amount as any;
        }) as any) - this.pieHeight,
        (d3.max(this.dataSet, (d: any) => {
          return d.amount as any;
        }) as any) + this.pieHeight,
      ]);

      this.rAxisGroup
        .call(this.rAxis)
        .selectAll('text')
        .attr('y', (d: any) => this.rScale(d.amount));

      // Compute the position of each group on the pie:
      const pie = d3.pie<any>().value((d: any) => Number(d.amount));

      // Add the colors system
      /*
      this.colorsLegend = d3.scaleOrdinal()
      .domain(this.dataSet.map((d:any) => d.amount.toString()))
      .range(["#c7d3ec", "#a5b8db", "#879cc4", "#677795", "#5a6782"]);
      */

      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(pie(this.dataSet)); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderLegend() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-legend`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items

      // this.lineLegend = this.svg.selectAll(".lineLegend").data(this.dataSet).enter().append("g")
      this.lineLegend = this.itemsGroup
        .selectAll('.lineLegend')
        .data(this.dataSet)
        .enter()
        .append('g')
        .attr('class', 'item-legend')
        .attr('transform', (d: any, i: any) => {
          return (
            'translate(' +
            this.innerWidth / 3 +
            ',' +
            (i * 20 - this.innerHeight / 2 - 10) +
            ')'
          );
        });

      this.lineLegend
        .append('rect')
        .attr('fill', (d: any, i: any) => {
          return this.colorsLegend(d.amount);
        })
        .attr('stroke', 'white')
        .attr('stroke-width', 1.5)
        .attr('width', 10)
        .attr('height', 10);

      this.lineLegend
        .append('text')
        // .attr("dx", "1em")
        // .style("text-anchor", "middle")
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', 'black')
        .text((d: any) => {
          return d.code;
        })
        .attr('transform', 'translate(15,9)'); //align texts with boxes

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderNodes() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-bar`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items

      // Build the pie chart
      this.enterSelections
        .append('path')
        .attr('class', 'item-bar item-rect')
        .attr('d', this.arc)
        .attr(
          'transform',
          'translate(' + this.width / 2 + ',' + this.height / 2 + ')'
        )
        .attr('x', this.width / 2)
        .attr('y', this.height / 2)
        .attr('fill', (d: any, i: any) => this.colorsLegend(i))
        .attr('stroke', '#121926')
        .style('stroke-width', '1px')
        .attr('opacity', '0.6')
        .attr('pointer-events', 'fill')
        .attr('cursor', 'pointer')
        .on('mouseover', (event: any, d: any) => {
          console.debug(d);
          console.debug(event);
          console.debug(d3.pointer(event));
          d3.select(event.target).attr('fill', 'green');
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).attr('fill', 'purple');
        });

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderTexts() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-text`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items
      //For converting Dates to strings
      const formatTime = d3.timeFormat('%Y-%m-%d %H:%M');

      // Generate the labels of the items first, so they are in back

      // Compute the position of each group on the pie:
      // const pie = d3.pie<any>().value((d: any) => Number(d.amount));
      /*
      this.itemsGroup
      .selectAll('.item-text')
      .data(pie(this.dataSet)) // bind data
      .enter() // Add a corresponding number of placeholders for the data
      */
      this.enterSelections
        .append('text')
        .attr('class', 'item-text')
        .text((d: any) => {
          const sampleText = `${d.data.code} - ${d.data.label}`;
          return sampleText.substring(0, 20).toUpperCase() + '...';
        })
        .attr(
          'transform',
          (d: any) => 'translate(' + this.arc3.centroid(d) + ')'
        )
        .style('text-anchor', 'middle')
        /*
        .attr("x", (d: any) => {
          return this.xScale(d.x) - this.pieWidth / 2 + 15;
        })
        .attr("y", (d: any) => {
          return this.yScale(d.y) - this.pieHeight / 2 + 15;
        })
        */
        .attr('font-family', 'sans-serif')
        .attr('font-size', '11px')
        .attr('fill', 'black');
      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderSymbols() {
    try {
      // Clear first all existing items
      d3.selectAll(`#${this.idName} .item-symbol`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Create a shape path
      const iconSize = 30;
      const icon = this.enterSelections
        .append('image')
        .attr('class', 'item-symbol item-image')
        .attr(
          'transform',
          (d: any) => 'translate(' + this.arc.centroid(d) + ')'
        )
        .attr('xlink:href', (d: any) => {
          return d.data.photo;
        })
        .attr('x', (-1 * iconSize) / 2)
        .attr('y', (-1 * iconSize) / 2)
        /*
        .attr("x", (d: any) => {
          return this.xScale(d.x) - (iconSize) + this.pieWidth / 2 - 5;
        })
        .attr("y", (d: any) => {
          return this.yScale(d.y) + (iconSize / 2) - this.pieHeight / 2 - 5;
        })
        */
        .attr('width', iconSize)
        .attr('height', iconSize)
        /*
        .attr("href", (d: any) => {
          return d.photo;
        })
        */
        .attr('pointer-events', 'fill')
        // .attr("cursor", "pointer")
        .on('mouseover', (event: any, d: any) => {
          d3.select(event.target).style('cursor', 'pointer');
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px blue)'
          );
        })
        .on('mouseout', (event: any, d: any) => {
          d3.select(event.target).style('filter', null);
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px yellow)'
          );
          this.myComponent.showPopin(d.data);
          // this.event = null;
        });

      /*
      // dragstarted
      .on("start", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px red)");
        d3.select(event.target)
        .raise() // D3 Raise function display current map at the top
        .classed("active", true);
      })
      // dragged
      .on("drag", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px yellow)");
        d3.select(event.target).raise() // D3 Raise function display current map at the top
        .attr("x", ()=> {
          d.x = event.x; // Todo: Issue bad behavior
          return d.x;
          })    
      })
      // dragended
      .on("end", (event: any, d: any) => {
        d3.select(event.target).classed("active", false);
        d3.select(event.target).style("filter", null);
      });
      */
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderEdges() {
    try {
      console.debug('render edges', this.edgesList);

      // Compute the position of each group on the pie:
      const pie = d3.pie<any>().value((d: any) => Number(d.amount));

      //Draw connection
      const edgesLine = this.enterSelections
        .append('path') // Generate a polyline on the placeholder (draw with path)
        .attr('class', 'item-edge')
        //.attr("transform", (d: any) => "translate(" + this.arc.centroid(d) + ")")
        .attr('d', (d: any, i: any) => {
          const source = `M ${this.arc.centroid(d)}`;
          const targetX = this.innerWidth / 3;
          const targetY = i * 20 - this.innerHeight / 2 - 10;
          const marginSource = ` H ${targetX - i * 20 - 50} `;
          const marginTarget = ` V ${targetY + 5} `;
          const target = `H ${targetX - 5}`;
          // const target2 = `L ${this.xScale(d.target.x)} ${this.yScale(d.target.y) + 5}`;
          return `${source + marginSource + marginTarget + target}`;
          //return `${source + target2}`;
        }) //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
        //.attr('d', (d: any) => { return d && 'M ' + d.source.x + ' ' + d.source.y + ' L ' + d.target.x + ' ' + d.target.y; }) //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
        .attr('id', (d: any, i: any) => {
          return `${this.idName}-edgepath-${i}`;
        }) // Set id, used for connecting text
        .attr('marker-start', 'url(#joinStart)') // Mark the arrow according to the id number marked by the arrow
        .attr('marker-mid', 'url(#joinMid)') // Mark the arrow according to the id number marked by the arrow
        .attr('marker-end', 'url(#joinEnd)') // Mark the arrow according to the id number marked by the arrow
        .style('stroke', 'black') // colour
        .style('fill', 'none') // colour
        .style('stroke-width', 2); // Thickness

      //Connection name
      const edgeLabel = this.enterSelections
        .append('text') // Create a text area for each connection
        .attr('class', `${this.idName}-item-edge-label`)
        //.attr('dx', 80)
        .attr('dy', 15);
      // .style("text-anchor", "middle")
      //.attr("transform", "rotate(-60)");

      edgeLabel
        .append('textPath')
        .style('pointer-events', 'none') // Disable mouse events
        .attr('font-family', 'sans-serif')
        .attr('font-size', '12px')
        .attr('fill', 'black')
        .attr('startOffset', '50%')
        .attr('xlink:href', (d: any, i: any) => {
          return `#${this.idName}-edgepath-${i}`;
        }) // The text is arranged on the line corresponding to the id
        .text((d: any) => {
          return d.data.label;
        }); // Set text content

      //Draw the arrow on the line
      const defs = this.itemsGroup.append('defs'); // defs defines reusable elements
      const joinEnd = defs
        .append('marker') // Create arrow
        .attr('id', 'joinEnd')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinEnd')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-5 -5 10 10') // viewBox
        .attr('refX', 6) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinEnd
        .append('path')
        .attr('d', 'M 0,0 m -5,-5 L 5,0 L -5,5 Z') // d: path description, Bezier curve
        .attr('fill', 'red'); // fill color

      const joinStart = defs
        .append('marker') // Create arrow
        .attr('id', 'joinStart')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinStart')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-6 -6 12 12') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinStart
        .append('path')
        .attr('d', 'M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0') // Circle d: path description, Bezier curve
        .attr('fill', 'yellow'); // fill color

      const joinMid = defs
        .append('marker') // Create arrow
        .attr('id', 'joinMid')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinMid')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-6 -6 12 12') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinMid
        .append('path')
        .attr('d', 'M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0') // Circle d: path description, Bezier curve
        .attr('fill', '#eee'); // fill color
    } catch (e) {
      this.errorHandler(e);
    }
  }

  zoomActions(event: any, data: any) {
    // function to redraw while zooming
    try {
      // Get X axis new scale
      // this.xScale = event.transform.rescaleX(this.xScale);
      // const newX = event.transform.rescaleX(this.xScale);
      // this.xAxis.scale(newX);
      // this.xAxisGroup.call(this.xAxis); // update the axes

      // Update text position
      /*
      this.xAxisGroup.selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", "1em")
        .attr("transform", "rotate(-10)");
      */

      // Get Y axis new scale
      // this.yScale = event.transform.rescaleY(this.yScale)
      // d3.select(`#${ this.idName } tick`).remove();
      d3.select(`#${this.idName}`)
        .selectAll('.x-axis-grid')
        .selectAll('line')
        .remove();
      d3.select(`#${this.idName}`)
        .selectAll('.y-axis-grid')
        .selectAll('line')
        .remove();
      // d3.selectAll('.x-axis > .tick').remove();
      // this.yAxisGroup.call(this.yAxis).selectAll('tick').remove();
      const newX = event.transform.rescaleX(this.xScale);
      this.xAxis.scale(newX);
      this.xAxisGroup.call(this.xAxis); // update the axis

      const newY = event.transform.rescaleY(this.yScale);
      this.yAxis.scale(newY);
      this.yAxisGroup.call(this.yAxis); // update the axis

      // Update the items
      // this.enterSelections.attr('transform', event.transform);

      this.enterSelections
        .selectAll('.item-text')
        //.attr('transform', event.transform) // auto zoom and auto pan
        //.attr("transform", "translate(" + event.translate + ")scale(" + event.scale + ")");
        .attr(
          'transform',
          (d: any) =>
            'translate(' + event.transform.apply(this.arc3.centroid(d)) + ')'
        );
      // .attr("x", (d: any) => event.transform.applyX(this.xScale(d.code)) + 20)
      // .attr('x', (d: any) => event.transform.applyX(this.xScale(d.x) - this.pieWidth / 2 + 15))
      // .attr('y', (d: any) => event.transform.applyY(this.yScale(d.y) - this.pieHeight / 2 + 15));

      this.enterSelections
        .selectAll('.item-rect')
        .attr('transform', event.transform); // auto zoom and auto pan
      //.attr('width', (d: any) => { return (event.transform.applyX(this.xScale(d.code)) ) })
      // .attr('width', (d: any) => event.transform.applyX(this.xScale(d.x)))
      // .attr('height', (d: any) => event.transform.applyY(this.yScale(d.y)))
      //.attr('height', (d: any) => { return (event.transform.applyY(this.yScale(d.y)) ) })
      //.attr('x', (d: any) => event.transform.applyX(this.xScale(d.code)))
      //.attr('x', (d: any) => event.transform.applyX(this.xScale(d.x)))
      //.attr('y', (d: any) => event.transform.applyY(this.yScale(d.y)));

      // Data dots
      this.enterSelections
        .selectAll('.item-dot-line')
        .attr('cx', (d: any) => {
          return event.transform.applyX(this.xScale(d.x));
        })
        .attr('cy', (d: any) => {
          return event.transform.applyY(this.yScale(d.y));
        });

      this.enterSelections
        .selectAll('.item-image')
        // .attr('transform', event.transform) // auto zoom and auto pan
        .attr(
          'transform',
          (d: any) =>
            'translate(' + event.transform.apply(this.arc2.centroid(d)) + ')'
        );
      // .attr('x', (d: any) => event.transform.applyX(this.xScale(d.x) + 50))
      // .attr('y', (d: any) => event.transform.applyY(this.yScale(d.y) + 50));

      this.itemsGroup.selectAll('.item-edge').attr('d', (d: any, i: any) => {
        const source = `M ${event.transform.apply(this.arc.centroid(d))}`;
        const targetX = this.innerWidth / 3;
        const targetY = i * 20 - this.innerHeight / 2 - 10;
        const marginSource = ` H ${targetX - i * 20 - 50} `;
        const marginTarget = ` V ${targetY + 5} `;
        const target = `H ${targetX - 5}`;
        // const target2 = `L ${this.xScale(d.target.x)} ${this.yScale(d.target.y) + 5}`;
        return `${source + marginSource + marginTarget + target}`;
        //return `${source + target2}`;
      }); //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
    } catch (e) {
      this.errorHandler(e);
    }
  }

  rescale(type: string, data: any) {
    try {
      switch (type) {
        case 'second':
          this.zoomLevel = 124308;
          break;
        case 'minute':
          this.zoomLevel = 2155;
          break;
        case 'hour':
          this.zoomLevel = 46;
          break;
        case 'day':
          this.zoomLevel = 1.65;
          break;
        case 'week':
          this.zoomLevel = 1.83;
          break;
        case 'month':
          this.zoomLevel = 0.073;
          break;
        case 'year':
          this.zoomLevel = 0.004;
          break;
        default:
          this.zoomLevel = 1;
          break;
      }

      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  clearItems(data: any) {
    try {
      // First Delete extra items
      this.itemsGroup.selectAll('.item').remove();

      // Then rebuild the data map
      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(this.dataSet); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // Update the zoom level
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  errorHandler(error: {
    error: { message: string };
    status: any;
    message: any;
  }) {
    // console.error(error);
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = error.error?.message || error.message || error;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `error Code: ${status}\nMessage: ${
        error.message
      } | more message : ${error.error?.message || error.message || error}`;
    }
    console.debug(error);
    return throwError(errorMessage);
  }
}


```



# D3.js Timeline
---

```ts

import { ElementRef, Injectable } from '@angular/core';
import * as d3 from 'd3';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Component } from '@angular/core';
import eventsMock from '../mock/events.json';
import { TimelineD3ChartComponent } from './timeline-d3-chart.component';

@Injectable({
  providedIn: 'root',
})
export class TimelineD3ChartService {
  private subscriptions: any = [];
  idName: string;
  myComponent!: TimelineD3ChartComponent;
  isLoading: boolean = false;
  margin: any;
  width: number;
  height: number;
  innerWidth: number;
  innerHeight: number;
  svg: any;
  svgCanvas: any;
  zoomCanvas: any;
  clipPath: any;
  lineLegend: any;
  codesLegend: any;
  colorsLegend: any;
  xScale: any;
  xAxis: any;
  xAxis2: any;
  xAxisGrid: any;
  xAxisGroup: any;
  xAxisGroup2: any;
  xAxisGridGroup: any;
  yScale: any;
  yAxis: any;
  yAxisGrid: any;
  yAxisGroup: any;
  yAxisGridGroup: any;
  itemsGroup: any;
  enterSelections: any;
  dataSet: any;
  edgesList: any;
  canvasGroup: any;
  zoom: any;
  nbTicks: number;
  zoomLevel: number;
  zoomPanX: number;
  zoomPanY: number;

  constructor() {
    // Initialize
    this.margin = {
      top: 40,
      bottom: 100,
      left: 100,
      right: 40,
    };
    this.idName = 'chart';
    this.width = 960;
    this.height = 480;
    this.innerWidth = this.width - this.margin.left - this.margin.right;
    this.innerHeight = this.height - this.margin.top - this.margin.bottom;
    this.nbTicks = 7;
    this.zoomLevel = 1;
    this.zoomPanX = 0;
    this.zoomPanY = 0;
    this.codesLegend = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.colorsLegend = d3
      .scaleOrdinal()
      .domain(this.codesLegend)
      .range(['yellow', 'blue', 'green', 'orange', 'purple', 'red']);
  }

  createChart(
    chartElement: any,
    data: any,
    width: any = undefined,
    height: any = undefined
  ): void {
    try {
      this.idName = chartElement.id;
      // CLean first
      d3.select(`#${this.idName} svg`).remove();
      this.zoomLevel = 1;
      this.zoomPanX = 0;
      this.zoomPanY = 0;

      if (!data) {
        throw 'No data provided';
        // return;
      }

      // Define responsive scale limits
      if (width != undefined) {
        // Height min size rule of 200px
        width = width > 600 ? width : 500;
        // width = (width > 3000 ) ? 3000 : width;
        this.innerWidth = width - this.margin.left - this.margin.right - 300; // Added a page margin of 300
        // Height min size rule of 200px
        height = height > 400 ? height : 300;
        // height = (height > 700 ) ? 600 : height;
        this.innerHeight = height - this.margin.top - this.margin.bottom;
      }

      // Create SVG Canvas viewport
      this.svg = d3
        .select(chartElement)
        .append('svg')
        .classed('chart', true)
        .attr('width', this.innerWidth + this.margin.left + this.margin.right)
        .attr('height', this.innerHeight + this.margin.top + this.margin.bottom)
        .append('g');

      // Append the zoom group
      this.zoomCanvas = this.svg
        .append('g')
        .append('rect')
        .attr('class', 'background')
        .attr('fill', '#efefef')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .attr('pointer-events', 'fill')
        .attr('cursor', 'grab');

      // Render the axis
      this.renderXAxis();
      this.renderYAxis();

      // Add a clipPath: everything out of this area won't be drawn.
      this.clipPath = this.svg
        .append('defs')
        .append('SVG:clipPath')
        .attr('id', 'clip')
        .append('SVG:rect')
        .attr('width', this.innerWidth)
        .attr('height', this.innerHeight)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

      // Add the canvas group where items take place
      this.svgCanvas = this.svg
        .append('g')
        .classed('svg-canvas', true)
        .attr('clip-path', 'url(#clip)');

      // Update data
      this.mapData(data);

      // Creating a zoom object

      // max zoom is the ratio of the initial domain extent to the minimum
      // unit that you want to zoom to (1 minute == 1000*60 milliseconds)
      /*
      const minTimeMilli = 20000; // do not allow zooming beyond displaying 20 seconds
      const maxTimeMilli = 6.3072e+11; // approx 20 years

      const minStartTime: any = d3.min(this.dataSet, (d: any) => { return d.dateStart as any; });
      const maxEndTime: any = d3.max(this.dataSet, (d: any) => { return d.dateEnd as any; });

      const widthMilli = maxEndTime.getTime() - minStartTime.getTime();

      console.debug("minStartTime", minStartTime.getTime());
      console.debug("minStartTime", maxEndTime.getTime());
      console.debug("widthMilli", widthMilli);

      const minZoomFactor = widthMilli / maxTimeMilli;
      const maxZoomFactor = widthMilli / minTimeMilli; 
      */
      this.zoom = d3.zoom();
      // .scaleExtent([0.001, 1000]) // Limit the extent of the zoom (ex: from 10 years to 5 min)
      // .scaleExtent([minZoomFactor, maxZoomFactor])
      // .translateExtent([[0, 0], [this.innerWidth, this.innerHeight]])
      // .extent([[0, 0], [this.innerWidth, this.innerHeight]])

      // center via style css
      // this.svg.style("transform-origin", "50% 50% 0");

      // Zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.on('zoom', (event: any) => {
            this.zoomLevel = event.transform.k;
            this.zoomPanX = event.transform.x;
            this.zoomPanY = event.transform.y;
            console.debug('this.zoomLevel', this.zoomLevel);
            console.debug('this.zoomPanX', this.zoomPanX);
            console.debug('this.zoomPanY', this.zoomPanY);
            this.zoomActions(event, data);
          })
        );

      // Render items
      this.renderLegend();
      this.renderBars();
      this.renderSymbols();
      this.renderTexts();
      this.renderEdges();

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderXAxis() {
    // Create x-axis and scale x
    try {
      // creating a scale with a given range (the output of the scale)
      this.xScale = d3.scaleTime().range([0, this.innerWidth]);

      // creating an axis with a given orientation, linked to the scale
      this.xAxis = d3.axisBottom(this.xScale);
      this.xAxis2 = d3.axisTop(this.xScale);

      // Add X vertical gridlines
      // const x = d3.scaleTime().range([0, this.innerWidth]);
      this.xAxisGrid = this.xAxis2.ticks(14).tickSize(-this.innerHeight);
      // .tickFormat('');
      this.xAxisGridGroup = this.svg
        .append('g')
        .classed('x-axis-grid', false)
        .attr(
          'transform',
          'translate(' +
            this.margin.left +
            ',' +
            this.margin.top +
            this.innerHeight +
            ')'
        )
        .call(this.xAxisGrid);

      // Customize axis
      this.xAxis
        .tickFormat(d3.timeFormat('%Y-%m-%d %H:%M')) // Format your ticks (not apply to domain axis or points axis)
        .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      this.xAxis2.tickFormat(d3.timeFormat('%H:%M:%S')); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks);

      // creating a group to render the axis
      this.xAxisGroup = this.svg
        .append('g')
        .classed('x-axis', true)
        .attr(
          'transform',
          `translate(${this.margin.left},${this.margin.top + this.innerHeight})`
        )
        .call(this.xAxis); // calling the axis from the selection

      this.xAxisGroup2 = this.svg
        .append('g')
        .classed('x-axis', true)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .call(this.xAxis2); // calling the axis from the selection

      // Title label for the x axis
      this.svg
        .append('text')
        .attr(
          'transform',
          `translate(${this.innerWidth / 2},${
            this.innerHeight +
            this.margin.top +
            this.margin.bottom -
            this.margin.bottom / 3
          })`
        )
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Date & Time axis');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderYAxis() {
    // Create y-axis and scale y
    try {
      // creating a scale with a given range (the output of the scale)
      // this.yScale = d3.scaleLinear().range([this.innerHeight - this.paddingVert, this.paddingVert]);
      this.yScale = d3
        .scaleBand()
        .range([this.innerHeight, 0])
        .paddingInner(0) // edit the inner padding value in [0,1]
        .paddingOuter(0.5) // edit the outer padding value in [0,1]
        .align(1); // edit the align: 0 is aligned left, 0.5 centered, 1 aligned right.

      // creating an axis with a given orientation, linked to the scale
      this.yAxis = d3.axisLeft(this.yScale);

      // Add Y horizontal gridlines
      this.yAxisGrid = this.yAxis.tickSize(-this.innerWidth).tickFormat('');
      this.yAxisGridGroup = this.svg
        .append('g')
        .attr('class', 'y-axis-grid')
        .attr(
          'transform',
          'translate(' + this.margin.left + ',' + this.margin.top + ')'
        )
        .call(this.yAxisGrid);

      // Customize axis
      this.yAxis.tickFormat(null); // Format your ticks (not apply to domain axis or points axis)
      // .ticks(this.nbTicks); // To create space beetween tick texts (not working on datetime zoomable axis)

      // creating a group to render the axis
      this.yAxisGroup = this.svg
        .append('g')
        .classed('y-axis', true)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`)
        .call(this.yAxis); // calling the axis from the selection

      // Title label for the y axis
      this.svg
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 0)
        .attr('x', 0 - this.innerHeight / 2)
        .attr('dy', '1em')
        .style('text-anchor', 'middle')
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', '#aaa')
        .text('Domain axis');

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  mapData(data: any) {
    try {
      // Add items
      this.itemsGroup = this.svgCanvas
        .append('g')
        .classed('items', true)
        .attr('transform', `translate(${this.margin.left},${this.margin.top})`);

      // Mapping
      // For converting strings to Dates
      const parseTime = d3.timeParse('%Y-%m-%dT%H:%M:%S%Z');
      // const strictIsoParse = d3.utcParse("%Y-%m-%dT%H:%M:%S%Z");
      this.dataSet = data
        .map((d: any) => {
          // const currDate = new Date(d.startDate);
          const dateObjStart = parseTime(d.startDate);
          const dateObjEnd = parseTime(d.endDate);

          // Return the custom data structure for d3js
          return {
            id: d.id,
            label: d.label,
            photo: d.photo,
            dateStart: dateObjStart,
            dateEnd: dateObjEnd,
            amount: parseInt(`${d.duration}`, 10),
          };
        })
        .reverse(); // reverse array order to get a first default section in y axis

      this.edgesList = eventsMock.edges.map((d: any) => {
        // Return the custom edge data structure for d3js
        return {
          id: d.id,
          source: this.dataSet.find((item: any) => item.id === d.source),
          target: this.dataSet.find((item: any) => item.id === d.target),
          tag: d.tag,
        };
      });

      // Update our scales and axes

      // this.xScale.domain(d3.extent(this.dataSet, (d: any) => { return d.dateStart as Date;}));
      this.xScale.domain([
        d3.min(this.dataSet, (d: any) => {
          return d.dateStart as any;
        }),
        d3.max(this.dataSet, (d: any) => {
          return d.dateEnd as any;
        }),
      ]);

      // Fix style format issues with zoom effects
      // this.xScale.tickFormat(d3.timeParse("%Y-%m-%dT%H:%M:%S%Z"));
      this.xAxisGroup
        .call(this.xAxis)
        .selectAll('text')
        .style('text-anchor', 'end')
        .attr('dx', '-.8em')
        .attr('dy', '1em')
        .attr('transform', 'rotate(-10)');

      this.xAxisGroup2.call(this.xAxis2).selectAll('text').attr('dy', '-0.3em');

      /*
      this.yScale.domain([
        d3.min(this.dataSet, (d: any)=> { return ((d.amount as any)); }),
        d3.max(this.dataSet, (d: any)=> { return d.amount as any; })
      ])
      */

      this.yScale.domain(this.dataSet.map((d: any) => d.label));

      this.yAxisGroup
        .call(this.yAxis)
        .selectAll('text')
        .attr('y', -(this.yScale.bandwidth() / 2));

      // Background y axis
      this.yAxisGroup
        .call(this.yAxis)
        .selectAll('.y-axis > .tick')
        .data(this.dataSet)
        .append('rect')
        .attr('x', -this.margin.left)
        .attr('y', -this.yScale.bandwidth())
        .attr('width', this.innerWidth + this.margin.left)
        .attr('height', this.yScale.bandwidth())
        .attr('stroke', '#B8B9BA')
        .attr('fill', (d: any) => {
          if (d.label == 'fake label') {
            return '#B8B9BA';
          } else {
            return '#F2F2F2';
          }
        })
        .attr('opacity', (d: any) => {
          if (d.label == 'fake label') {
            return '0.5';
          } else {
            return '0.6';
          }
        })
        .attr('pointer-events', 'none'); // disable event mouses for enable zoom tool

      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(this.dataSet); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderLegend() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-legend`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items

      // this.lineLegend = this.svg.selectAll(".lineLegend").data(this.dataSet).enter().append("g")
      this.lineLegend = this.enterSelections
        .append('g')
        .attr('class', 'item-legend item-legend')
        .attr('transform', (d: any, i: any) => {
          return 'translate(10,' + (i * 20 + 10) + ')';
        });

      this.lineLegend
        .append('rect')
        .attr('fill', (d: any, i: any) => {
          return this.colorsLegend(d.amount);
        })
        .attr('stroke', 'white')
        .attr('stroke-width', 1.5)
        .attr('width', 10)
        .attr('height', 10);

      this.lineLegend
        .append('text')
        // .attr("dx", "1em")
        // .style("text-anchor", "middle")
        .attr('font-family', 'sans-serif')
        .attr('font-size', '16px')
        .attr('fill', 'black')
        .text((d: any) => {
          return d.label;
        })
        .attr('transform', 'translate(15,9)'); //align texts with boxes

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderBars() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-bar`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items

      // this.enterSelections.merge(dataBound as any).attr('transform', (d, i) => `translate(${xScale(d.date as Date)},${yScale(d.amount)})`);
      this.enterSelections
        .append('rect')
        .attr('class', 'item-bar item-rect')
        .attr('x', (d: any) => this.xScale(d.dateStart as Date))
        .attr('y', (d: any) => this.yScale(d.label) - 16)
        .attr('width', (d: any) => {
          return this.xScale(d.dateEnd) - this.xScale(d.dateStart);
        })
        // .attr('height', this.yScale.bandwidth())
        .attr('height', 20)
        .attr('fill', (d: any) => {
          return this.colorsLegend(d.amount);
        })
        .attr('pointer-events', 'fill')
        .attr('cursor', 'pointer')
        .on('mouseover', (event: any, d: any) => {
          console.debug(d);
          console.debug(event);
          console.debug(d3.pointer(event));
          d3.select(event.target).attr('fill', 'green');
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).attr('fill', 'purple');
        });
      // Create a shape path
      this.enterSelections
        .append('path')
        .attr('class', 'item-bar item-path-left')
        .attr('d', 'M0 0L9 10L0 20V0Z')
        .attr('fill', '#F2F2F2')
        .style('height', '20')
        .attr(
          'transform',
          (d: any) =>
            `translate(${this.xScale(d.dateStart as Date)},${
              this.yScale(d.label) - 16
            }) scale(1) `
        );

      // Create a shape path
      this.enterSelections
        .append('path')
        .attr('class', 'item-bar item-path-right')
        .attr('d', 'M3.94118 1C3.16421 1 3 1 3 1L11 10L3 19H3.94118')
        .attr('stroke', '#D31F10')
        .attr('stroke-width', '2')
        .attr('fill', 'none')
        .style('height', '20')
        .attr(
          'transform',
          (d: any) =>
            `translate(${this.xScale(d.dateEnd as Date)},${
              this.yScale(d.label) - 16
            }) scale(1) `
        );

      // Create a shape path
      this.enterSelections
        .append('path')
        .attr('class', 'item-bar item-path-right')
        .attr('d', 'M0 0L9 10L0 20V0Z')
        .attr('fill', (d: any) => {
          return this.colorsLegend(d.amount);
        })
        .style('height', '20')
        .attr(
          'transform',
          (d: any) =>
            `translate(${this.xScale(d.dateEnd as Date)},${
              this.yScale(d.label) - 16
            }) scale(1) `
        );

      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderTexts() {
    try {
      // Clear existing items first
      d3.selectAll(`#${this.idName} .item-text`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Update all existing items
      //For converting Dates to strings
      const formatTime = d3.timeFormat('%Y-%m-%d %H:%M');

      // Generate the labels of the items first, so they are in back
      this.enterSelections
        .append('text')
        .attr('class', 'item-text')
        .text((d: any) => {
          const sampleText = `${d.label} ${formatTime(
            d.dateStart as Date
          )} --> ${formatTime(d.dateEnd as Date)}`;
          return sampleText.substring(0, 20).toUpperCase() + '...';
        })
        .attr('x', (d: any) => {
          return this.xScale(d.dateStart as Date) + 20;
        })
        .attr('y', (d: any) => {
          return this.yScale(d.label);
        })
        .attr('font-family', 'sans-serif')
        .attr('font-size', '11px')
        .attr('fill', 'white');
      // throw "myException";
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderSymbols() {
    try {
      // Clear first all existing items
      d3.selectAll(`#${this.idName} .item-symbol`).remove();

      // reset zoom
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );

      // Create a shape path

      this.enterSelections
        .append('path')
        .attr('class', 'item-symbol item-symbol-left1')
        .attr(
          'd',
          'M5.99999 6.0013C7.47332 6.0013 8.66666 4.80464 8.66666 3.33464C8.66666 1.8613 7.47332 0.667969 5.99999 0.667969C4.52666 0.667969 3.33332 1.8613 3.33332 3.33464C3.33332 4.80464 4.52666 6.0013 5.99999 6.0013ZM5.99999 7.33463C4.22332 7.33463 0.666656 8.22464 0.666656 10.0013V11.3346H11.3333V10.0013C11.3333 8.22464 7.77666 7.33463 5.99999 7.33463Z'
        )
        .attr('fill', '#333333')
        .attr(
          'transform',
          (d: any) =>
            `translate(${this.xScale(d.dateStart as Date)},${
              this.yScale(d.label) - this.yScale.bandwidth() / 2
            }) scale(1) `
        );

      this.enterSelections
        .append('circle')
        .attr('class', 'item-symbol item-circle')
        .attr('cx', (d: any) => {
          return this.xScale(d.dateStart as Date);
        })
        .attr('cy', (d: any) => {
          return this.yScale(d.label) - this.yScale.bandwidth() / 2;
        })
        .attr('r', (d: any) => {
          return (this.xScale(d.dateEnd) - this.xScale(d.dateStart)) / 15;
        })
        .attr('fill', 'none')
        .attr('stroke', '#FF0000');

      const icon = this.enterSelections
        .append('image')
        .attr('class', 'item-symbol item-image')
        .attr('x', (d: any) => {
          const pos =
            (this.xScale(d.dateEnd as Date) -
              this.xScale(d.dateStart as Date)) /
            2;
          return this.xScale(d.dateStart as Date) + pos;
        })
        .attr('y', (d: any) => {
          return this.yScale(d.label) - 50;
        })
        .attr('width', '30')
        .attr('height', '30')
        .attr('href', (d: any) => {
          return d.photo;
        })
        .attr('pointer-events', 'fill')
        // .attr("cursor", "pointer")
        .on('mouseover', (event: any, d: any) => {
          d3.select(event.target).style('cursor', 'pointer');
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px blue)'
          );
        })
        .on('mouseout', (event: any, d: any) => {
          d3.select(event.target).style('filter', null);
        })
        .on('click', (event: any, d: any) => {
          d3.select(event.target).style(
            'filter',
            'drop-shadow(0px 0px 6px yellow)'
          );
          this.myComponent.showPopin(d);
          // this.event = null;
        });

      /*
      // dragstarted
      .on("start", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px red)");
        d3.select(event.target)
        .raise() // D3 Raise function display current node at the top
        .classed("active", true);
      })
      // dragged
      .on("drag", (event: any, d: any) => {
        d3.select(event.target).style("filter", "drop-shadow(0px 0px 6px yellow)");
        d3.select(event.target).raise() // D3 Raise function display current node at the top
        .attr("x", ()=> {
          d.x = event.x; // Todo: Issue bad behavior
          return d.x;
          })    
      })
      // dragended
      .on("end", (event: any, d: any) => {
        d3.select(event.target).classed("active", false);
        d3.select(event.target).style("filter", null);
      });
      */
    } catch (e) {
      this.errorHandler(e);
    }
  }

  renderEdges() {
    try {
      console.debug('render edges', this.edgesList);
      //Draw connection
      const edgesLine = this.itemsGroup
        .append('g')
        .classed('item', true)
        .selectAll('.item-edge')
        .data(this.edgesList) // bind data
        .enter() // Add a corresponding number of placeholders for the data
        .append('path') // Generate a polyline on the placeholder (draw with path)
        .attr('class', 'item-edge')
        .attr('d', (d: any) => {
          const source = `M ${this.xScale(d.source.dateEnd as Date)} ${
            this.yScale(d.source.label) - 5
          }`;
          // const marginSource = ` H ${this.xScale(d.source.dateEnd as Date) + 50 }`;
          const marginTarget = ` V ${this.yScale(d.target.label) - 5}`;
          const target = ` H ${this.xScale(d.target.dateStart as Date)}`;
          return `${source + marginTarget + target}`;
        }) //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
        //.attr('d', (d: any) => { return d && 'M ' + d.source.x + ' ' + d.source.y + ' L ' + d.target.x + ' ' + d.target.y; }) //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve
        .attr('id', (d: any, i: any) => {
          return `${this.idName}-edgepath-${i}`;
        }) // Set id, used for connecting text
        .attr('marker-start', 'url(#joinStart)') // Mark the arrow according to the id number marked by the arrow
        .attr('marker-mid', 'url(#joinMid)') // Mark the arrow according to the id number marked by the arrow
        .attr('marker-end', 'url(#joinEnd)') // Mark the arrow according to the id number marked by the arrow
        .style('stroke', 'black') // colour
        .style('fill', 'none') // colour
        .style('stroke-width', 2); // Thickness

      //Connection name
      const edgeLabel = this.itemsGroup
        .append('g')
        .classed('item', true)
        .selectAll(`${this.idName}-item-edge-label`)
        .data(this.edgesList) // bind data
        .enter() // Add a corresponding number of placeholders for the data
        .append('text') // Create a text area for each connection
        .attr('class', `${this.idName}-item-edge-label`)
        //.attr('dx', 80)
        .attr('dy', 15);
      // .style("text-anchor", "middle")
      //.attr("transform", "rotate(-60)");

      edgeLabel
        .append('textPath')
        .style('pointer-events', 'none') // Disable mouse events
        .attr('font-family', 'sans-serif')
        .attr('font-size', '12px')
        .attr('fill', 'black')
        .attr('startOffset', '50%')
        .attr('xlink:href', (d: any, i: any) => {
          return `#${this.idName}-edgepath-${i}`;
        }) // The text is arranged on the line corresponding to the id
        .text((d: any) => {
          return d.tag;
        }); // Set text content

      //Draw the arrow on the line
      const defs = this.itemsGroup.append('defs'); // defs defines reusable elements
      const joinEnd = defs
        .append('marker') // Create arrow
        .attr('id', 'joinEnd')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinEnd')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-5 -5 10 10') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinEnd
        .append('path')
        .attr('d', 'M 0,0 m -5,-5 L 5,0 L -5,5 Z') // d: path description, Bezier curve
        .attr('fill', 'red'); // fill color

      const joinStart = defs
        .append('marker') // Create arrow
        .attr('id', 'joinStart')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinStart')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-6 -6 12 12') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinStart
        .append('path')
        .attr('d', 'M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0') // Circle d: path description, Bezier curve
        .attr('fill', 'red'); // fill color

      const joinMid = defs
        .append('marker') // Create arrow
        .attr('id', 'joinMid')
        // .attr('markerUnits','strokeWidth') // set to strokeWidth arrow will scale with the thickness of the line
        .attr('markerUnits', 'userSpaceOnUse') // Set to userSpaceOnUse arrow is not affected by connected elements
        .attr('class', 'joinMid')
        .attr('markerWidth', 12) // viewport
        .attr('markerHeight', 12) // viewport
        .attr('viewBox', '-6 -6 12 12') // viewBox
        .attr('refX', 0) // Distance deviation from the center of the circle
        .attr('refY', 0) // Deviation from the center of the circle
        .attr('orient', 'auto'); // The drawing direction can be set as: auto (automatically confirm the direction) and angle value
      joinMid
        .append('path')
        .attr('d', 'M 0, 0  m -5, 0  a 5,5 0 1,0 10,0  a 5,5 0 1,0 -10,0') // Circle d: path description, Bezier curve
        .attr('fill', 'black'); // fill color
    } catch (e) {
      this.errorHandler(e);
    }
  }

  zoomActions(event: any, data: any) {
    // function to redraw while zooming
    try {
      // Get X axis new scale
      // this.xScale = event.transform.rescaleX(this.xScale);
      const newX = event.transform.rescaleX(this.xScale);

      this.xAxis.scale(newX);
      this.xAxisGroup.call(this.xAxis); // update the axes

      this.xAxis2.scale(newX);
      this.xAxisGroup2.call(this.xAxis2); // update the axes

      // Update text position
      this.xAxisGroup
        .selectAll('text')
        .style('text-anchor', 'end')
        .attr('dx', '-.8em')
        .attr('dy', '1em')
        .attr('transform', 'rotate(-10)');

      this.xAxisGroup2.call(this.xAxis2).selectAll('text').attr('dy', '-0.3em');

      // Get Y axis new scale
      // this.yScale = event.transform.rescaleY(this.yScale)
      // const newY = event.transform.rescaleY(this.yScale);
      // this.yAxis.scale(newY);
      // this.yAxisGroup.call(this.yAxis); // update the axis

      // Update the items
      // this.enterSelections.attr('transform', event.transform);
      this.enterSelections
        .selectAll('.item-text')
        .attr(
          'x',
          (d: any) =>
            event.transform.applyX(this.xScale(d.dateStart as Date)) + 20
        );

      this.enterSelections
        .selectAll('.item-rect')
        .attr('width', (d: any) => {
          return (
            event.transform.applyX(this.xScale(d.dateEnd)) -
            event.transform.applyX(this.xScale(d.dateStart))
          );
        })
        .attr('x', (d: any) =>
          event.transform.applyX(this.xScale(d.dateStart as Date))
        );
      // .attr('y', (d: any)=> event.transform.applyY(this.yScale(d.label)));

      this.enterSelections
        .selectAll('.item-symbol-left1')
        .attr(
          'transform',
          (d: any) =>
            `translate(${event.transform.applyX(
              this.xScale(d.dateStart as Date)
            )},${this.yScale(d.label) - this.yScale.bandwidth() / 2}) scale(1)`
        );

      this.itemsGroup.selectAll('.item-edge').attr('d', (d: any) => {
        const source = `M ${event.transform.applyX(
          this.xScale(d.source.dateEnd as Date)
        )} ${this.yScale(d.source.label) - 5}`;
        // const marginSource = ` H ${event.transform.applyX(this.xScale(d.source.dateEnd as Date) + 50)}`;
        const marginTarget = ` V ${this.yScale(d.target.label) - 5}`;
        const target = ` H ${event.transform.applyX(
          this.xScale(d.target.dateStart as Date)
        )}`;
        return `${source + marginTarget + target}`;
      }); //Traverse all data. d represents the data currently traversed, and returns the drawn Bezier curve

      this.enterSelections
        .selectAll('.item-circle')
        .attr('cx', (d: any) =>
          event.transform.applyX(this.xScale(d.dateStart as Date))
        );
      //.attr('cy', (d: any)=> event.transform.applyY(this.yScale(d.label) - (this.yScale.bandwidth() / 2)));
      // .attr('transform', (d: any) => `translate(${event.transform.applyX(this.xScale(d.dateStart as Date))},${this.yScale(d.label) - (this.yScale.bandwidth() / 2)}) scale(1)`);

      this.enterSelections.selectAll('.item-image').attr('x', (d: any) => {
        const pos =
          (this.xScale(d.dateEnd as Date) - this.xScale(d.dateStart as Date)) /
          2;
        return event.transform.applyX(this.xScale(d.dateStart as Date) + pos);
      });

      this.enterSelections
        .selectAll('.item-path-left')
        .attr(
          'transform',
          (d: any) =>
            `translate(${event.transform.applyX(
              this.xScale(d.dateStart as Date)
            )},${this.yScale(d.label) - 16}) scale(1)`
        );

      this.enterSelections
        .selectAll('.item-path-right')
        .attr(
          'transform',
          (d: any) =>
            `translate(${event.transform.applyX(
              this.xScale(d.dateEnd as Date)
            )},${this.yScale(d.label) - 16}) scale(1)`
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  rescale(type: string, data: any) {
    try {
      switch (type) {
        case 'second':
          this.zoomLevel = 124308;
          break;
        case 'minute':
          this.zoomLevel = 2155;
          break;
        case 'hour':
          this.zoomLevel = 46;
          break;
        case 'day':
          this.zoomLevel = 1.65;
          break;
        case 'week':
          this.zoomLevel = 1.83;
          break;
        case 'month':
          this.zoomLevel = 0.073;
          break;
        case 'year':
          this.zoomLevel = 0.004;
          break;
        default:
          this.zoomLevel = 1;
          break;
      }

      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  clearItems(data: any) {
    try {
      // First Delete extra items
      this.itemsGroup.selectAll('.item').remove();

      // Then rebuild the data map
      // Drawing the Items
      // The selectAll() will detect and select existing Dom elements and new added DOM elements from data binding
      // Then filter with functions like enter() (data > DOM elements),
      // or update() (data = DOM elements),
      // or exit() (data < DOM elements)
      const dataBound = this.itemsGroup
        .selectAll('.item') // Retrieve elements based on a valid css selector
        .data(this.dataSet); // Then bind data

      dataBound.exit().remove(); // Use exit() to select or delete existing dom elements (if exists) before binding data to new added dom elements

      // Add new items
      this.enterSelections = dataBound
        .enter() // Use enter() to select future new dom elements ; replace it by update() if you prefer to append manualy each dom elements for each data record
        .append('g')
        .classed('item', true);

      // Update the zoom level
      if (this.zoomCanvas)
        this.zoomCanvas.call(
          this.zoom.transform,
          d3.zoomIdentity
            .translate(this.zoomPanX, this.zoomPanY)
            .scale(this.zoomLevel)
        );
    } catch (e) {
      this.errorHandler(e);
    }
  }

  errorHandler(error: {
    error: { message: string };
    status: any;
    message: any;
  }) {
    // console.error(error);
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = error.error?.message || error.message || error;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `error Code: ${status}\nMessage: ${
        error.message
      } | more message : ${error.error?.message || error.message || error}`;
    }
    console.debug(error);
    return throwError(errorMessage);
  }
}

```